package logistique.modele.objets;

import java.time.LocalDate;

public class Session {
	
	private int id ;
	private double note ;
	private Formateur formateur;
	private PlanificationFormation planificationFormation;
	
	
	public Session() {
	}


	public Session(PlanificationFormation planificationFormation, Formateur formateur) {
		super();
		this.planificationFormation = planificationFormation;
		this.formateur = formateur;
	}


	public PlanificationFormation getPlanificationFormattion() {
		return planificationFormation;
	}


	public void setPlanificationFormattion(PlanificationFormation planificationFormattion) {
		this.planificationFormation = planificationFormattion;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public double getNote() {
		return note;
	}


	public void setNote(double note) {
		this.note = note;
	}


	public Formateur getFormateur() {
		return formateur;
	}


	public void setFormateur(Formateur formateur) {
		this.formateur = formateur;
	}


	public PlanificationFormation getPlanificationFormation() {
		return planificationFormation;
	}


	public void setPlanificationFormation(PlanificationFormation planificationFormation) {
		this.planificationFormation = planificationFormation;
	}


	@Override
	public String toString() {
		return "Session [id=" + id + ", note=" + note + ", formateur=" + formateur + ", planificationFormation="
				+ planificationFormation + "]";
	}


	
	
	
}
