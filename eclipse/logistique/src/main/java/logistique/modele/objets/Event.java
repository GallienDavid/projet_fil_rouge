package logistique.modele.objets;

import java.io.Serializable;
import java.time.LocalDate;

public class Event { 
	

	private String title ;
	private LocalDate start ;
	private LocalDate end ;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public LocalDate getStart() {
		return start;
	}
	public void setStart(LocalDate start) {
		this.start = start;
	}
	public LocalDate getEnd() {
		return end;
	}
	public void setEnd(LocalDate end) {
		this.end = end;
	}
	public Event(String title, LocalDate start, LocalDate end) {
		super();
		this.title = title;
		this.start = start;
		this.end = end;
	}
	public Event() {
		super();
	}
	@Override
	public String toString() {
		return "Event [title=" + title + ", start=" + start + ", end=" + end + "]";
	}
	
	
	
}
