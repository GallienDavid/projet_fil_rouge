package logistique.modele.objets;

public class Agence {
	
	private int id ;
	private String zoneGeo ;
	private String tel ;
	private String adresse ;
	private String nomResponsable ;
	private String prenomResponsable ;
	private String mailResponsable ;
	private String telResponsable ;
	
	
	
	public Agence(String zoneGeo, String tel, String adresse, String nomResponsable, String prenomResponsable,
			String mailResponsable, String telResponsable) {
		super();
		this.zoneGeo = zoneGeo;
		this.tel = tel;
		this.adresse = adresse;
		this.nomResponsable = nomResponsable;
		this.prenomResponsable = prenomResponsable;
		this.mailResponsable = mailResponsable;
		this.telResponsable = telResponsable;
	}
	public Agence() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getZoneGeo() {
		return zoneGeo;
	}
	public void setZoneGeo(String zoneGeo) {
		this.zoneGeo = zoneGeo;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getNomResponsable() {
		return nomResponsable;
	}
	public void setNomResponsable(String nomResponsable) {
		this.nomResponsable = nomResponsable;
	}
	public String getPrenomResponsable() {
		return prenomResponsable;
	}
	public void setPrenomResponsable(String prenomResponsable) {
		this.prenomResponsable = prenomResponsable;
	}
	public String getMailResponsable() {
		return mailResponsable;
	}
	public void setMailResponsable(String mailResponsable) {
		this.mailResponsable = mailResponsable;
	}
	public String getTelResponsable() {
		return telResponsable;
	}
	public void setTelResponsable(String telResponsable) {
		this.telResponsable = telResponsable;
	}
	@Override
	public String toString() {
		return "Agence [id=" + id + ", zoneGeo=" + zoneGeo + ", tel=" + tel + ", adresse=" + adresse
				+ ", nomResponsable=" + nomResponsable + ", prenomResponsable=" + prenomResponsable
				+ ", mailResponsable=" + mailResponsable + ", telResponsable=" + telResponsable + "]";
	}
	
	
}
