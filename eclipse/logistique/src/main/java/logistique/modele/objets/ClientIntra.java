package logistique.modele.objets;

public class ClientIntra {

	private int id_Societe;
	private String nom_Societe;
	private String nom_Contact;
	private String prenom_Contact;
	private String mail;
	private String telephone;
	private String passwrd;

	public ClientIntra() {
		
	}

	public ClientIntra(String nom_Societe, String nom_Contact, String prenom_Contact,
			String mail, String telephone, String passwrd) {
		super();
		this.nom_Societe = nom_Societe;
		this.nom_Contact = nom_Contact;
		this.prenom_Contact = prenom_Contact;
		this.mail = mail;
		this.telephone = telephone;
		this.passwrd = passwrd;
	}

	public int getId_Societe() {
		return id_Societe;
	}

	public void setId_Societe(int id_Societe) {
		this.id_Societe = id_Societe;
	}

	public String getNom_Societe() {
		return nom_Societe;
	}

	public void setNom_Societe(String nom_Societe) {
		this.nom_Societe = nom_Societe;
	}

	public String getNom_Contact() {
		return nom_Contact;
	}

	public void setNom_Contact(String nom_Contact) {
		this.nom_Contact = nom_Contact;
	}

	public String getPrenom_Contact() {
		return prenom_Contact;
	}

	public void setPrenom_Contact(String prenom_Contact) {
		this.prenom_Contact = prenom_Contact;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPasswrd() {
		return passwrd;
	}

	public void setPasswrd(String passwrd) {
		this.passwrd = passwrd;
	}

	@Override
	public String toString() {
		return "ClientIntra [Id_Societe=" + id_Societe + ", nom_Societe=" + nom_Societe + ", nom_Contact=" + nom_Contact
				+ ", prenom_Contact=" + prenom_Contact + ", mail=" + mail + ", telephone=" + telephone + ", passwrd="
				+ passwrd + "]";
	}
	
}