package logistique.modele.objets;

public class SousTheme {
	private int id ;
	private String nom ;
	public SousTheme() {
		super();
	}



	public SousTheme(int id, String nom) {
		super();
		this.id = id;
		this.nom = nom;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	@Override
	public String toString() {
		return "SousTheme [id=" + id + ", nom=" + nom + "]";
	}
	
	

}
