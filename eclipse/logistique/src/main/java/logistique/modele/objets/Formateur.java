package logistique.modele.objets;

import java.util.List;

public class Formateur {
	
	private int id;
	private String nom ;
	private String Prenom;
	private String email ;
	private String adresse ;
	private float evaluation;
	private String telephone ;
	private String zoneGeo ;
	private float salaire ;
	private int nbJourTravailler ;
	
	private Module module ;
	
	public Formateur() {
		super();
	}
	
	public Formateur(String nom, String prenom, String email, String adresse, Module module, String telephone,
			String zoneGeo, float salaire) {
		super();
		this.nom = nom;
		Prenom = prenom;
		this.email = email;
		this.adresse = adresse;
		this.module = module;
		this.telephone = telephone;
		this.zoneGeo = zoneGeo;
		this.salaire = salaire;
		this.nbJourTravailler = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getZoneGeo() {
		return zoneGeo;
	}

	public void setZoneGeo(String zoneGeo) {
		this.zoneGeo = zoneGeo;
	}

	public float getSalaire() {
		return salaire;
	}

	public void setSalaire(float salaire) {
		this.salaire = salaire;
	}

	public int getNbJourTravailler() {
		return nbJourTravailler;
	}

	public void setNbJourTravailler(int nbJourTravailler) {
		this.nbJourTravailler = nbJourTravailler;
	}
	
	

	public float getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(float evaluation) {
		this.evaluation = evaluation;
	}

	@Override
	public String toString() {
		return "Formateur [id=" + id + ", nom=" + nom + ", Prenom=" + Prenom + ", email=" + email + ", adresse="
				+ adresse + ", module=" + module + ", telephone=" + telephone + ", zoneGeo=" + zoneGeo + ", salaire="
				+ salaire + ", nbJourTravailler=" + nbJourTravailler + "]";
	}

	

	
	

}
