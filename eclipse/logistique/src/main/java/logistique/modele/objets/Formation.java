package logistique.modele.objets;

import java.util.List;

public class Formation {
	
	private int id ;
	private ResponsableFormation responsable ;
	private String nom ;
	private int nbInscritMini ;
	private List<Module> modules ;
	
	public Formation() {
		super();
	}


	public Formation(String nom, List<Module> modules, ResponsableFormation responsable,int nbInscritMini) {
		super();
		this.nom = nom;
		this.modules = modules;
		this.responsable = responsable;
		this.nbInscritMini = nbInscritMini;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public List<Module> getModules() {
		return modules;
	}


	public void setModules(List<Module> modules) {
		this.modules = modules;
	}


	public ResponsableFormation getResponsable() {
		return responsable;
	}


	public void setResponsable(ResponsableFormation responsable) {
		this.responsable = responsable;
	}


	public int getNbInscritMini() {
		return nbInscritMini;
	}


	public void setNbInscritMini(int nbInscritMini) {
		this.nbInscritMini = nbInscritMini;
	}


	@Override
	public String toString() {
		return "Formation [id=" + id + ", responsable=" + responsable + ", nom=" + nom + ", nbInscritMini="
				+ nbInscritMini + ", modules=" + modules + "]";
	}
	
	public int duree() {
		int duree = 0 ;
		for (int i = 0; i < this.getModules().size(); i++) {
			this.getModules().get(i).getNombreJour();
			duree = duree + this.getModules().get(i).getNombreJour();
		}
		return duree;		
	}
	
	public float prix() {
		float prix = 0 ;
		for (int i = 0; i < this.getModules().size(); i++) {
			this.getModules().get(i).getPrix();
			prix = prix + this.getModules().get(i).getPrix();
		}
		return prix;	
	}

}
