package logistique.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logistique.modele.objets.ClientInter;
import logistique.modele.objets.ClientIntra;


public class ClientIntraDao {

	public void enregistrer(ClientIntra clientIntra) {
	
	// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
			try {
				
				Connection connexion = PoolConnexion.getConnexion();
				// 2 - Fabriquer la requete
				
				String query = "INSERT INTO CLIENTS_INTRA VALUES(?,?,?,?,?,CONVERT(NVARCHAR(32),"
						+ "HASHBYTES('MD5', cast(? as varchar)),2))";
				// 3 - Ex�cuter la requete
				
				PreparedStatement pstmt = connexion.prepareStatement(query);
				pstmt.setString(1, clientIntra.getNom_Societe());
				pstmt.setString(2, clientIntra.getNom_Contact());
				pstmt.setString(3, clientIntra.getPrenom_Contact());
				pstmt.setString(4, clientIntra.getMail());
				pstmt.setString(5, clientIntra.getTelephone());
				pstmt.setString(6, clientIntra.getPasswrd());

				pstmt.executeUpdate();

				
				connexion.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}

	public ClientIntra findByMailAndPasswrd(String mail, String passwrd) {
		ClientIntra clientIntra = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
		
			String query = "SELECT * FROM CLIENTS_INTRA WHERE mail = ? and PASSWRD = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";
			
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			pstmt.setString(2, passwrd);
	
			ResultSet rs = pstmt.executeQuery();
	
			while(rs.next()) {
				clientIntra = connexionClientIntra(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clientIntra;
	}

	private static ClientIntra connexionClientIntra(ResultSet rs) throws SQLException {
		ClientIntra clientIntra = new ClientIntra();
		clientIntra.setId_Societe(rs.getInt("id_Societe"));
		clientIntra.setNom_Societe(rs.getString("nom_Societe"));
		clientIntra.setNom_Contact(rs.getString("nom_Contact"));
		clientIntra.setPrenom_Contact(rs.getString("prenom_Contact"));
		clientIntra.setMail(rs.getString("mail"));
		clientIntra.setTelephone(rs.getString("telephone"));
		clientIntra.setPasswrd(rs.getString("passwrd"));
		return clientIntra;
	}
	
	
	public List<ClientIntra> findAll() {
		List<ClientIntra> clients = new ArrayList<>();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "select * from CLIENTS_INTRA;";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				ClientIntra client  = remplirClient(rs);	
				clients.add(client);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clients;
	}

	private ClientIntra remplirClient(ResultSet rs) throws SQLException {
		ClientIntra client;
		client = new ClientIntra();
		client.setId_Societe(rs.getInt("ID_SOCIETE"));
		client.setNom_Societe(rs.getString("NOM_SOCIETE"));
		client.setNom_Contact(rs.getString("NOM_CONTACT"));
		client.setPrenom_Contact(rs.getString("PRENOM_CONTACT"));
		client.setMail(rs.getString("MAIL"));
		client.setTelephone(rs.getString("TELEPHONE"));
		client.setPasswrd(rs.getString("PASSWRD"));
		
		return client;
	}
	
		



}


