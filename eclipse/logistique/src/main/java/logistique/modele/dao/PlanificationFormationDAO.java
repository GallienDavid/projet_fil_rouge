package logistique.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


import logistique.modele.objets.Event;
import logistique.modele.objets.Formation;
import logistique.modele.objets.Module;
import logistique.modele.objets.PlanificationFormation;

public class PlanificationFormationDAO {

	public void enregistrer(PlanificationFormation planificationFormation) {
		Connection connexion = PoolConnexion.getConnexion();
		String query = "INSERT INTO PLANIFICATION_FORMATION VALUES(?,?,?,0,?)";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, planificationFormation.getFormation().getId());
			pstmt.setInt(2, planificationFormation.getSalle().getId());
			pstmt.setDate(3, Date.valueOf(planificationFormation.getDateDebut()));
			pstmt.setInt(4, planificationFormation.getCible());
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<PlanificationFormation> findAll() {
		System.out.println(LocalTime.now());
		ModuleDAO moduleManager = new ModuleDAO();
		List<PlanificationFormation> planificationFormations = new ArrayList<>();
		Connection connexion = PoolConnexion.getConnexion();
		String query ="SELECT ID_SALLE, DATE_DEBUT, O.ID_PLANIFICATION,VALIDER,CIBLE ,NOM_FORMATION, FORMATION.ID_FORMATION,RESPONSABLE_FORMATION.ID_RESPONSABLE, NOM_RESPONSABLE,PRENOM_RESPONSABLE,MAIL_RESPONSABLE,TELEPHONE_RESPONSABLE,nombreInscrit, NOM_MODULE, MODULE.ID_MODULE ,PRIX, NOMBRE_JOUR FROM PLANIFICATION_FORMATION O JOIN (SELECT COUNT(p.ID_CLIENT) as 'nombreInscrit' , t.ID_PLANIFICATION FROM PLANIFICATION_FORMATION t  LEFT JOIN INTER_PLANIFICATION p "
				+" ON t.ID_PLANIFICATION = p.ID_PLANIFICATION GROUP BY t.ID_PLANIFICATION) A ON A.ID_PLANIFICATION = O.ID_PLANIFICATION JOIN FORMATION ON "
				+" O.ID_FORMATION = FORMATION.ID_FORMATION JOIN RESPONSABLE_FORMATION on FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE ORDER BY DATE_DEBUT DESC ; " ;
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			
			FormationDAO formationDAO = new FormationDAO();
			SalleDAO salleDAO = new SalleDAO();
			PlanificationFormation planificationFormation;
			planificationFormation = new PlanificationFormation();
			while (rs.next()) {
				if(rs.getInt("ID_PLANIFICATION") == planificationFormation.getId()) {
					
					Module module = moduleManager.remplirModule(rs);
					planificationFormation.getFormation().getModules().add(module) ;	
				}else if(rs.getInt("ID_PLANIFICATION") != planificationFormation.getId()) {
					planificationFormation = new PlanificationFormation();
				
					planificationFormations.add(planificationFormation);
				
					List<Module> modules = new ArrayList();
				
					planificationFormation.setCible(rs.getInt("CIBLE"));
					planificationFormation.setId(rs.getInt("ID_PLANIFICATION"));
					planificationFormation.setSalle(salleDAO.salleByID(rs.getInt("ID_SALLE")));
					planificationFormation.setDateDebut(rs.getDate("DATE_DEBUT").toLocalDate());
					planificationFormation.setNbInscrit(rs.getInt("nombreInscrit"));
					
					planificationFormation.setFormation(formationDAO.remplirFormationWithoutModule(rs));
					planificationFormation.getFormation().setModules(modules);
					Module module = moduleManager.remplirModule(rs);
					planificationFormation.getFormation().getModules().add(module) ;
				}
				
				/*
				FormationDAO formationDAO = new FormationDAO();
				SalleDAO salleDAO = new SalleDAO();
				
				planificationFormation.setCible(rs.getInt("CIBLE"));
				planificationFormation.setId(rs.getInt("ID_PLANIFICATION"));
				planificationFormation.setSalle(salleDAO.salleByID(rs.getInt("ID_SALLE")));
				planificationFormation.setDateDebut(rs.getDate("DATE_DEBUT").toLocalDate());
				planificationFormation.setNbInscrit(rs.getInt("nombreInscrit"));
				planificationFormation.setFormation(formationDAO.remplirFormationWithoutModule(rs));	
				
				Module module = moduleManager.remplirModule(rs);
				planificationFormation.getFormation().getModules().add(module) ;
				planificationFormations.add(planificationFormation);*/
				
			}
			/*for (int i = 0; i < planificationFormations.size(); i++) {
				query = "SELECT  MODULE.ID_MODULE , NOM_MODULE , PRIX , NOMBRE_JOUR"
						+ " FROM FORMATION JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION"
						+ " JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE WHERE FORMATION.ID_FORMATION = "
						+ planificationFormations.get(i).getFormation().getId() + ";";
				pstmt = connexion.prepareStatement(query);
				// System.out.println(query);
				rs = pstmt.executeQuery();
				
				List<Module> modules = new ArrayList();
				while (rs.next()) {
					Module module = moduleManager.remplirModule(rs);
					modules.add(module);
				}
				planificationFormations.get(i).getFormation().setModules(modules);
				//planificationFormations.get(i).setNbInscrit(nombreInscrit(planificationFormations.get(i).getId()));
			}*/
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long time2 = System.currentTimeMillis();
		System.out.println(LocalTime.now());
		return planificationFormations;

	}

	private PlanificationFormation remplirPlanificationFormation(ResultSet rs) throws SQLException {
		FormationDAO formationDAO = new FormationDAO();
		SalleDAO salleDAO = new SalleDAO();
		PlanificationFormation planificationFormation;
		planificationFormation = new PlanificationFormation();
		planificationFormation.setCible(rs.getInt("CIBLE"));
		planificationFormation.setId(rs.getInt("ID_PLANIFICATION"));
		planificationFormation.setSalle(salleDAO.salleByID(rs.getInt("ID_SALLE")));
		planificationFormation.setDateDebut(rs.getDate("DATE_DEBUT").toLocalDate());

		planificationFormation.setFormation(formationDAO.remplirFormationWithoutModule(rs));

		return planificationFormation;

	}

	public PlanificationFormation findById(Integer id) {
		PlanificationFormation planificationFormation = new PlanificationFormation();
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT ID_SALLE, DATE_DEBUT, PLANIFICATION_FORMATION.ID_PLANIFICATION,VALIDER,CIBLE ,NOM_FORMATION, FORMATION.ID_FORMATION,RESPONSABLE_FORMATION.ID_RESPONSABLE, NOM_RESPONSABLE,PRENOM_RESPONSABLE,MAIL_RESPONSABLE,TELEPHONE_RESPONSABLE FROM PLANIFICATION_FORMATION JOIN FORMATION ON"
				+ "	PLANIFICATION_FORMATION.ID_FORMATION = FORMATION.ID_FORMATION JOIN RESPONSABLE_FORMATION on FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE WHERE ID_PLANIFICATION = "
				+ id + "ORDER BY ID_PLANIFICATION ASC ;";

		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				planificationFormation = remplirPlanificationFormation(rs);
			}
			query = "SELECT  MODULE.ID_MODULE , NOM_MODULE , PRIX , NOMBRE_JOUR"
					+ " FROM FORMATION JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION"
					+ " JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE WHERE FORMATION.ID_FORMATION = "
					+ planificationFormation.getFormation().getId() + ";";
			pstmt = connexion.prepareStatement(query);
			// System.out.println(query);
			rs = pstmt.executeQuery();
			ModuleDAO moduleManager = new ModuleDAO();
			List<Module> modules = new ArrayList();
			while (rs.next()) {
				Module module = moduleManager.remplirModule(rs);
				modules.add(module);
			}
			planificationFormation.getFormation().setModules(modules);
			planificationFormation.setDateFin(planificationFormation.getFormation().duree());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return planificationFormation;
	}

	public List<PlanificationFormation> findByIdFormation(int idFormation) {
		
		List<PlanificationFormation> planificationFormations = new ArrayList<PlanificationFormation>();
		
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT ID_SALLE, DATE_DEBUT, PLANIFICATION_FORMATION.ID_PLANIFICATION,VALIDER,CIBLE ,NOM_FORMATION, FORMATION.ID_FORMATION,RESPONSABLE_FORMATION.ID_RESPONSABLE, NOM_RESPONSABLE,PRENOM_RESPONSABLE,MAIL_RESPONSABLE,TELEPHONE_RESPONSABLE FROM PLANIFICATION_FORMATION JOIN FORMATION ON"
				+ "	PLANIFICATION_FORMATION.ID_FORMATION = FORMATION.ID_FORMATION JOIN RESPONSABLE_FORMATION on FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE WHERE FORMATION.ID_FORMATION = "
				+ idFormation + ";";

		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				PlanificationFormation planificationFormation = remplirPlanificationFormation(rs);
				planificationFormations.add(planificationFormation);
			}
			for (int i = 0; i < planificationFormations.size(); i++) {
				query = "SELECT  MODULE.ID_MODULE , NOM_MODULE , PRIX , NOMBRE_JOUR"
						+ " FROM FORMATION JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION"
						+ " JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE WHERE FORMATION.ID_FORMATION = "
						+ planificationFormations.get(i).getFormation().getId() + ";";
				pstmt = connexion.prepareStatement(query);
				// System.out.println(query);
				rs = pstmt.executeQuery();
				ModuleDAO moduleManager = new ModuleDAO();
				List<Module> modules = new ArrayList();
				while (rs.next()) {
					Module module = moduleManager.remplirModule(rs);
					modules.add(module);
				}
				planificationFormations.get(i).getFormation().setModules(modules);
				planificationFormations.get(i).setDateFin(planificationFormations.get(i).getFormation().duree());
				planificationFormations.get(i).setNbInscrit(nombreInscrit(planificationFormations.get(i).getId()));
				System.out.println( planificationFormations.get(i).getNbInscrit());
				System.out.println(planificationFormations.get(i).getDateFin());
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return planificationFormations;
		
		
		
		
	}
	
	public int nombreInscrit(int idPlanification) {
		int compteurInscrit = 0;
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT COUNT(p.ID_CLIENT) as 'nombreInscrit' FROM INTER_PLANIFICATION p WHERE ID_PLANIFICATION =" + idPlanification + ";";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			rs =pstmt.executeQuery();
			rs.next();
			compteurInscrit = rs.getInt("nombreInscrit");
			connexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return compteurInscrit;
	}

	public List<PlanificationFormation> findAllWithoutModule(){
		List<PlanificationFormation> planificationFormations = new ArrayList<>();
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT ID_SALLE, DATE_DEBUT, O.ID_PLANIFICATION,VALIDER,CIBLE ,NOM_FORMATION, FORMATION.ID_FORMATION,RESPONSABLE_FORMATION.ID_RESPONSABLE, NOM_RESPONSABLE,PRENOM_RESPONSABLE,MAIL_RESPONSABLE,TELEPHONE_RESPONSABLE,nombreInscrit FROM PLANIFICATION_FORMATION O JOIN (SELECT COUNT(p.ID_CLIENT) as 'nombreInscrit' , t.ID_PLANIFICATION FROM PLANIFICATION_FORMATION t  LEFT JOIN INTER_PLANIFICATION p "
				 + " ON t.ID_PLANIFICATION = p.ID_PLANIFICATION GROUP BY t.ID_PLANIFICATION) A ON A.ID_PLANIFICATION = O.ID_PLANIFICATION JOIN FORMATION ON " +
								" O.ID_FORMATION = FORMATION.ID_FORMATION JOIN RESPONSABLE_FORMATION on FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE ORDER BY DATE_DEBUT DESC ; " ;

		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				PlanificationFormation planificationFormation = remplirPlanificationFormation(rs);
				planificationFormation.setNbInscrit(rs.getInt("nombreInscrit"));
				planificationFormations.add(planificationFormation);
				
			}} catch (SQLException e) {
				e.printStackTrace();
			}

			return planificationFormations;
	}
	
	public List<PlanificationFormation> findAllWithoutOrder() {

		List<PlanificationFormation> planificationFormations = new ArrayList<>();
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT ID_SALLE, DATE_DEBUT, PLANIFICATION_FORMATION.ID_PLANIFICATION,VALIDER,CIBLE ,NOM_FORMATION, FORMATION.ID_FORMATION,RESPONSABLE_FORMATION.ID_RESPONSABLE, NOM_RESPONSABLE,PRENOM_RESPONSABLE,MAIL_RESPONSABLE,TELEPHONE_RESPONSABLE FROM PLANIFICATION_FORMATION JOIN FORMATION ON"
				+ "	PLANIFICATION_FORMATION.ID_FORMATION = FORMATION.ID_FORMATION JOIN RESPONSABLE_FORMATION on FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE ;";

		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				PlanificationFormation planificationFormation = remplirPlanificationFormation(rs);
				planificationFormations.add(planificationFormation);
			}
			for (int i = 0; i < planificationFormations.size(); i++) {
				query = "SELECT  MODULE.ID_MODULE , NOM_MODULE , PRIX , NOMBRE_JOUR"
						+ " FROM FORMATION JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION"
						+ " JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE WHERE FORMATION.ID_FORMATION = "
						+ planificationFormations.get(i).getFormation().getId() + ";";
				pstmt = connexion.prepareStatement(query);
				// System.out.println(query);
				rs = pstmt.executeQuery();
				ModuleDAO moduleManager = new ModuleDAO();
				List<Module> modules = new ArrayList();
				while (rs.next()) {
					Module module = moduleManager.remplirModule(rs);
					modules.add(module);
				}
				planificationFormations.get(i).getFormation().setModules(modules);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return planificationFormations;

	}
	
	


}
