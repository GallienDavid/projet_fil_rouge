package logistique.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logistique.modele.objets.ClientInter;
import logistique.modele.objets.Formation;
import logistique.modele.objets.Module;



public class ClientInterDao {

	public void enregistrer(ClientInter clientInter) {
		
		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib du tomcat)
		try {
			
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			
			String query = "INSERT INTO CLIENTS_INTER VALUES(?,?,?,?,CONVERT(NVARCHAR(32),"
					+ "HASHBYTES('MD5', cast(? as varchar)),2))";
			// 3 - Ex�cuter la requete
			
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, clientInter.getNom());
			pstmt.setString(2, clientInter.getPrenom());
			pstmt.setString(3, clientInter.getMail());
			pstmt.setString(4, clientInter.getTelephone());
			pstmt.setString(5, clientInter.getPasswrd());

			pstmt.executeUpdate();

			
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public ClientInter findByMailAndPasswrd(String mail, String passwrd) {
		ClientInter clientInter = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
		
			String query = "SELECT * FROM CLIENTS_INTER WHERE mail = ? and PASSWRD = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";
			
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			pstmt.setString(2, passwrd);
	
			ResultSet rs = pstmt.executeQuery();
	
			while(rs.next()) {
				clientInter = connexionClientInter(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clientInter;
	}

	private static ClientInter connexionClientInter(ResultSet rs) throws SQLException {
		ClientInter clientInter = new ClientInter();
		clientInter.setId_Client(rs.getInt("id_Client"));
		clientInter.setNom(rs.getString("nom"));
		clientInter.setPrenom(rs.getString("prenom"));
		clientInter.setMail(rs.getString("mail"));
		clientInter.setTelephone(rs.getString("telephone"));
		clientInter.setPasswrd(rs.getString("passwrd"));
		return clientInter;
	}
	
	
	public List<ClientInter> findAll() {
		List<ClientInter> clients = new ArrayList<>();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "select * from CLIENTS_INTER;";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				ClientInter client  = remplirClient(rs);	
				clients.add(client);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clients;
	}

	private ClientInter remplirClient(ResultSet rs) throws SQLException {
		ClientInter client;
		client = new ClientInter();
		client.setId_Client(rs.getInt("ID_CLIENT"));
		client.setNom(rs.getString("NOM"));
		client.setPrenom(rs.getString("PRENOM"));
		client.setMail(rs.getString("MAIL"));
		client.setTelephone(rs.getString("TELEPHONE"));
		client.setPasswrd(rs.getString("PASSWRD"));
		
		return client;
	}
	
	public ClientInter findById(int id) {
		ClientInter clients = new ClientInter();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "select * from CLIENTS_INTER WHERE ID_CLIENT = "+ id +  " ;";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				clients  = remplirClient(rs);	
				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clients;
		
	
	}
}
	

	
	
		


