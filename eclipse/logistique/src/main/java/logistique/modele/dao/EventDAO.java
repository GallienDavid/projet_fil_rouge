package logistique.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logistique.modele.objets.Event;
import logistique.modele.objets.Module;
import logistique.modele.objets.PlanificationFormation;

public class EventDAO {
	
	
	public List<Event> findAll() {
			
			List<Event> events = new ArrayList<>();
			
			PlanificationFormationDAO planificationDAO = new PlanificationFormationDAO() ;
			
			List<PlanificationFormation> plani = planificationDAO.findAll() ;
			
			for(int i = 0 ; i < plani.size();i++) {
				Event event = new Event();
				event.setStart(plani.get(i).getDateDebut());
				event.setEnd(plani.get(i).dateFin());
				event.setTitle(plani.get(i).getFormation().getNom());
				events.add(event) ;
			}
			
		return events ;
	}

}
