package logistique.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logistique.modele.objets.Agence;



public class AgenceDAO {
	
	
	public List<Agence> findAll() {
			
			List<Agence> agences = new ArrayList<>();
			
			// 1 - R�cup�rer la connexion
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM AGENCE";
			try {
				PreparedStatement pstmt = connexion.prepareStatement(query);
				// 3 - Ex�cuter la requete
				ResultSet rs = pstmt.executeQuery();
				while(rs.next()) {
					Agence agence = remplirAgence(rs);
					agences.add(agence);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return agences;
		}
	
	private Agence remplirAgence(ResultSet rs) throws SQLException {
		Agence agence;
		agence = new Agence();
		agence.setId(rs.getInt("ID_AGENCE"));
		agence.setZoneGeo(rs.getString("ZONE_GEOGRAHIQUE"));
		agence.setTel(rs.getString("TELEPHONE_AGENCE"));
		agence.setAdresse(rs.getString("ADRESSE_AGENCE")); // le mdp stock� est hach� ici
		agence.setNomResponsable(rs.getString("NOM_RESPONSABLE"));
		agence.setPrenomResponsable(rs.getString("PRENOM_RESPONSABLE"));
		agence.setMailResponsable(rs.getString("CONTACT_MAIL_RESPONSABLE"));
		agence.setTelResponsable(rs.getString("CONTACT_TELEPHONE_RESPONSABLE"));
		return agence;
	}

	public List<Agence> findDepuisZoneGeo(String zoneGeo) {
		List<Agence> agences = new ArrayList<>();
		
		// 1 - R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 - Fabriquer la requete
		String query = "SELECT * FROM AGENCE WHERE ZONE_GEOGRAPHIQUE = " + zoneGeo + ";";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Agence agence = remplirAgence(rs);
				agences.add(agence);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return agences;
	}

}
