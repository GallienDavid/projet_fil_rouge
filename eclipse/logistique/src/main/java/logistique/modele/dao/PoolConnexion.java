package logistique.modele.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class PoolConnexion {
	
	public static Connection getConnexion() {
		Connection connexion = null;
		try {
			 // charger le driver
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			connexion = 
					DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databasename=BDD_IT_Training", "sa", "Password1234");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connexion;
	}

}
