package logistique.modele.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import logistique.modele.manager.ClientInterManager;
import logistique.modele.manager.FormateurManager;
import logistique.modele.objets.ClientInter;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.PlanificationFormation;
import logistique.modele.objets.Session;

public class SessionDAO {

	public void enregistrer(Session session) {

		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO SESSION(ID_PLANIFICATION, ID_FORMATEUR, NOTE) VALUES(?,?,0)";

			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, Integer.valueOf(session.getPlanificationFormation().getId()));
			pstmt.setInt(2, Integer.valueOf(session.getFormateur().getId()));
			pstmt.executeUpdate();
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<Formateur> findAllByIdPlanifcation(int idPlanification){
		FormateurManager formateurManager = new FormateurManager();
		List<Formateur> formateurs = new ArrayList();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM SESSION WHERE ID_PLANIFICATION = " + idPlanification + " ;";

			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				rs.getInt("ID_FORMATEUR");
				System.out.println(rs.getInt("ID_FORMATEUR"));
				formateurs.add(formateurManager.FormateurById(rs.getInt("ID_FORMATEUR")));
				
			}
			
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return formateurs ;
	}
	
	public  List<ClientInter> findAllById(int idPlanification){
		
		ClientInterDao formateurManager = new ClientInterDao();
		List<ClientInter> clients = new ArrayList();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM INTER_PLANIFICATION JOIN CLIENTS_INTER ON CLIENTS_INTER.ID_CLIENT = INTER_PLANIFICATION.ID_CLIENT WHERE ID_PLANIFICATION = " + idPlanification + " ;";

			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				rs.getInt("ID_CLIENT");
				System.out.println(rs.getInt("ID_CLIENT"));
				clients.add(formateurManager.findById(rs.getInt("ID_CLIENT")));		
			}	
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return clients ;
		
	}
	
	public  Set<PlanificationFormation> findAllByIdFormateur(int idFormateur){
			
			PlanificationFormationDAO formateurManager = new PlanificationFormationDAO();
			Set<PlanificationFormation> planis = new HashSet();
			try {
				Connection connexion = PoolConnexion.getConnexion();
				String query = "SELECT * FROM SESSION WHERE ID_FORMATEUR = " + idFormateur + " ;";
	
				PreparedStatement pstmt = connexion.prepareStatement(query);
				// 3 Ex�cuter la requ�te
				ResultSet rs = pstmt.executeQuery();
				int id = 0 ;
				while (rs.next()) {
					
					if(id != rs.getInt("ID_PLANIFICATION")) {
						id = rs.getInt("ID_PLANIFICATION");
						planis.add(formateurManager.findById(rs.getInt("ID_PLANIFICATION")));
					}
				}	
				connexion.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return planis ;
			
		}
		

	

}
