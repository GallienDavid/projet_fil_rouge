package logistique.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import logistique.modele.objets.User;

public class ConnexionDAO {

	public User findByUsername(String mail, String passwrd) {
		
		User user = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();
		
			String query = "SELECT * FROM USER_LOGISTIQUE WHERE USERNAME = ? and PASSWRD = ?";
			
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			pstmt.setString(2, passwrd);
	
			ResultSet rs = pstmt.executeQuery();
	
			while(rs.next()) {
				user = connexionUser(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	
	private static User connexionUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setUsername(rs.getString("USERNAME"));
		user.setPasswrd(rs.getString("PASSWRD"));
		
		return user;
	}


}
