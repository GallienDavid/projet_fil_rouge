package logistique.modele.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logistique.modele.objets.Formateur;
import logistique.modele.objets.Module;

public class FormateurDAO {

	public static void enregistrer(Formateur formateur) {

		try {
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			/*
			 * Statement statement = connexion.createStatement(); String query =
			 * "INSERT INTO Utilisateur(nom, email, mdp) VALUES('" + utilisateur.getNom() +
			 * "','" + utilisateur.getEmail() +
			 * "', CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast('" + utilisateur.getMdp() +
			 * "'as varchar)),2)" + ");"; System.out.println(query); // 3 - Ex�cuter la
			 * requete statement.executeUpdate(query);
			 */
			String query = "INSERT INTO FORMATEUR(NOM,PRENOM,TELEPHONE,MAIL,ADRESSE,SALAIRE_JOURNALIER,ZONE_GEOGRAPHIQUE,NB_JOURS_TRAVAILLES,ID_MODULE) VALUES(?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, formateur.getNom());
			// le nom de l'utilisateur est mis en tant que
			// string dans le 1er parametre de la query
			pstmt.setString(2, formateur.getPrenom());
			pstmt.setString(3, formateur.getTelephone());
			pstmt.setString(4, formateur.getEmail());
			pstmt.setString(5, formateur.getAdresse());
			pstmt.setFloat(6, formateur.getSalaire());
			pstmt.setString(7, formateur.getZoneGeo());
			pstmt.setInt(8, formateur.getNbJourTravailler());
			pstmt.setInt(9, formateur.getModule().getID_MODULE());
			// pstmt.executeQuery() // que pour les SELECT
			pstmt.executeUpdate(); // que pour INSERT, DELETE, UPDATE
			// 4 - G�rer les retours �ventuels
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Formateur> findAll() {
		List<Formateur> formateurs = new ArrayList<>();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "SELECT * FROM FORMATEUR JOIN MODULE ON FORMATEUR.ID_MODULE = MODULE.ID_MODULE";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Formateur formateur = remplirFormateur(rs);
				formateurs.add(formateur);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formateurs;
	}

	public Formateur remplirFormateur(ResultSet rs) throws SQLException {
		Formateur formateur;
		formateur = new Formateur();
		formateur.setId(rs.getInt("ID_FORMATEUR"));
		formateur.setNom(rs.getString("NOM"));
		formateur.setPrenom(rs.getString("PRENOM"));
		formateur.setEmail(rs.getString("MAIL"));
		formateur.setAdresse(rs.getString("ADRESSE"));
		formateur.setEvaluation(rs.getFloat("EVALUATION"));
		formateur.setTelephone(rs.getString("TELEPHONE"));
		formateur.setZoneGeo(rs.getString("ZONE_GEOGRAPHIQUE"));
		formateur.setSalaire(rs.getFloat("SALAIRE_JOURNALIER"));
		formateur.setNbJourTravailler(rs.getInt("NB_JOURS_TRAVAILLES"));

		Module module;
		module = new Module();
		module.setID_MODULE(rs.getInt("ID_MODULE"));
		module.setNomModule(rs.getString("NOM_MODULE"));
		module.setPrix(rs.getFloat("PRIX"));
		module.setNombreJour(rs.getInt("NOMBRE_JOUR"));

		formateur.setModule(module);

		return formateur;
	}

	public Formateur findFormateurById(int id) {
		Formateur formateur = new Formateur();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "SELECT * FROM FORMATEUR JOIN MODULE ON FORMATEUR.ID_MODULE = MODULE.ID_MODULE WHERE ID_FORMATEUR = "
				+ "'" + id + "';";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				formateur = remplirFormateur(rs);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formateur;

	}

	public Formateur formateurDepuisNom(String nom) {
		Formateur formateur = new Formateur();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "SELECT * FROM FORMATEUR JOIN MODULE ON FORMATEUR.ID_MODULE = MODULE.ID_MODULE WHERE NOM = "
				+ "'" + nom + "';";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				formateur = remplirFormateur(rs);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formateur;
	}

	public List<Formateur> formateurDepuisModule(String nomModule) {
		List<Formateur> formateurs = new ArrayList<Formateur>();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "SELECT * FROM FORMATEUR JOIN MODULE ON FORMATEUR.ID_MODULE = MODULE.ID_MODULE WHERE NOM_MODULE = "
				+ "'" + nomModule + "';";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Formateur formateur = new Formateur();
				formateur = remplirFormateur(rs);
				formateurs.add(formateur);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formateurs;
	}

	public void deleteById(int id) {
		Connection connexion = PoolConnexion.getConnexion();

		String query = "DELETE FROM FORMATEUR WHERE ID_FORMATEUR = " + "'" + id + "' ;";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public List<Formateur> formateurDepuisIdModule(int idModule) {
		List<Formateur> formateurs = new ArrayList<Formateur>();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "SELECT * FROM FORMATEUR JOIN MODULE ON FORMATEUR.ID_MODULE = MODULE.ID_MODULE WHERE MODULE.ID_MODULE = "
				+ "'" + idModule + "';";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Formateur formateur = new Formateur();
				formateur = remplirFormateur(rs);
				formateurs.add(formateur);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formateurs;
	}

}
