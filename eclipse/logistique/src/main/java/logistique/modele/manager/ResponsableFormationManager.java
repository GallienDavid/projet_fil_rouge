package logistique.modele.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import logistique.modele.dao.AgenceDAO;
import logistique.modele.dao.ModuleDAO;
import logistique.modele.dao.ResponsableFormationDAO;
import logistique.modele.objets.Agence;
import logistique.modele.objets.ResponsableFormation;

public class ResponsableFormationManager {

	
	public List<ResponsableFormation> findAll() {
		ResponsableFormationDAO responsableFormationDAO = new ResponsableFormationDAO();
		return responsableFormationDAO.findAll();
	}

	public ResponsableFormation responsableDepuisNom(String nomResponsable) {
		ResponsableFormationDAO responsableFormationDAO = new ResponsableFormationDAO();
		return responsableFormationDAO.responsableDepuisNom(nomResponsable);
	}

	public ResponsableFormation remplirResponsableFormation(ResultSet rs) throws SQLException {
		ResponsableFormationDAO responsableFormationDAO = new ResponsableFormationDAO();
		return responsableFormationDAO.remplirResponsableFormation(rs);
		
	}
}
