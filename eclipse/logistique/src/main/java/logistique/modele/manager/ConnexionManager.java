package logistique.modele.manager;

import logistique.modele.dao.ConnexionDAO;
import logistique.modele.objets.User;

public class ConnexionManager {

	public User findByUsername(String mail, String passwrd) {
		ConnexionDAO connexionDAO = new ConnexionDAO() ;
		User user = connexionDAO.findByUsername(mail,passwrd);
		return user;
	}

}
