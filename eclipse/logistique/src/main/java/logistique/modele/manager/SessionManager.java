package logistique.modele.manager;

import java.util.List;
import java.util.Set;

import logistique.modele.dao.SessionDAO;
import logistique.modele.objets.ClientInter;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.PlanificationFormation;
import logistique.modele.objets.Session;


public class SessionManager {
	
	public void enregistrer(Session session) {
		SessionDAO sessionDAO = new SessionDAO();
		// NB : on pourrait chiffrer le MDP ici, c'est le bon endroit
		sessionDAO.enregistrer(session);
	}

	public List<Formateur> findAllByIdPlanification(int idPlanification){
		System.out.println("je suis l�");
		SessionDAO sessionDAO = new SessionDAO();
		return sessionDAO.findAllByIdPlanifcation(idPlanification);
	}
	
	public List<ClientInter> findAllClientById(int idPlanification){
		System.out.println("je suis l�");
		SessionDAO sessionDAO = new SessionDAO();
		return sessionDAO.findAllById(idPlanification);
	}
	
	public Set<PlanificationFormation> findAllByIdFormateur(int idFormateur){
		System.out.println("je suis l�");
		SessionDAO sessionDAO = new SessionDAO();
		return sessionDAO.findAllByIdFormateur(idFormateur);
	}
}
