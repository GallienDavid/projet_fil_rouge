package logistique.modele.manager;

import java.util.List;

import logistique.modele.dao.SalleDAO;
import logistique.modele.objets.Salle;



public class SalleManager {
	
	public List<Salle> findAll() {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.findAll();
	}

	public Salle salleDepuisNom(String salleNom) {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.salleDepuisNom(salleNom);
	}

	public List<Salle> findDepuisZoneGeo(String zoneGeo) {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.salleDepuisZoneGeo(zoneGeo);
	}

	public Salle salleByID(int idSalle) {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.salleByID(idSalle);
	}
	
	public Salle salleByNOM(String idSalle) {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.salleByNOM(idSalle);
	}

}
