package logistique.modele.manager;

import java.util.List;

import logistique.modele.dao.ThemeDAO;
import logistique.modele.objets.Theme;



public class ThemeManager {
	
	public List<Theme> findAll() {
		ThemeDAO themeDAO = new ThemeDAO();
		return themeDAO.findAll();
	}

}
