package logistique.modele.manager;

import java.util.List;

import logistique.modele.dao.AgenceDAO;
import logistique.modele.objets.Agence;


public class AgenceManager {

	public List<Agence> findAll() {
		AgenceDAO agenceDAO = new AgenceDAO();
		return agenceDAO.findAll();
	}

	public List<Agence> findDepuisZoneGeo(String zoneGeo) {
		AgenceDAO agenceDAO = new AgenceDAO();
		return agenceDAO.findDepuisZoneGeo(zoneGeo);
	}
	
	
	
}
