package logistique.modele.manager;

import java.util.List;

import logistique.modele.dao.EventDAO;
import logistique.modele.objets.Event;


public class EventManager {
	
	public List<Event> findAll() {
		EventDAO eventDAO = new EventDAO();
		return eventDAO.findAll() ;
	}

}
