package logistique.modele.manager;

import java.util.List;

import logistique.modele.dao.PlanificationFormationDAO;
import logistique.modele.objets.Formation;
import logistique.modele.objets.PlanificationFormation;

public class PlanificationFormationManager {

	public void enregistrer(PlanificationFormation planificationFormation) {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		planificationFormationDAO.enregistrer(planificationFormation);
	}
	
	public List<PlanificationFormation> findAll() {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		System.out.println("bonjour je passe ici");
		return planificationFormationDAO.findAll();
	}

	public PlanificationFormation findById(Integer id) {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		return planificationFormationDAO.findById(id);
	}

	public List<PlanificationFormation> findByIdFormation(int idFormation) {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		return planificationFormationDAO.findByIdFormation(idFormation);
	}

	public List<PlanificationFormation> setDuree(List<PlanificationFormation> plani,List<Formation> forma) {
		System.out.println("je suis ici");
		for(int i = 0 ; i < plani.size() ; i++ ) {
			for(int j = 0 ; j < forma.size();j++) {
				if(forma.get(j).getId() == plani.get(i).getFormation().getId()) {
					plani.get(i).setDateFin(forma.get(j).duree());
					break;
				}
			}
					
		}
		return plani;
	}

	public List<PlanificationFormation> findAllWithoutModule(){
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		return planificationFormationDAO.findAllWithoutModule();
	}

	
	public List<PlanificationFormation> findAllWithoutOrder() {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		System.out.println("bonjour je passe ici");
		return planificationFormationDAO.findAllWithoutOrder();
	}

}
