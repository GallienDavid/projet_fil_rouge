package logistique.modele.manager;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import logistique.modele.dao.FormationDAO;
import logistique.modele.objets.Formation;


public class FormationManager {
	
	public void enregistrer(Formation formation) {
		FormationDAO formationDAO = new FormationDAO();
		// NB : on pourrait chiffrer le MDP ici, c'est le bon endroit
		formationDAO.enregistrer(formation);
	}

	public List<Formation> findAll() {
		FormationDAO formationDAO = new FormationDAO();
		return formationDAO.findAll();
	}

	public Formation FormationById(int idFormation) {
		
		FormationDAO formationDAO = new FormationDAO();
		
		return formationDAO.FormationById(idFormation);
	}

	
	Map<String,String> erreurs = new HashMap<>();
	
	public void verif(String param, String value) {
		if(value == null || value.isBlank()) {
			erreurs.put(param,value);
		}
	}
	
	public Map<String,String> verif(String nom, String prenom, List<String> nomModules ) {
		verif("nom",nom);
		verif("prenom",prenom);
		for(int i = 0 ; i < nomModules.size() ; i++) {
			verif("module"+i,nomModules.get(i));
		}
		
		return erreurs;
	}
	
	public void verifMail(String mail) {
		if(!(mail.trim().endsWith(".fr") || mail.endsWith(".com")) || mail== null || mail.isBlank()) {
			erreurs.put("mail", "L'email doit terminer par .com ou .fr");
		}
		
	}

	public Formation FormationByNom(String idFormation) {
		FormationDAO formationDAO = new FormationDAO();
		// NB : on pourrait chiffrer le MDP ici, c'est le bon endroit
		return formationDAO.FormationByNom(idFormation);
	}
	
	public Map<String,String> verifSession(String nom, String salle, String dateDebut, String cible) {
		verif("nom",nom);
		verif("salle",salle);
		verifDate(dateDebut,"dateDebut") ;
		verif("cible",cible);
		
		return erreurs ;
	}
	
	public void verifDate(String dateDebut,String param) {
		if(dateDebut == null || dateDebut.isBlank()|| LocalDate.now().isAfter(LocalDate.parse(dateDebut))) {
			erreurs.put(param,"A�e mauvaise date !");
		}
	}

}
