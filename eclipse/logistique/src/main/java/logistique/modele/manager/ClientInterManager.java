package logistique.modele.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import logistique.modele.dao.ClientInterDao;
import logistique.modele.objets.ClientInter;


public class ClientInterManager {

Map<String,String> erreurs = new HashMap<>();
	
	public Map<String,String> verif(String mail, String passwrd, String confirmPasswrd) {
		verifMail(mail);
		verifPasswrd(passwrd, confirmPasswrd);
		return erreurs;
	}

	
	public void verifMail(String mail) {
		if(!(mail.trim().endsWith(".fr") || mail.endsWith(".com"))) {
			erreurs.put("mail", "L'email doit terminer par .com ou .fr");
		}
	}
	
	public void verifPasswrd(String passwrd, String confirmPasswrd) {
		if(!passwrd.equals(confirmPasswrd)) {
			erreurs.put("confirmPasswrd", "Les mots de passes ne sont pas identiques");
		}
	}
	
	public void enregistre(ClientInter clientInter) {
		ClientInterDao clientInterDao = new ClientInterDao();
		clientInterDao.enregistrer(clientInter);
	}


	public ClientInter findByMailAndPasswrd(String mail, String passwrd) {
		ClientInterDao clientInterDao = new ClientInterDao();
		ClientInter clientInter = clientInterDao.findByMailAndPasswrd(mail, passwrd);
		return clientInter;
	
	}
	
	
	public List<ClientInter> findAll() {
		ClientInterDao clientInterDao = new ClientInterDao();
		List<ClientInter> clients = clientInterDao.findAll();
		return clients;
	
	}
	
	
	
}