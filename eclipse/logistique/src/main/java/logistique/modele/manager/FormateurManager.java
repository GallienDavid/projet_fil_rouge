package logistique.modele.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import logistique.modele.dao.FormateurDAO;
import logistique.modele.dao.FormationDAO;
import logistique.modele.dao.ModuleDAO;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.Formation;
import logistique.modele.objets.Module;

public class FormateurManager {
	
	public void enregistrer(Formateur formateur) {
		FormateurDAO formateurDAO = new FormateurDAO();
		// NB : on pourrait chiffrer le MDP ici, c'est le bon endroit
		FormateurDAO.enregistrer(formateur);
	}
	
	public List<Formateur> findAll() {
		FormateurDAO formateurDAO = new FormateurDAO();
		return formateurDAO.findAll();
	}
	
	public Formateur FormateurById(int id) {
		FormateurDAO formateurDAO = new FormateurDAO();
		return formateurDAO.findFormateurById(id);
	}

	public Formateur formateurDepuisNom(String nom) {
		FormateurDAO formateurDAO = new FormateurDAO();
		return formateurDAO.formateurDepuisNom(nom);
	}

	public List<Formateur> findDepuisModule(String nomModule) {
		FormateurDAO formateurDAO = new FormateurDAO();
		return formateurDAO.formateurDepuisModule(nomModule);
	}
	
	public List<Formateur> findDepuisIdModule(int nomModule) {
		FormateurDAO formateurDAO = new FormateurDAO();
		return formateurDAO.formateurDepuisIdModule(nomModule);
	}
	
	public void deleteById(int id) {
		FormateurDAO formateurDAO = new FormateurDAO();
		formateurDAO.deleteById(id);
	}
	
	Map<String,String> erreurs = new HashMap<>();
	
	public Map<String,String> verif(String nom,String prenom,String telephone,String zoneGeo,String tarif,String adresse, String mail, String module ) {
		verif("nom",nom);
		verif("prenom",prenom);
		verif("telephone",telephone);
		verif("zoneGeo",zoneGeo);
		verif("tarif",tarif);
		verif("adresse",adresse) ;
		verif("module",module);
		verifMail(mail);
		return erreurs;
	}
	
	public void verifMail(String mail) {
		if(!(mail.trim().endsWith(".fr") || mail.endsWith(".com")) || mail== null || mail.isBlank()) {
			erreurs.put("mail", "L'email doit terminer par .com ou .fr");
		}
		
	}
	
	public void verif(String param, String value) {
		if(value == null || value.isBlank()) {
			erreurs.put(param,value);
		}
	}

}
