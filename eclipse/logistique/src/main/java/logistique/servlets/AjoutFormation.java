package logistique.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.AgenceManager;
import logistique.modele.manager.FormationManager;
import logistique.modele.manager.ModuleManager;
import logistique.modele.manager.ResponsableFormationManager;
import logistique.modele.objets.Agence;
import logistique.modele.objets.Formation;
import logistique.modele.objets.Module;
import logistique.modele.objets.ResponsableFormation;

@WebServlet(urlPatterns = "/ajoutFormation")
public class AjoutFormation extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		
		ResponsableFormationManager responsableFormationManager = new ResponsableFormationManager();
		List<ResponsableFormation> responsableFormations = responsableFormationManager.findAll();
		// on stocke attribut la liste pour l'afficher sur la page d'admin
		req.setAttribute("responsableFormations", responsableFormations);
		
		
		ModuleManager moduleManager = new ModuleManager();
		List<Module> modules = moduleManager.findAll();
		// on stocke attribut la liste pour l'afficher sur la page d'admin
		req.setAttribute("modules", modules);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutFormation.jsp").forward(req, resp);
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ModuleManager moduleManager = new ModuleManager();
		ResponsableFormationManager responsableFormationManager = new ResponsableFormationManager() ;
		FormationManager formationManager = new FormationManager();
		
		// récupération des paramètres
		String nomFormation = request.getParameter("nomFormation");
		String SEPARATEUR = " ";
		String mots[] = request.getParameter("responsable").split(SEPARATEUR);
		List<String> nomModules = new ArrayList<>() ;
		nomModules.add(request.getParameter("module")) ;
		
		int i = 1 ;
		do {
			nomModules.add(request.getParameter("module"+i));
			System.out.println(nomModules.get(i));
			i++;
		}while(request.getParameter("module"+i) != null);
		
		Map<String,String> erreurs = formationManager.verif(nomFormation,mots[0],nomModules) ;
		
		if(erreurs.isEmpty()) {
			
			Module module = ModuleManager.moduleDepuisNom(request.getParameter("module"));
			
			List<Module> modules = new ArrayList<>();
			modules.add(module);
			
			int j = 1 ;
			do {
				modules.add(ModuleManager.moduleDepuisNom(request.getParameter("module"+j)));
				System.out.println(modules.get(j));
				j++;
			}while(request.getParameter("module"+j) != null);
			
			ResponsableFormation responsable = responsableFormationManager.responsableDepuisNom(mots[0]);
			Formation formation = new Formation() ;
			formation.setNbInscritMini(3);
			formation.setNom(nomFormation);
			formation.setResponsable(responsable);
			formation.setModules(modules);
				
			formationManager.enregistrer(formation);
			response.sendRedirect("connect/formation");
		}else {
			
			List<ResponsableFormation> responsableFormations = responsableFormationManager.findAll();
			// on stocke attribut la liste pour l'afficher sur la page d'admin
			request.setAttribute("responsableFormations", responsableFormations);
			
			
			List<Module> modules = moduleManager.findAll();
			// on stocke attribut la liste pour l'afficher sur la page d'admin
			request.setAttribute("modules", modules);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutFormation.jsp").forward(request, response);
			
		}
		
		
		
		
	}
}
