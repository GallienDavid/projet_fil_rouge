package logistique.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.FormationManager;
import logistique.modele.manager.PlanificationFormationManager;
import logistique.modele.objets.Formation;
import logistique.modele.objets.PlanificationFormation;


@WebServlet("/connect/calendrier")
public class Calendrier extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("coucou");
		PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager() ;
		FormationManager formationManager = new FormationManager() ;
		//List<PlanificationFormation> plani = planificationFormationManager.findAll();
		
		 List<PlanificationFormation> plani2 = planificationFormationManager.findAllWithoutModule();
		 
		 List<Formation> forma = formationManager.findAll();
		 
		 plani2 = planificationFormationManager.setDuree(plani2, forma);
		 System.out.println(plani2);
		 req.setAttribute("planifications", plani2);
		 
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/calendrier.jsp").forward(req, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// récupération des paramètres
		
		
		
		doGet(request, response);
		
		
		
	}

}
