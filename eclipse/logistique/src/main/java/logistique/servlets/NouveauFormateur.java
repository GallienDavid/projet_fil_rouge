package logistique.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.AgenceManager;
import logistique.modele.manager.FormateurManager;
import logistique.modele.manager.ModuleManager;
import logistique.modele.objets.Agence;
import logistique.modele.manager.ModuleManager;

import logistique.modele.objets.Formateur;
import logistique.modele.objets.Module;

@WebServlet(urlPatterns = "/nouveauFormateur")
public class NouveauFormateur extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		ModuleManager moduleManager = new ModuleManager();
		List<Module> modules = moduleManager.findAll();
		// on stocke attribut la liste pour l'afficher sur la page d'admin
		req.setAttribute("modules", modules);
		AgenceManager agenceManager = new AgenceManager();
		List<Agence> agences = agenceManager.findAll();
		// on stocke attribut la liste pour l'afficher sur la page d'admin
		req.setAttribute("agences", agences);
		this.getServletContext().getRequestDispatcher("/WEB-INF/nouveauFormateur.jsp").forward(req, resp);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email = request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String zoneGeo = request.getParameter("agence");
		String adresse = request.getParameter("adresse") + " " + request.getParameter("ville") + " " + request.getParameter("pays") + " " + request.getParameter("codepostal") ;
		String tarif = request.getParameter("tarif");

		ModuleManager moduleManager = new ModuleManager();
		FormateurManager formateurManager = new FormateurManager();
		
		
		Map<String,String> erreurs = formateurManager.verif(nom,prenom, telephone,zoneGeo,tarif,adresse,email,request.getParameter("module"));
		
		
		if(erreurs.isEmpty()) {
			Module module = moduleManager.moduleDepuisNom(request.getParameter("module"));
			float tarif2 = Float.valueOf(request.getParameter("tarif"));
			//System.out.println(adresse);
			// encapsulation de la verification dans un manager	
			Formateur formateur = new Formateur(nom, prenom,email,adresse,module,telephone,zoneGeo,tarif2);
			System.out.println(formateur);
			
			
			formateurManager.enregistrer(formateur);
			response.sendRedirect("connect/formateur");
			
		}else {
			
			List<Module> modules = moduleManager.findAll();
			// on stocke attribut la liste pour l'afficher sur la page d'admin
			request.setAttribute("modules", modules);
			AgenceManager agenceManager = new AgenceManager();
			List<Agence> agences = agenceManager.findAll();
			// on stocke attribut la liste pour l'afficher sur la page d'admin
			request.setAttribute("agences", agences);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/nouveauFormateur.jsp").forward(request, response);
		}
	
		

	}
}