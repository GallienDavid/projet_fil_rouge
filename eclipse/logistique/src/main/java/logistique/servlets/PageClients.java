package logistique.servlets;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.ClientInterManager;
import logistique.modele.manager.ClientIntraManager;
import logistique.modele.objets.ClientInter;
import logistique.modele.objets.ClientIntra;

@WebServlet("/connect/client")
public class PageClients extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		ClientInterManager clientInterManager = new ClientInterManager();
		List<ClientInter> clientsInter = clientInterManager.findAll();
		System.out.println(clientsInter);
		req.setAttribute("clientsInter", clientsInter);
		ClientIntraManager clientIntraManager = new ClientIntraManager();
		List<ClientIntra> clientsIntra = clientIntraManager.findAll();
		System.out.println(clientsIntra);
		req.setAttribute("clientsIntra", clientsIntra);
		this.getServletContext().getRequestDispatcher("/WEB-INF/pageClients.jsp").forward(req, resp);
	}

}
