package logistique.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.AgenceManager;
import logistique.modele.manager.FormateurManager;
import logistique.modele.manager.FormationManager;
import logistique.modele.manager.PlanificationFormationManager;
import logistique.modele.manager.SalleManager;
import logistique.modele.objets.Agence;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.Formation;
import logistique.modele.objets.Module;
import logistique.modele.objets.PlanificationFormation;
import logistique.modele.objets.Salle;
import logistique.modele.objets.Session;

@WebServlet(urlPatterns = "/infosSession")
public class InfosSession extends HttpServlet{
	
	
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("Je rentre dans le get de infosSession");
		AgenceManager agenceManager = new AgenceManager();
		List<Agence> agences = agenceManager.findAll();
		req.setAttribute("agences", agences);
		
		SalleManager salleManager = new SalleManager();
		List<Salle> salles = salleManager.findAll();
		req.setAttribute("salles", salles);
		
		FormationManager formationManager = new FormationManager();
		List<Formation> formations = formationManager.findAll();
		req.setAttribute("formations", formations);
		
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/infosSession.jsp").forward(req, resp);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FormationManager formationManager = new FormationManager();

		SalleManager salleManager = new SalleManager();
		String idFormation = request.getParameter("nomFormation");
		//String idSalle = request.getParameter("salle");
		
		String SEPARATEUR = " - ";
		String mots[] = request.getParameter("salle").split(SEPARATEUR);

		String dateDebut = request.getParameter("dateDebut");
		String cible = request.getParameter("cible");
		
		
		
		Map<String,String> erreurs = formationManager.verifSession(idFormation,mots[0],dateDebut,cible);
		
		
		if(erreurs.isEmpty()) {
			//Salle salle = salleManager.salleByID(idSalle);
			Salle salle = salleManager.salleByNOM(mots[1]);
			
			
			//Formation formation = formationManager.FormationById(idFormation);
			Formation formation = formationManager.FormationByNom(idFormation);
			
			
			
			PlanificationFormation planificationFormation = new PlanificationFormation(formation, salle, dateDebut, cible);
			PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager();
			planificationFormationManager.enregistrer(planificationFormation);
			
			List<PlanificationFormation> planificationFormations = planificationFormationManager.findAllWithoutOrder();
			System.out.println("la taille de planification");
			System.out.println(planificationFormations.size());
			int last = planificationFormations.size() ;
			last = last - 1 ;
			System.out.println(last);
			request.setAttribute("planificationFormation", planificationFormations.get(last));
			System.out.println(planificationFormations.get(last));
			
			List<Module> modules = new ArrayList<Module>();
			for(Module module : planificationFormation.getFormation().getModules()) {
				modules.add(module);
				
			}
			request.setAttribute("modules", modules);
			
			FormateurManager formateurManager = new FormateurManager();
			List<Formateur> formateurs = formateurManager.findAll();
			System.out.println(formateurs);
			request.setAttribute("formateurs", formateurs);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/nouvelleFormation.jsp").forward(request, response);
			
		}else {
			
			System.out.println("Je rentre dans le get de infosSession");
			AgenceManager agenceManager = new AgenceManager();
			List<Agence> agences = agenceManager.findAll();
			request.setAttribute("agences", agences);
			
			List<Salle> salles = salleManager.findAll();
			request.setAttribute("salles", salles);
			
	
			List<Formation> formations = formationManager.findAll();
			request.setAttribute("formations", formations);
			request.setAttribute("erreurs", erreurs);
			System.out.println(erreurs.get("dateDebut"));
			
			
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/infosSession.jsp").forward(request, response);
			
		}
		
	}

}