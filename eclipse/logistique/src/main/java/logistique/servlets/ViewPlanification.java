package logistique.servlets;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.FormationManager;
import logistique.modele.manager.PlanificationFormationManager;
import logistique.modele.manager.SessionManager;
import logistique.modele.objets.ClientInter;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.Formation;
import logistique.modele.objets.PlanificationFormation;
@WebServlet(urlPatterns = "/viewPlanification")
public class ViewPlanification extends HttpServlet {
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idFormation = Integer.valueOf(request.getParameter("idformation"));
		System.out.println(idFormation);
		
		
		SessionManager sessionManager = new SessionManager();
		List<Formateur> formateurs = sessionManager.findAllByIdPlanification(idFormation);
		System.out.println(formateurs.size());
		PlanificationFormationManager planificationManager = new PlanificationFormationManager();
		
		PlanificationFormation planiFormation = planificationManager.findById(idFormation) ;
		
		List<ClientInter> clients = sessionManager.findAllClientById(idFormation);
		
	
		request.setAttribute("plani", planiFormation);
		request.setAttribute("formateurs", formateurs);
		float cout= 0 ;
		for(int i = 0 ; i < formateurs.size();i++) {
			cout = cout+ (formateurs.get(i).getSalaire() * formateurs.get(i).getModule().getNombreJour());
		}
		request.setAttribute("cout", cout);
		
		request.setAttribute("clients", clients);
		
		float recette = (clients.size()*planiFormation.getFormation().prix()) - cout ;
		
		request.setAttribute("recette", recette);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/viewPlanification.jsp")
		.forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
	}

}
