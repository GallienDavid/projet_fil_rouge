package logistique.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.FormateurManager;
import logistique.modele.manager.ModuleManager;
import logistique.modele.manager.PlanificationFormationManager;
import logistique.modele.manager.SessionManager;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.Module;
import logistique.modele.objets.PlanificationFormation;
import logistique.modele.objets.Session;

@WebServlet(urlPatterns = "/nouvelle-formation")
public class NouvelleFormation extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/nouvelleFormation.jsp").forward(req, resp);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		ModuleManager moduleManager = new ModuleManager();
		List<Module> modules = new ArrayList<Module>();
		modules = moduleManager.findAll();
		
		System.out.println(req.getParameter("planificationFormationId"));
		
		for (Module module : modules) {
			if(req.getParameter(module.getNomModule()) != null) {
				FormateurManager formateurManager = new FormateurManager();
				Formateur formateur = formateurManager.FormateurById(Integer.valueOf(req.getParameter(module.getNomModule())));
				
				PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager();
				PlanificationFormation planificationFormation = planificationFormationManager.findById(Integer.valueOf(req.getParameter("planificationFormationId")));
				System.out.println(planificationFormation.getId());
				Session session = new Session(planificationFormation,formateur);
				SessionManager sessionManager = new SessionManager();
				sessionManager.enregistrer(session);
				System.out.println(session);
			}
		}
		
		//FormateurManager formateurManager = new FormateurManager();
		//Formateur formateur = formateurManager.FormateurById(Integer.valueOf(req.getParameter("moduleFormateur")));
		
		
		//SessionManager sessionManager = new SessionManager();
		//sessionManager.enregistrer(session);
		resp.sendRedirect("connect/calendrier");	
	}
	
}