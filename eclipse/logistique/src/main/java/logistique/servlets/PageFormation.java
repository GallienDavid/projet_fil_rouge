package logistique.servlets;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.FormationManager;
import logistique.modele.manager.ModuleManager;
import logistique.modele.objets.Formation;
import logistique.modele.objets.Module;

@WebServlet("/connect/formation")
public class PageFormation extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		FormationManager formationManager = new FormationManager();
		
		List<Formation> formations = formationManager.findAll();
		/*for (int i = 0; i < formations.size(); i++) {
			System.out.println(formations.get(i));
			System.out.println(formations.get(i).duree());
			System.out.println(formations.get(i).prix());
		}*/
		
		// on stocke attribut la liste pour l'afficher sur la page d'admin
		req.setAttribute("formations", formations);
		this.getServletContext().getRequestDispatcher("/WEB-INF/pageFormation.jsp").forward(req, resp);
	}

}
