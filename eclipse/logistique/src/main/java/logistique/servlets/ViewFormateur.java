package logistique.servlets;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.Set;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.FormateurManager;
import logistique.modele.manager.ModuleManager;
import logistique.modele.manager.PlanificationFormationManager;
import logistique.modele.manager.SessionManager;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.Module;
import logistique.modele.objets.PlanificationFormation;

@WebServlet(urlPatterns = "/viewFormateur")
public class ViewFormateur extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//doPost(request,response);
		System.out.println("Je suis l�");
		System.out.println(request.getParameter("formateur"));
		int idFormateur = Integer.valueOf(request.getParameter("formateur"));
		//System.out.println(idFormateur);
		FormateurManager formateurManager = new FormateurManager();
		Formateur formateur = formateurManager.FormateurById(idFormateur);
		SessionManager sessionManager = new SessionManager();
		
		HashSet<PlanificationFormation> planiFormation = (HashSet<PlanificationFormation>) sessionManager.findAllByIdFormateur(idFormateur) ;
		System.out.println(planiFormation);
		//System.out.println(formateur);	
		request.setAttribute("formateur", formateur);
		request.setAttribute("planis", planiFormation);
		this.getServletContext().getRequestDispatcher("/WEB-INF/viewFormateur.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// r�cup�ration des param�tres
		
		
		
		doGet(request, response);
		
		
		
	}
	
	
}
