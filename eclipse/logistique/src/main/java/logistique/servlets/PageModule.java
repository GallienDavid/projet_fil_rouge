package logistique.servlets;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.ModuleManager;
import logistique.modele.objets.Module;

@WebServlet("/connect/module")
public class PageModule extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ModuleManager moduleManager = new ModuleManager();
		List<Module> modules = moduleManager.findAll();
		// on stocke attribut la liste pour l'afficher sur la page d'admin
		System.out.println(modules);
		req.setAttribute("modules", modules);
		this.getServletContext().getRequestDispatcher("/WEB-INF/pageModule.jsp").forward(req, resp);
	}

}
