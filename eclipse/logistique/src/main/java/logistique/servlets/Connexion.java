package logistique.servlets;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.ConnexionManager;
import logistique.modele.manager.FormationManager;
import logistique.modele.manager.PlanificationFormationManager;
import logistique.modele.objets.Formation;
import logistique.modele.objets.PlanificationFormation;
import logistique.modele.objets.User;

@WebServlet(urlPatterns = "/connexion")
public class Connexion extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(req, resp);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String mail = request.getParameter("username");
		String passwrd = request.getParameter("passwrd");
		
		System.out.println(mail);
		System.out.println(passwrd);
		
		ConnexionManager connexionManager = new ConnexionManager();
		User user = connexionManager.findByUsername(mail, passwrd);
		System.out.println(user);
		
		if(user == null) {
			request.setAttribute("message", "Mail ou mot de passe invalide");
			this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp")
			.forward(request, response);
			
		}else {
			
			
			PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager() ;
			FormationManager formationManager = new FormationManager() ;
			//List<PlanificationFormation> plani = planificationFormationManager.findAll();
			
			 List<PlanificationFormation> plani2 = planificationFormationManager.findAllWithoutModule();
			 
			 List<Formation> forma = formationManager.findAll();
			 
			 plani2 = planificationFormationManager.setDuree(plani2, forma);
			 System.out.println(plani2);
			 request.setAttribute("planifications", plani2);
			 
			
			
			request.getSession().setAttribute("user", user);
			this.getServletContext().getRequestDispatcher("/WEB-INF/calendrier.jsp").forward(request, response);
		}
		
		
		//response.sendRedirect("espaceClient");
}

}
