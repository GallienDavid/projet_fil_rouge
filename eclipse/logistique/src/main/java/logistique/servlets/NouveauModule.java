package logistique.servlets;

import java.io.IOException;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.ModuleManager;
import logistique.modele.objets.Module;

@WebServlet("/nouveauModule")
public class NouveauModule extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/nouveauModule.jsp")
		.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// récupération des paramètres
		String nomModule = request.getParameter("nomModule");
		String prix = request.getParameter("prix");
		String nombreJour = request.getParameter("nombreJour");
		System.out.println(request.getParameter("nomModule").isBlank());
	
		
		ModuleManager moduleManager = new ModuleManager();
		Map<String,String> erreurs = moduleManager.verif(nomModule, nombreJour, prix);
		System.out.println(nomModule);
		System.out.println(prix);
		System.out.println(erreurs.get("nom"));
		System.out.println(erreurs.get("prix"));
		System.out.println(erreurs.get("duree"));
		
		if(erreurs.isEmpty()) {
			Module module = new Module(nomModule, prix, nombreJour);
			moduleManager.enregistrer(module);
			
			response.sendRedirect("connect/module");
			
		}else {
			request.setAttribute("erreurs", erreurs);
			this.getServletContext().getRequestDispatcher("/WEB-INF/nouveauModule.jsp")
			.forward(request, response);	
		}
		
		
	}

}
