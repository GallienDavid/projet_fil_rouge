package logistique.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.FormateurManager;
import logistique.modele.manager.FormationManager;
import logistique.modele.manager.PlanificationFormationManager;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.Formation;
import logistique.modele.objets.PlanificationFormation;

import java.io.IOException;
import java.util.List;
@WebServlet(urlPatterns = "/viewFormation")
public class ViewFormation extends HttpServlet {
	
  
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idFormation = Integer.valueOf(request.getParameter("idformation"));
		System.out.println(idFormation);
		
		FormationManager formationManager = new FormationManager();
		Formation formation = formationManager.FormationById(idFormation);
		PlanificationFormationManager planificationManager = new PlanificationFormationManager();
		List<PlanificationFormation> planiFormation = planificationManager.findByIdFormation(idFormation) ;
		
		
		
		
		request.setAttribute("formation", formation);
		request.setAttribute("plani", planiFormation);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/viewFormation.jsp")
		.forward(request, response);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
	}

}
