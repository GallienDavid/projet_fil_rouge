package logistique.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.FormateurManager;

import java.io.IOException;

@WebServlet(urlPatterns = "/deleteFormateur")
public class DeleteFormateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idFormateur = Integer.valueOf(request.getParameter("id"));
		System.out.println(idFormateur);
		
		FormateurManager formateurManager = new FormateurManager();
		formateurManager.deleteById(idFormateur);
		response.sendRedirect("connect/formateur");	
		

		
	}

}
