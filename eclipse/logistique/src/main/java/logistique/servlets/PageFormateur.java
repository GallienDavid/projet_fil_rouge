package logistique.servlets;

import java.io.IOException;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import logistique.modele.manager.FormateurManager;
import logistique.modele.manager.FormationManager;
import logistique.modele.manager.ModuleManager;
import logistique.modele.objets.Formateur;
import logistique.modele.objets.Formation;
import logistique.modele.objets.Module;

@WebServlet("/connect/formateur")
public class PageFormateur extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		FormateurManager formateurManager = new FormateurManager();
		List<Formateur> formateurs = formateurManager.findAll();
		
		/*for (int i = 0; i < formateurs.size(); i++) {
			System.out.println(formateurs.get(i));
			
		}*/
		// on stocke attribut la liste pour l'afficher sur la page d'admin
		req.setAttribute("formateurs", formateurs);
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/pageFormateur.jsp").forward(req, resp);
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// récupération des paramètres
		/*int idFormateur = Integer.valueOf(request.getParameter("formateur"));
		System.out.println(idFormateur);
		FormateurManager formateurManager = new FormateurManager();
		Formateur formateur = formateurManager.FormteurById(idFormateur);
		System.out.println(formateur);*/
		
		
		
		
		response.sendRedirect("viewFormateur");	
		
		
	}

}
