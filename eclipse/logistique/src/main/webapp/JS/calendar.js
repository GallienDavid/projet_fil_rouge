document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth' ,
                locale: 'fr',
                height: 650,
                eventSources: [

                    // your event source
                    {
                      url: 'http://localhost:8080/calendars',
                      method: 'GET',
                      failure: function() {
                        alert('there was an error while fetching events!');
                      }
                    }

                    // any other sources...

                  ],
                headerToolbar: {
                    left: 'prev,next myCustomButton',
                    center: 'title',
                    right: 'dayGridMonth'
                },
                eventClick: function(info) {
                    alert('Formation : ' + info.event.title + '\n' +info.event.extendedProps.description);

                }

            });
            calendar.render();
        });
