<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <link rel="shortcut icon" href="<c:url value="/image/session.ico"/>" type="image/x-icon">
<title>Créer un nouveau module</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/sidebarForAll.css">
    <link rel="stylesheet" href="CSS/interfaceFormation.css">
    <link rel="stylesheet" href="CSS/formation.css">
	<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">
<br>
<h1 class="NewFormation" >Nouveau Module</h1> 
<br>
<h5 class="NewFormation" >Veuillez renseigner les champs du module à rajouter</h5>
<br>
<form action="${pageContext.request.contextPath}/nouveauModule" method="post">
	<div class="col-md-2">
		<label for="nomModule" class="form-label">Nom du module :</label>
		<input type="text" id="nomModule" name="nomModule" class="form-control" value="${param.nomModule}"><br/>
	</div>
	<div class="col-md-2">
	<label for="prix" class="form-label">Prix :</label>
	<input type="number" step="0.01" id="prix" name="prix" class="form-control" value="${param.prix}" ><br/>
	</div>
	<div class="col-md-2">
	<label for="nombreJour" class="form-label" >Nombre de jour :</label>
	<input type="number" step="1" id="nombreJour" name="nombreJour" class="form-control" value="${param.nombreJour}"><br/>
	</div>
	<input type="submit" value="Ajouter" class=" btn btn-danger"/>
</form>
</section>
</body>
</html>