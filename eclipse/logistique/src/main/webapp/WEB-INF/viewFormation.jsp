<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="<c:url value="/image/session.ico"/>" type="image/x-icon">
<title>${formation.nom}</title>
   
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="CSS/sidebarForAll.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="CSS/interfaceFormateur.css" rel="stylesheet">
    <link rel="stylesheet" href="CSS/idformateur.css">
    
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">

    <br>
    <div id="ajoutFormateur">
        <div class="text"> <h1 id="NewFormateur">${formation.nom}</h1></div>
        <br>
        <br>
        <form method="post" action="">
            <h5>Informations sur la formation</h5>
            <br>
            <div class="row g-3">
                <div class="col">
                	<h5>Prix :</h5>
                    <p class="cadre">${formation.prix()} €</p>
                </div>
                <div class="col">
               	 	<h5>Durée :</h5>
                    <p class="cadre">${formation.duree()} Jours </p>
                </div>

            </div>
            <br>
    </div>
    <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#id</th>
            <th scope="col">Nom</th>
            <th scope="col">Durée</th>
            <th scope="col">Prix</th>
        </tr>
        </thead>
         <c:forEach items="${formation.modules}" var="module">
        <tr>
            <td><a>${module.ID_MODULE }</a></td>
            <td>${module.nomModule }</td>
            <td>${module.nombreJour } Jours</td>
            <td>${module.prix } € </td>

        </tr>
        </c:forEach>
        
    </table>
    </form>
    <table class="table">
            <thead>
            <tr>
                <th scope="col">#id</th>
                <th scope="col">Nom</th>
                <th scope="col">Date de début</th>
                <th scope="col">Date de fin</th>
                <th scope="col">Agence</th>
                <th scope="col">Responsable</th>
                <th scope="col">Type</th>
                <th scope="col">Salle</th>
                <th scope="col">Nombre d'inscrit(s)</th>
                <th scope="col">Validé</th>
                <th scope="col">État </th>

            </tr>
            </thead>
            <c:forEach items="${plani}" var="planificationsCourant">
            <tr>
                <tr><td><form action="${pageContext.request.contextPath}/viewPlanification" method="post"><input type="submit" value= "${planificationsCourant.id }" name="idformation" style="border:0px #000 solid;background-color:#fff;text-decoration : underline"/></form></td>
                <td>${planificationsCourant.formation.nom}</td>
                <td>${planificationsCourant.dateDebut}</td>
                <td>${planificationsCourant.dateFin()}</td>
                <td>${planificationsCourant.salle.agence.zoneGeo}</td>
                <td>${planificationsCourant.formation.responsable.nom}</td>
                <td> <c:if test="${planificationsCourant.cible == 0}">
                Intra
                </c:if>
                <c:if test="${planificationsCourant.cible == 1}">
                Inter
                </c:if></td>
                <td>${planificationsCourant.salle.nom}</td>
                <td>${planificationsCourant.nbInscrit}</td>
                <td>
                <c:if test="${planificationsCourant.nbInscrit < 3}">
                Non
                </c:if>
                <c:if test="${planificationsCourant.nbInscrit >= 3}">
                Oui
                </c:if>
                
                </td>
                <td>${planificationsCourant.enCours() }
                </td>

            </tr>
            </c:forEach>
        </table>

</section>

</body>
</html>