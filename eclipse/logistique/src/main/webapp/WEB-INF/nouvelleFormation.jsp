<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="<c:url value="/image/session.ico"/>" type="image/x-icon">
<title>Sélection des formateurs</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<link rel="stylesheet" href="CSS/sidebarForAll.css">
<link rel="stylesheet" href="CSS/interfaceFormation.css">
<!-- Boxicons CDN Link -->
<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css'
	rel='stylesheet'>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<%@ include file="sidebar.jspf"%>

	<section class="home-section">

		<br>
		<div id="ajoutFormateur">
			<div class="text">
				<h1 class="NewFormation">Planification formation</h1>
			</div>
			<br> <br>
			<form method="post" action="nouvelle-formation">
				<h5>Information Session</h5>
				<c:forEach items="${ modules }" var="moduleP">
					<label for="moduleFormateur" class="form-label">${moduleP.nomModule }</label>
					
					<select id="moduleFormateur" name="${moduleP.nomModule}" class="form-control">
						<c:forEach items="${formateurs}" var="formateur">
							<c:if
								test="${formateur.module.nomModule.equals(moduleP.nomModule)}"> 
							<option value="${formateur.id }">${formateur.nom } ${formateur.prenom }</option>
							</c:if>
						</c:forEach>
					</select>
				</c:forEach>
				<br> <input type="submit"  class="btn btn-danger" value="Créer les sessions">
				<input type="hidden" value="${planificationFormation.id}" name="planificationFormationId">
			</form>
		</div>


	</section>



</body>
</html>