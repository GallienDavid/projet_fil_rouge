<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<c:url value="/image/session.ico"/>" type="image/x-icon">
    <title>Planification formation</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/sidebarForAll.css">
    <link rel="stylesheet" href="CSS/interfaceFormation.css">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<%@ include file="sidebar.jspf" %>

<section class="home-section">

    <br>
    <div id="ajoutFormateur">
        <div class="text"> <h1 class="NewFormation">Renseigner les informations de la session</h1></div>
        <br>
        <br>
        <form method="post" action="infosSession">
            <h5>Information Session</h5>
            <br>
            <div class="row g-3">
               
                <div class="col">
                    <input type="date" class="form-control" placeholder="Date de début" aria-label="Last name" name="dateDebut">
                	<p style="color: red">${erreurs.get("dateDebut")}</p>
                </div>
                
            </div>
            <br>
            <div class="row g-3">
                <div class="col">
                     <label for="salle" class="form-label">Salle</label>
                    <input class="form-control" list="sallelistOptions" id="salle" name="salle">
                	<datalist id="sallelistOptions">
                		<c:forEach items="${salles}" var="salleCourante">
                		<option> ${salleCourante.agence.zoneGeo} - ${ salleCourante.nom }</option>
                		</c:forEach>
                	</datalist>
                </div>
            </div>
            <br/>
            <div class="row g-3">
                <div class="col">
                     <label for="formation" class="form-label">Formation</label>
                    <input class="form-control" list="formationlistOptions" id="formation" name="nomFormation">
                	<datalist id="formationlistOptions">
                		<c:forEach items="${formations}" var="formationCourant">
                		<option value="${formationCourant.nom}"> ${formationCourant.nom}</option>
                		</c:forEach>
                	</datalist>
                </div>
            </div>
            <br>
            <br>
            <div class="col-md-2">
                    <label class="form-label" >Type</label>
                    <select class="form-select" name="cible">
                        <option> Intra</option>
                        <option> Inter</option>
                    </select>

                </div>
            <br>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <button type="submit" class="btn btn-danger">Plannifier la session</button>
            </div>
        </form>

    </div>


</section>



</body>
</html>