<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${plani.id } | ${plani.formation.nom} | ${plani.dateDebut}</title>

<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="CSS/sidebarForAll.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="CSS/interfaceFormateur.css" rel="stylesheet">
    <link rel="stylesheet" href="CSS/idformateur.css">
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">

    <br>
    <div id="ajoutFormateur">
        <div class="text"> <h1 id="NewFormateur">${plani.formation.nom} du ${plani.dateDebut} au ${plani.dateFin()} </h1></div>
        <br>
        <br>
            <h5>Informations sur la formation</h5>
            <br>
            <div class="row g-3">
                <div class="col">
                	<h5>Prix :</h5>
                    <p class="cadre">${plani.formation.prix()} €</p>
                </div>
                <div class="col">
               	 	<h5>Durée :</h5>
                    <p class="cadre">${plani.formation.duree()} Jours </p>
                </div>

            </div>
            <br>
            <div class="row g-3">
                <div class="col">
                	<h5>Recette</h5>
                	<c:if test="${recette > 0}">
                   		 <p class="cadre">${recette} €</p>
                   	</c:if>
                   	<c:if test="${recette < 0 }">
                   		 <p class="cadre" id="aie">${recette} €</p>
                   	</c:if>
                </div>

            </div>
            
    </div>
    <br>
    <h1> Formateurs </h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#id</th>
            <th scope="col">Nom</th>
            <th scope="col">Module</th>
            <th scope="col">Tarif</th>
            <th scope="col">Nb jours</th>
        </tr>
        </thead>
         <c:forEach items="${formateurs}" var="formateur">
        <tr>
            <td><form action="${pageContext.request.contextPath}/viewFormateur" method="post"><input type="submit" value= "${formateur.id}" name="formateur" style="border:0px #000 solid;background-color:#fff;text-decoration : underline"/></form></td>
            <td>${formateur.nom }</td>
            <td>${formateur.module.nomModule }</td>
            <td>${formateur.salaire } €</td>
            <td>${formateur.module.nombreJour }</td>
        </tr>
        </c:forEach>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>${cout} €</td>
            <td></td>
        </tr>
    </table>
    
<h1> Inscrit(s) </h1>
     <table class="table">
	            <thead>
	            <tr>
	                <th scope="col">#id</th>
	                <th scope="col">Nom</th>
	                <th scope="col">Prénom</th>
	                <th scope="col">Mail</th>
	                <th scope="col">Téléphone</th>
	            </tr>
	            </thead>
	            <c:forEach items="${clients}" var="clientsInterCourant">
	            	<tr>
		                <td>${clientsInterCourant.id_Client }</td>
		                <td>${clientsInterCourant.nom }</td>
		                <td>${clientsInterCourant.prenom }</td>
		                <td>${clientsInterCourant.mail }</td>
		                <td>${clientsInterCourant.telephone } </td>
	            	</tr>
            	</c:forEach>
	  </table>
    </form>
</section>


</body>
</html>