<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="<c:url value="/image/calendar.ico"/>" type="image/x-icon">
<title>Calendrier</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.css' rel='stylesheet' />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value="/CSS/sidebarForAll.css"/>">
    <link rel="stylesheet" href="<c:url value="/CSS/interfaceFormation.css"/>">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<c:url value="/CSS/fullcalendar/main.css"/>" rel='stylesheet' />
    <link href="<c:url value="/CSS/fullcalendar.css"/>" rel='stylesheet' />

    <link href='https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.1/css/all.css' rel='stylesheet'>
    <script src='${pageContext.request.contextPath}/CSS/fullcalendar/main.js'> </script>
    <script src='${pageContext.request.contextPath}/CSS/fullcalendar/locales-all.js'></script>
	<script src='${pageContext.request.contextPath}/JS/calendar.js'> </script>
	
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">
    <div>
        <div id='calendar' class ='test'></div>
    </div>
    <br>
    <div id="connexion">
        <br>
        <br>
        <br>
        <form method="get" action="utilisateur.java">
            <div class="row g-3">
                <div class="col-md-2">
                    <label class="form-label" >Nom</label>
                    <input class="form-control" type="text" name="search" placeholder="Search...">
                </div>
                <div class="col-md-2">
                    <label class="form-label" >Agence</label>
                    <select class="form-select" aria-label="Disabled select example">
                        <option selected>None</option>
                        <option value="1">Ile de France</option>
                        <option value="2">Grand Ouest</option>
                        <option value="3">Sud Ouest</option>
                        <option value="4">Nord Est</option>
                        <option value="5">Nord</option>
                        <option value="6">Sud Est</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="inputState" class="form-label">Module</label>

                    <input class="form-control" list="datalistOptions" id="exampleDataList" placeholder="Type to search...">
                    <datalist id="datalistOptions">
                        <option> JAVA</option>
                        <option> Python</option>
                        <option> HTML</option>
                        <option> CSS</option>
                    </datalist>
                </div>
                <div class="col-md-2">
                    <label class="form-label" >Type</label>
                    <select class="form-select" >
                        <option> None</option>
                        <option> Intra</option>
                        <option> Inter</option>
                    </select>

                </div>
                <div class="col-md-2">
                    <label class="form-label" >Validé</label>
                    <select class="form-select" >
                        <option> None</option>
                        <option> Oui</option>
                        <option> Non</option>
                    </select>

                </div>
                <div class="col-md-2">
                    <div id="nouveau-formateur">
                        <a type="button" class="btn btn-warning" href="${pageContext.request.contextPath}/infosSession">
                            <i class='bx bxs-book-content' ></i>
                            <span>Nouvelle formation</span>
                        </a>
                    </div>
                </div>

            </div>
        </form>
        <br>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#id</th>
                <th scope="col">Nom</th>
                <th scope="col">Date de début</th>
                <th scope="col">Date de fin</th>
                <th scope="col">Agence</th>
                <th scope="col">Responsable</th>
                <th scope="col">Type</th>
                <th scope="col">Salle</th>
                <th scope="col">Nombre d'inscrit(s)</th>
                <th scope="col">Validé</th>
                <th scope="col">État</th>

            </tr>
            </thead>
            <c:forEach items="${planifications}" var="planificationsCourant">
            <tr><td><form action="${pageContext.request.contextPath}/viewPlanification" method="post"><input type="submit" value= "${planificationsCourant.id }" name="idformation" style="border:0px #000 solid;background-color:#fff;text-decoration : underline"/></form></td>
                <td>${planificationsCourant.formation.nom}</td>
                <td>${planificationsCourant.dateDebut}</td>
                <td>${planificationsCourant.dateFin() }</td>
                <td>${planificationsCourant.salle.agence.zoneGeo}</td>
                <td>${planificationsCourant.formation.responsable.nom}</td>
                <td> <c:if test="${planificationsCourant.cible == 0}">
                Intra
                </c:if>
                <c:if test="${planificationsCourant.cible == 1}">
                Inter
                </c:if></td>
                <td>${planificationsCourant.salle.nom}</td>
                <td>${planificationsCourant.nbInscrit}</td>
                <td>
                <c:if test="${planificationsCourant.nbInscrit < 3}">
                Non
                </c:if>
                <c:if test="${planificationsCourant.nbInscrit >= 3}">
                Oui
                </c:if>
                
                </td>
                <td>${planificationsCourant.enCours() }
                </td>

            </tr>
            </c:forEach>
        </table>
     </div>
    </div>
</section>
</body>
</html>