<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
 	<link rel="shortcut icon" href="<c:url value="/image/formateur.ico"/>" type="image/x-icon">
	<title>Nouveau formateur</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="CSS/ajoutFormateur.css">
    <link rel="stylesheet" href="CSS/sidebarForAll.css">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>

</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">

    <br>
    <div id="ajoutFormateur">
        <div class="text"> <h1 id="NewFormateur">Nouveau formateur</h1></div>
        <br>
        <br>
        <form method="post" action="nouveauFormateur">
            <h5>Informations personnelles</h5>
            <br>
            <div class="row g-3">
                <div class="col">
                    <input type="text" class="form-control" placeholder="Nom" id="nom" name="nom" value="${param.nom}">
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Prénom" id="prenom" name="prenom" value="${param.Prenom}">
                </div>

            </div>
            <br>

            <div class="col-md-6">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" name="email" value="${param.email}">
            </div>
            <div class="col-md-6">
                <label for="telephone" class="form-label">Téléphone</label>
                <input type="text" class="form-control" id="telephone" name="telephone" value="${param.telephone}">
            </div>
            <div class="col-12">
                <label for="adresse" class="form-label">Addresse</label>
                <input type="text" class="form-control" id="adresse" name="adresse">
            </div>
            <div class="row g-3">
                <div class="col-md-6">
                    <label for="ville" class="form-label">Ville</label>
                    <input type="text" class="form-control" id="ville" name="ville">
                </div>
                <div class="col-md-4">
                    <label for="pays" class="form-label">Pays</label>
                    <input id="pays" class="form-control" name="pays">
                        
                </div>
            </div>
            <div class="row g-3">
            
	            <div class="col-md-2">
	            <label for="codepostal" class="codepostal" id="labZip">Code postal</label>
	                <input type="text" class="form-control" id="codepostal" name="codepostal">
	            </div>
            </div>
            <br>
            <h5>Informations relatives aux formations</h5>
            <br>
            <div class="row g-3">
                <div class="col">
                <label for="module" class="form-label">Module</label>
                <input class="form-control" list="modulelistOptions" id="module" name="module">
                	<datalist id="modulelistOptions">
                		<c:forEach items="${modules}" var="moduleCourant">
                		<option> ${moduleCourant.nomModule}</option>
                		</c:forEach>
                	</datalist>
                </div>
                <div class="col">
                <label for="agence" class="form-label">Agence</label>
                    <input class="form-control" list="agencelistOptions" id="agence" name="agence">
                	<datalist id="agencelistOptions">
                		<c:forEach items="${agences}" var="agenceCourant">
                		<option value="${agenceCourant.zoneGeo}"> ${agenceCourant.zoneGeo}</option>
                		</c:forEach>
                	</datalist>
                </div>
            </div>
            <br>
            <div class="row g-3">
            	<div class="col-md-2">
            		<label for="tarif" class="form-label">Tarif</label>
            		<input type="number" id="tarif" name="tarif" class="form-control" value="${param.tarif}">
            	</div>
            </div>


            <br>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <button type="submit" class="btn btn-danger">Validé</button>
            </div>
        </form>

    </div>


</section>

</body>
</html>