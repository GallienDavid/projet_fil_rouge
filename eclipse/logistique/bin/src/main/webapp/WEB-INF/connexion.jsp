<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<title>Connexion</title>

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value="/CSS/sidebarForAll.css"/>">
    <link rel="stylesheet" href="<c:url value="/CSS/connexion.css"/>">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">
    <br>
    <div class="text" id="head"><h1>Connexion</h1></div>
    <br>
    <div id="connexion">

        <br>
        <br>
        <br>
        <form method="POST" action="connexion">
            <label class="form-label">Nom d'Utilisateur</label>
            <br>
            <input type="text" class="form-control" id="username" name="username">
            <br>
            <label class="form-label">Mot de passe</label>
            <br>
            <input type="password" class="form-control" id="passwrd" name="passwrd">
            <br>
            <br>
            <div class="valider">
                <button type="submit" class="btn btn-danger" >Connexion</button>

            </div>
        </form>
        ${message }

    </div>


</section>
</body>
</html>