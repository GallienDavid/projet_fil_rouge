<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/sidebarForAll.css">
    <link rel="stylesheet" href="CSS/interfaceFormation.css">
    <link rel="stylesheet" href="CSS/formation.css">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<%@ include file="sidebar.jspf" %>

<section class="home-section">
<div id="ajoutFormateur">
    <div class="text"> <h1 id="NewFormation">Nouvelle formation</h1></div>
    <br>
    <br>
    <form method="post" action="ajoutFormation">
        <h5>Information formation</h5>
        <br>
        <div class="row g-3">
            <div class="col">
                <input type="text" class="form-control" placeholder="Nom de la formation" id="nom" name="nomFormation">
            </div>
        </div>
        <br>
        <br>
        <div class="row g-3">
            <div class="col">
                <label for="responsableFormation" class="form-label">Responsable</label>
                    <input class="form-control" list="responsablelistOptions" id="responsable" name="responsable">
                	<datalist id="responsablelistOptions">
                		<c:forEach items="${responsableFormations}" var="responsableCourant">
                		<option> ${responsableCourant.nom} ${responsableCourant.prenom }</option>
                		</c:forEach>
                	</datalist>
            </div>
        </div>
        <br>
        <br>
        <div class="row g-3">
            <div class="col-auto">
                <label for="module" class="form-label">Module</label>
                <input class="form-control" list="modulelistOptions" id="module" name="module">
                	<datalist id="modulelistOptions">
                		<c:forEach items="${modules}" var="moduleCourant">
                		<option> ${moduleCourant.nomModule}</option>
                		</c:forEach>
                	</datalist>
            </div>
        </div>
        <br>
          <div id="cont">
            <datalist id="datalistOptions">
                		<c:forEach items="${modules}" var="moduleCourant">
                		<option> ${moduleCourant.nomModule}</option>
                		</c:forEach>
            </datalist>
            <div class="row g-3" >
                <div class="col-auto">
                    <h2>Module</h2>
                </div>
                <div class="col-auto" id="module">
                    <br>
                    <a type="button" class="btn btn-warning" onclick="addRow()" id="addRow">
                        <i class='bx bx-folder-plus'></i>
                        <span class="links_name">Module</span>
                    </a>
                </div>
            </div>
            <br>
        
        </div>
        <br>

        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <button type="submit" class="btn btn-danger">Validé</button>
        </div>
    </form>
</div>

</section>

</body>
<script>
    //<label htmlFor="inputState" className="form-label">Module</label>
    //<input className="form-control" list="datalistOptions" id="exampleDataList" placeholder="Type to search...">
	var compteur = 1 ;
    function addRow() {
    	//var testCrea = document.getElementById('module'+compteur) == undefined ;
    	/*while(testCrea == false){
    		compteur++
    		testCrea = document.getElementById('module'+compteur)
    		consol.log(compteur)
    	}*/
    	var name = "module"+compteur ;

        var rowg = document.createElement('div')
        rowg.setAttribute('class','row') ;
        var colauto = document.createElement('div');

        var br = document.createElement('br');
        br.setAttribute('id','br');
        colauto.setAttribute('class','col') ;

        var input = document.createElement('input')
        input.setAttribute('class','form-control')
        input.setAttribute('list','datalistOptions')
        input.setAttribute('placeHolder','Type to search')
        
		input.setAttribute('name',name)
        var div = document.getElementById('cont');

        div.appendChild(br);
        div.appendChild(rowg)
        rowg.appendChild(colauto);

        colauto.appendChild(input) ;
        //ajout de la durée


        var colauto5 = document.createElement('div');
        colauto5.setAttribute('class','col') ;
        var input5 = document.createElement('a')
        input5.setAttribute('onclick','remove(this)');
        var input6 = document.createElement('i');
        input6.setAttribute('class','bx bx-x bx-border-circle');
        //input4.setAttribute('class','form-control')
        //input4.setAttribute('type','number')
        //input4.setAttribute('placeHolder','Formateur')
        rowg.appendChild(colauto5);
        colauto5.appendChild(input5) ;
        input5.appendChild(input6);
        compteur++



    }

    function remove(el) {
        var element = el;
        var par = element.parentNode;
        var par2 = par.parentNode;
        var element = document.getElementById("br");
        element.parentNode.removeChild(element);
        par2.remove();
        //par.remove();
        //element.remove();
    }

</script>
</html>