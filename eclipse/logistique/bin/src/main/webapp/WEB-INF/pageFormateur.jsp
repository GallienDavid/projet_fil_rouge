<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<c:url value="/image/formateur.ico"/>" type="image/x-icon">
    <title>Formateur</title>
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="<c:url value="/CSS/sidebarForAll.css"/>">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="<c:url value="/CSS/interfaceFormateur.css"/>" rel="stylesheet"/>">

</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">

    <br>
    <div id="connexion">
        <div class="text">Formateur</div>
        <br>
        <br>
        <br>
        <form method="get" action="utilisateur.java">
            <div class="row g-3">
                <div class="col-md-2">
                    <label class="form-label" >Nom</label>
                    <input class="form-control" type="text" placeholder="Search...">
                </div>
                <div class="col-md-2">
                    <label class="form-label" >Zone géographique</label>
                    <select class="form-select" aria-label="Disabled select example">
                        <option selected>None</option>
                        <option value="1">Ile de France</option>
                        <option value="2">Grand Ouest</option>
                        <option value="3">Sud Ouest</option>
                        <option value="4">Nord Est</option>
                        <option value="5">Nord</option>
                        <option value="6">Sud Est</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="inputState" class="form-label">Module</label>

                    <input class="form-control" list="datalistOptions" id="exampleDataList" placeholder="Type to search...">
                    <datalist id="datalistOptions">
                        <option> JAVA</option>
                        <option> Python</option>
                        <option> HTML</option>
                        <option> CSS</option>
                    </datalist>
                </div>
                <div class="col-md-2">
                    <label class="form-label" >Note</label>
                    <select class="form-select" >
                        <option> None</option>
                        <option> Croissant</option>
                        <option> Décroissant</option>
                    </select>

                </div>
                <div class="col-md-2">
                    <div id="nouveau-formateur">
                        <a type="button" class="btn btn-warning" href="${pageContext.request.contextPath}/nouveauFormateur">
                            <i class='bx bxs-contact' ></i>
                            <span>Nouveau formateur</span>
                        </a>
                    </div>
                </div>

            </div>
        </form>
        <br>
        <table class="table">
        <thead>
        <tr>
            <th scope="col">#id</th>
            <th scope="col">Nom</th>
            <th scope="col">Prenom</th>
            <th scope="col">Zone geographique</th>
            <th scope="col">Note</th>
            <th scope="col">Module</th>
        </tr>
        </thead>
            <c:forEach items="${formateurs}" var="formateursCourant">
            <tr>
                <td><form action="${pageContext.request.contextPath}/viewFormateur" method="post"><input type="submit" value= "${formateursCourant.id}" name="formateur" style="border:0px #000 solid;background-color:#fff;text-decoration : underline"/></form></td>
                <td>${formateursCourant.nom }</td>
                <td>${formateursCourant.prenom }</td>
                <td>${formateursCourant.zoneGeo }</td>
                <td>${formateursCourant.evaluation }<c:if test="${formateursCourant.evaluation < 6 && formateursCourant.evaluation != 0}"> <i class='bx bx-error-alt'></i></c:if> </td>
                <td>${formateursCourant.module.nomModule }</td>
            </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>