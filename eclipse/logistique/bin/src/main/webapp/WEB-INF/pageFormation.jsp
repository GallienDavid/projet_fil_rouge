<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!-- Created by CodingLab |www.youtube.com/CodingLabYT-->
<html lang="fr" dir="ltr">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<c:url value="/image/session.ico"/>" type="image/x-icon">
    <title> Formation</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value="/CSS/sidebarForAll.css"/>">
    <link rel="stylesheet" href="<c:url value="/CSS/interfaceFormation.css"/>">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">

    <br>
    <div id="connexion">
        <div class="text">Formation</div>
        <br>
        <br>
        <br>
        <form method="get" action="utilisateur.java">
            <div class="row g-3">
                <div class="col-md-2">
                    <label class="form-label" >Nom</label>
                    <input class="form-control" type="text" placeholder="Search...">
                </div>
                <div class="col-md-2">
                    <label class="form-label" >Agence</label>
                    <select class="form-select" aria-label="Disabled select example">
                        <option selected>None</option>
                        <option value="1">Ile de France</option>
                        <option value="2">Grand Ouest</option>
                        <option value="3">Sud Ouest</option>
                        <option value="4">Nord Est</option>
                        <option value="5">Nord</option>
                        <option value="6">Sud Est</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="inputState" class="form-label">Module</label>

                    <input class="form-control" list="datalistOptions" id="exampleDataList" placeholder="Type to search...">
                    <datalist id="datalistOptions">
                        <option> JAVA</option>
                        <option> Python</option>
                        <option> HTML</option>
                        <option> CSS</option>
                    </datalist>
                </div>
                <div class="col-md-2">
                    <label class="form-label" >Type</label>
                    <select class="form-select" >
                        <option> None</option>
                        <option> Intra</option>
                        <option> Inter</option>
                    </select>

                </div>
                <div class="col-md-2">
                    <div id="nouveau-formateur">
                        <a type="button" class="btn btn-warning" href="${pageContext.request.contextPath}/ajoutFormation">
                            <i class='bx bxs-book-content' ></i>
                            <span>Nouvelle formation</span>
                        </a>
                    </div>
                </div>

            </div>
        </form>
        <br>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#id</th>
                <th scope="col">Nom de la formation</th>
                <th scope="col">Responsable</th>
                <th scope="col">Nombre de module</th>
                <th scope="col">Durée /j</th>
                <th scope="col">Prix /€</th>
                <th scope="col">Nombre de place</th>
            </tr>
            </thead>
            <c:forEach items="${formations}" var="formationsCourant">
            <tr>
                <td><form action="${pageContext.request.contextPath}/viewFormation" method="post"><input type="submit" value= "${formationsCourant.id }" name="idformation" style="border:0px #000 solid;background-color:#fff;text-decoration : underline"/></form></td>
                <td>${formationsCourant.nom }</td>
                <td>${formationsCourant.responsable.nom }  ${formationsCourant.responsable.prenom }</td>
                <td>${formationsCourant.modules.size() }</td>
                <td>${formationsCourant.duree() } </td>
                <td>${formationsCourant.prix() }</td>
                <td>15</td>
            </tr>
            </c:forEach>
        </table>

    </div>
    
    
</section>

</body>
</html>