<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
     <link rel="shortcut icon" href="<c:url value="/image/session.ico"/>" type="image/x-icon">
    <title>Module</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value="/CSS/sidebarForAll.css"/>">
    <link rel="stylesheet" href="<c:url value="/CSS/interfaceFormation.css"/>">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">

    <br>
    <div id="connexion">
        <div class="text">Module</div>
        <br>
        <br>
        <br>
        <form method="post" action="utilisateur.java">
            <div class="row g-3">
                <div class="col-md-2">
                    <label class="form-label" >Nom</label>
                    <input class="form-control" type="text" placeholder="Search...">
                </div>
                <div class="col-md-2">
                    <label class="form-label" >Thème</label>
                    <select class="form-select" aria-label="Disabled select example">
                        <option selected>None</option>
                        <option value="1">Developpement</option>
                        <option value="2">Reseaux</option>
                        <option value="3">Administration système</option>
                        <option value="4">Cybersécurité</option>
                        <option value="5">Big-Data</option>

                    </select>
                </div>
                <div class="col-md-2">
                    <label class="form-label" >Domaine</label>
                    <select class="form-select" >
                        <option> None</option>
                        <option> BDD</option>
                        <option> Programmation</option>
                    </select>

                </div>

                <div class="col-md-2">
                    <div id="nouveau-formateur">
                        <a type="button" class="btn btn-warning" href="${pageContext.request.contextPath}/nouveauModule">
                            <i class='bx bxs-book-content' ></i>
                            <span>Nouveau module</span>
                        </a>
                    </div>
                </div>

            </div>
        </form>
        <br>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#id</th>
                <th scope="col">Theme</th>
                <th scope="col">Sous Theme</th>
                <th scope="col">Nom Module</th>
            </tr>
            </thead>
            <c:forEach items="${modules}" var="moduleCourant">
            <tr>
            	<td><a type="button" href="">#${moduleCourant.ID_MODULE }</a></td>
               	<td>Developpement</td>
                <td>Langage</td>
                <td>${moduleCourant.nomModule}</td> 

            </tr>
            </c:forEach>
        </table>

    </div>
</section>

</body>
</html>
</body>
</html>