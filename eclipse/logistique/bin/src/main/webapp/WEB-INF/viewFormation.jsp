<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<link rel="shortcut icon" href="image/formateur.ico" type="image/x-icon">
   
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="CSS/sidebarForAll.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="CSS/interfaceFormateur.css" rel="stylesheet">
    <link rel="stylesheet" href="CSS/idformateur.css">
    
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">

    <br>
    <div id="ajoutFormateur">
        <div class="text"> <h1 id="NewFormateur">${formation.nom}</h1></div>
        <br>
        <br>
        <form method="post" action="">
            <h5>Information sur la formation</h5>
            <br>
            <div class="row g-3">
                <div class="col">
                	<h5>Prix :</h5>
                    <p class="cadre">${formation.prix()} €</p>
                </div>
                <div class="col">
               	 	<h5>Durée :</h5>
                    <p class="cadre">${formation.duree()} Jours </p>
                </div>

            </div>
            <br>
    </div>
    <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#id</th>
            <th scope="col">Nom</th>
            <th scope="col">Durée</th>
            <th scope="col">Prix</th>
        </tr>
        </thead>
         <c:forEach items="${formation.modules}" var="module">
        <tr>
            <td><a>${module.ID_MODULE }</a></td>
            <td>${module.nomModule }</td>
            <td>${module.nombreJour } Jours</td>
            <td>${module.prix } € </td>

        </tr>
        </c:forEach>
    </table>
    </form>
    <table class="table">
            <thead>
            <tr>
                <th scope="col">#id</th>
                <th scope="col">Nom</th>
                <th scope="col">date de début</th>
                <th scope="col">date de fin</th>
                <th scope="col">Agence</th>
                <th scope="col">Responsable</th>
                <th scope="col">type</th>
                <th scope="col">salles</th>
                <th scope="col">Nombre d'inscrits</th>
                <th scope="col">Validé</th>
                <th scope="col">En cours</th>

            </tr>
            </thead>
            <c:forEach items="${plani}" var="planificationsCourant">
            <tr>
                <td>${planificationsCourant.id }</td>
                <td>${planificationsCourant.formation.nom}</td>
                <td>${planificationsCourant.dateDebut}</td>
                <td>${planificationsCourant.dateFin()}</td>
                <td>${planificationsCourant.salle.agence.zoneGeo}</td>
                <td>${planificationsCourant.formation.responsable.nom}</td>
                <td> <c:if test="${planificationsCourant.cible == 0}">
                Intra
                </c:if>
                <c:if test="${planificationsCourant.cible == 1}">
                Inter
                </c:if></td>
                <td>${planificationsCourant.salle.nom}</td>
                <td>0</td>
                <td>0</td>
                <td>${planificationsCourant.enCours() }
                </td>

            </tr>
            </c:forEach>
        </table>

</section>

</body>
</html>