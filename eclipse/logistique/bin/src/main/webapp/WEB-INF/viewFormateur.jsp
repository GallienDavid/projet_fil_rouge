<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<link rel="shortcut icon" href="image/formateur.ico" type="image/x-icon">
   
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="CSS/sidebarForAll.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="CSS/interfaceFormateur.css" rel="stylesheet">
    <link rel="stylesheet" href="CSS/idformateur.css">
    
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section">

    <br>
    <div id="ajoutFormateur">
        <div class="text"> <h1 id="NewFormateur">${formateur.nom} ${formateur.prenom}</h1></div>
        <br>
        <br>
            <h5>Information personnelle</h5>
            <br>
            <div class="row g-3">
                <div class="col">
                    <p class="cadre">${formateur.nom}</p>
                </div>
                <div class="col">
                    <p class="cadre">${formateur.prenom}</p>
                </div>

            </div>
            <br>

            <div class="col-md-6">
                <label for="inputEmail4" class="form-label">Email</label>
                <p class="cadre">${formateur.email}</p>
            </div>
            <div class="col-md-6">
                <label for="inputtel4" class="form-label">Telephone</label>
                <p class="cadre">${formateur.telephone}</p>
            </div>
            <div class="col-12">
                <label for="inputAddress" class="form-label">Address</label>
                <p class="cadre">${formateur.adresse}</p>
            </div>
            <br>
            <h5>Information relative aux formations</h5>
            <br>
            <div class="row g-3">
                <div class="col">
                    <label for="inputState" class="form-label">Module</label>
                    <p class="cadre">${formateur.module.nomModule}</p>
                </div>
                <div class="col">
                    <label for="inputState" class="form-label">Zone géographique</label>
                    <p class="cadre">${formateur.zoneGeo}</p>
                </div>
            </div>
            <br>
            <div class="col">
                <label for="inputState" class="form-label">Note moyenne</label>
                <p class="cadre">${formateur.evaluation}</p>
            </div>


            <br>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <form method="post" action="${pageContext.request.contextPath}/deleteFormateur"><button type="submit" class="btn btn-danger" value="${formateur.id }" name="id">Supprimer</button></form>
            </div>


    </div>
    <br>
    <table class="table">
            <thead>
            <tr>
                <th scope="col">#id</th>
                <th scope="col">Nom</th>
                <th scope="col">date de début</th>
                <th scope="col">date de fin</th>
                <th scope="col">Agence</th>
                <th scope="col">Responsable</th>
                <th scope="col">type</th>
                <th scope="col">salles</th>
                <th scope="col">Nombre d'inscrits</th>
                <th scope="col">Validé</th>
                <th scope="col">En cours</th>

            </tr>
            </thead>
            <c:forEach items="${planifications}" var="planificationsCourant">
            <tr>
                <td>${planificationsCourant.id }</td>
                <td>${planificationsCourant.formation.nom}</td>
                <td>${planificationsCourant.dateDebut}</td>
                <td>${planificationsCourant.dateFin() }</td>
                <td>${planificationsCourant.salle.agence.zoneGeo}</td>
                <td>${planificationsCourant.formation.responsable.nom}</td>
                <td> <c:if test="${planificationsCourant.cible == 0}">
                Intra
                </c:if>
                <c:if test="${planificationsCourant.cible == 1}">
                Inter
                </c:if></td>
                <td>${planificationsCourant.salle.nom}</td>
                <td>0</td>
                <td>0</td>
                <td>${planificationsCourant.enCours() }
                </td>

            </tr>
            </c:forEach>
        </table>

</section>

</body>
</html>