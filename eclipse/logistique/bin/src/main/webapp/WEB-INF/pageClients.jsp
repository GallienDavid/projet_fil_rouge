<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <link rel="shortcut icon" href="<c:url value="/image/formateur.ico"/>" type="image/x-icon">
    <title>Clients</title>
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="<c:url value="/CSS/sidebarForAll.css"/>">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="<c:url value="/CSS/interfaceFormateur.css"/>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<c:url value="/CSS/clients.css"/>">
</head>
<body>
<%@ include file="sidebar.jspf" %>
<section class="home-section" >
<br>
<ul class="nav nav-pills mb-3 nav-justified" id="ex1" role="tablist">
		<li class="nav-item" role="presentation">
    <a
      class="nav-link active"
      id="ex1-tab-1"
      data-mdb-toggle="pill"
      href="#ex1-pills-1"
      role="tab"
      aria-controls="ex1-pills-1"
      aria-selected="true"
      >Client inter</a
    >
  </li>
	  <li class="nav-item" role="presentation">
	    <a
	      class="nav-link"
	      id="ex1-tab-2"
	      data-mdb-toggle="pill"
	      href="#ex1-pills-2"
	      role="tab"
	      aria-controls="ex1-pills-2"
	      aria-selected="false"
	      >Client Intra</a
	    >
	  </li>
</ul>
<!-- Pills navs -->

<!-- Pills content -->
<div class="tab-content" id="ex1-content">
  <div
    class="tab-pane fade show active "
    id="ex1-pills-1"
    role="tabpanel"
    aria-labelledby="ex1-tab-1"
  >
	  <table class="table">
	            <thead>
	            <tr>
	                <th scope="col">#id</th>
	                <th scope="col">Nom</th>
	                <th scope="col">Prenom</th>
	                <th scope="col">Mail</th>
	                <th scope="col">Telephone</th>
	            </tr>
	            </thead>
	            <c:forEach items="${clientsInter}" var="clientsInterCourant">
	            	<tr>
		                <td>${clientsInterCourant.id_Client }</td>
		                <td>${clientsInterCourant.nom }</td>
		                <td>${clientsInterCourant.prenom }</td>
		                <td>${clientsInterCourant.mail }</td>
		                <td>${clientsInterCourant.telephone } </td>
	            	</tr>
            	</c:forEach>
	  </table>
  </div>
  <div class="tab-pane fade" id="ex1-pills-2" role="tabpanel" aria-labelledby="ex1-tab-2">
     <table class="table">
	            <thead>
	            <tr>
	                <th scope="col">#id</th>
	                <th scope="col">Société</th>
	                <th scope="col">Contact</th>
	                <th scope="col">Mail</th>
	                <th scope="col">Telephone</th>
	            </tr>
	            </thead>
	            <c:forEach items="${clientsIntra}" var="clientsIntraCourant">
	            	<tr>
		                <td>${clientsIntraCourant.id_Societe }</td>
		                <td>${clientsIntraCourant.nom_Societe }</td>
		                <td>${clientsIntraCourant.nom_Contact } ${clientsIntraCourant.prenom_Contact }</td>
		                <td>${clientsIntraCourant.mail }</td>
		                <td>${clientsIntraCourant.telephone } </td>
	            	</tr>
            	</c:forEach>
	  </table>
  </div>
</div>
<!-- Pills content -->

</section>
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.10.2/mdb.min.js"
></script>
</body>
</html>