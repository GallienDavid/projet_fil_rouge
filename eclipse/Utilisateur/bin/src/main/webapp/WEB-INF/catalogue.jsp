<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catalogue</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/body.css">
    <link rel="stylesheet" href="CSS/style.css">
</head>
<body>
<%@ include file="newHeader.jspf" %>
<div class="corps">

<p>Veuillez entrer le nom de votre formation</p>
<form action="catalogue" method="post">
<div class="row">
	<div class="col-md-2">
			
                    <label class="form-label" >Formation</label>
                    <select class="form-select" aria-label="Disabled select example" name="formationId">
                        <option selected disabled >Formation</option>
                        <c:forEach items="${formations}" var="formation"> <option  value="${formation.id}"> ${formation.nom}</option></c:forEach>
                    </select>
                    <br>
                    <input type="submit" class="btn btn-danger" value = "Filtrer">
             </div>	       
                
 </div>	
</form>
<br>


	<div class="row">
		 
			<c:forEach items="${formations }" var="formationCourant">
				<div class="col-sm-6">
					<div class="card">
					  <div class="card-header">
					    <h5>${formationCourant.nom }</h5>
					  </div>
					  <div class="card-body">
					    <h5 class="card-title"> Responsable : ${formationCourant.responsable.nom} ${formationCourant.responsable.prenom}</h5>
					    <p> Prix : ${formationCourant.prix()} €</p>
					     <p> Durée : ${formationCourant.duree()} Jours</p>
					     <p>Module : </p>
						    <c:forEach items="${formationCourant.modules}" var="module">
								<span>${module.nomModule} | </span>
							</c:forEach>
					    <br>
					    <br>
					    <form action="detailsformation" method="post"><button type="submit" value="${formationCourant.id}" name="idformation" class="btn btn-primary">Plus d'informations</button></input></form>
					  </div>
					</div>
					<br>
				</div>
			</c:forEach>
		
	</div>






<!-- 

<c:forEach items="${ planifFormation}" var="planifFormation">
${ planifFormation.formation.nom}
	<c:forEach items="${ planifFormation.formation.modules}" var="module">
		${module.nomModule} 
	</c:forEach>
${ planifFormation.dateDebut}
${ planifFormation.salle.agence.zoneGeo}
</c:forEach>

 
                <div class="col-md-2">
                    <label class="form-label" >Agence</label>
                    <select class="form-select" aria-label="Disabled select example" name="agenceId">
                        <option selected disabled>Zone géographique</option>
                       <c:forEach items="${agences}" var="agence"> <option  value="${agence.id }"> ${agence.zoneGeo}</option></c:forEach>
                    </select>
                </div>
 <table class="table">
            <thead>
            <tr>
                <th scope="col">Nom de la formation</th>
                <th scope="col">Module</th>
                <th scope="col">Date de début</th>
            </tr>
            </thead>
<c:forEach items="${planifications }" var="planification"> 
<tr>
<td>${planification.formation.nom}</td>
<td>${planification.formation.modules}</td>
<td>${planification.dateDebut} </td>
</tr>
</c:forEach>
</table>

${formation}

<table border = 1 class="table">
	<th scope="col">Nom de la formation</th>
    <th scope="col">Module</th>
    <th scope="col">Date de début</th></p>
		</tr>
            </thead>
            <c:forEach items="${formation}" var="formation"> 
<tr>
<td>${formation}</td>

</tr>
<p>${formation}</p>
</c:forEach>
<!--   <p>${planifications}</p> 
<c:forEach items="${planifications }" var="planification"> 
${planification.id}
${planification.formation.nom}</br>
${planification.formation.modules} </br>
${planification.dateDebut} </br>
<form action="inscriptionFormation" method="post"><button type="submit" value="${planification.id}" name="idPlanification" class="btn btn-primary">S'inscrire</button></input></form>

</c:forEach>
</table> 

${planifFormation[0].salle.nom}
${planifFormation}
<!--  <div class="col-md-2">
                  <label class="form-label" >Module</label>
                  <select class="form-select" aria-label="Disabled select example" name="moduleId">
                     <option selected>None</option>
                     <c:forEach items="${modules}" var="module"> <option  value="${module.ID_MODULE }"> ${module.nomModule}</option></c:forEach>
                    </select>
                </div> -->


-->




</body> 
</html>