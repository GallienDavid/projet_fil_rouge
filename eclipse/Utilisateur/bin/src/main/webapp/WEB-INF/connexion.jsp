<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Connexion</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/body.css">
    <link rel="stylesheet" href="CSS/style.css">
</head>
<body>
<%@ include file="newHeader.jspf" %>
<div class="corps">
	<h1> Connexion</h1><br>
	<h5>Particulier</h5>

	<form action="connexion" method="post">
		<label for="mail" class="form-label">Email :</label>
		<input type="mail" id="mail" name="mail" value="${param.mail}"  class="form-control"><br/>
		<label for="passwrd" class="form-label">Mot de passe :</label>
		<input type="password" id="passwrd" name="passwrd" class="form-control"><br/>
		<input type="submit" value="se connecter" class="btn btn-success"  />
	</form><br>

</div>



</body>
</html>