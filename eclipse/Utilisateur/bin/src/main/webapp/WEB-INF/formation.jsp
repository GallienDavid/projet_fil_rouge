<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/body.css">
    <link rel="stylesheet" href="CSS/style.css">
</head>
<body>
<%@ include file="newHeader.jspf" %>
<div class="corps">
	<div class="card">
		<h1 class="card-header">${formation.nom }</h1>
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Prix : ${formation.prix() } €<br>
			Durée : ${formation.duree() } jours</li>
		    <li class="list-group-item">
		    	<h5>Responsable de la formation : </h5>
			    ${formation.responsable.nom }
				${formation.responsable.prenom }<br>
				
			    Télephone : ${formation.responsable.tel }<br>
				Mail : ${formation.responsable.mail }
		    </li>
		    <li class="list-group-item">
		   		<h5>Module :</h5>
		    	<c:forEach items="${formation.modules}" var="module">
					${module.nomModule}
					Durée : ${module.nombreJour} jours <br>
				</c:forEach>
		    </li>   
		 </ul>
	</div>
	<br>
	<div class="card">
		<h1 class="card-header">Prochaines sessions</h1>
		    	<table class="table">
		            <thead>
		            <tr>
		                <th scope="col">Agence</th>
		                <th scope="col">Date début </th>
		                <th scope="col">date de fin</th>
		                <th scope="col">Nombre de place restante</th>
		                <th scope="col">S'incrire</th>
		            </tr>
            		</thead>
             		<c:forEach items="${formation.plani}" var="planificationsCourant">
             			<c:if test="${planificationsCourant.nbInscrit < 15}">
				            <tr>
				                <td>${planificationsCourant.salle.agence.zoneGeo}</td>
				                <td>${planificationsCourant.dateDebut}</td>
				                <td>${planificationsCourant.dateFin() }</td>
				                <td>${15 - planificationsCourant.nbInscrit}</td>
				                <td>
				                	<form action="inscriptionFormation" method="post"><button type="submit" value="${planification.id}" name="idPlanification" class="btn btn-primary">S'inscrire</button></input></form>
				                </td>
				            </tr>
			             </c:if>
		            </c:forEach>
            		
           		</table>
	</div>
</div>
</body>
</html>