<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inscription</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Boxicons CDN Link -->
<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
<link rel="stylesheet" href="CSS/body.css">
<link rel="stylesheet" href="CSS/style.css">
</head>
<body>
<%@ include file="newHeader.jspf" %>
<div class="corps">
	<h1 class="text" >Inscription </h1><br>
	
	<form action="inscription" method="post">
	
	<label for="nom_Societe" class="form-label">Nom de votre société :</label>
	<input type="text" id="nom_Societe" name="nom_Societe" value="${param.nom_Societe}" class="form-control">${erreurs.get('nom_Societe')}<br/>
	
	<label for="nom" class="form-label">Nom :</label>
	<input type="text" id="nom" name="nom" value="${param.nom}" class="form-control">${erreurs.get('nom')}<br/>
	
	<label for="prenom" class="form-label">Prenom :</label>
	<input type="text" id="prenom" name="prenom" value="${param.nom}" class="form-control">${erreurs.get('prenom')}<br/>
	
	<label for="mail" class="form-label">Email :</label>
	<input type="email" id="mail" name="mail" value="${empty erreurs.get('mail') ? param.mail : ''}" class="form-control">${erreurs.get('mail')}<br/>
	
	<label for="telephone" class="form-label">Telephone:</label>
	<input type="text" id="telephone" name="telephone" value="${param.telephone}" class="form-control">${erreurs.get('telephone')}<br/>
	
	<label for="passwrd" class="form-label">Mot de passe :</label>
	<input type="password" id="passwrd" name="passwrd" class="form-control">${erreurs.get('passwrd')}<br/>
	
	<label for="confirmPasswrd" class="form-label">Confirmer le mot de passe :</label>
	<input type="password" id="confirmPasswrd" name="confirmPasswrd" class="form-control">${erreurs.get('confirmPasswrd')}<br/>
	
	<input type="submit" class="btn btn-success" value="s'inscrire" />
</form>
	


</div>

</body>
</html>