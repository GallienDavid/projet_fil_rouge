<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>EspaceClient</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/body.css">
</head>
<body>
<%@ include file="newHeader.jspf" %>
<div class="corps">
	<h1 class="text" >Espace client </h1><br>
	

		<div class="col-sm-6">
			<div class="card text-center">
				<div class="card-body">
					<a href="inscription" class="btn btn-info"> Inscription</a>
					<a  href="connexion" class="btn btn-info"> Connexion</a>
				</div>
			</div>
		</div>
</div>
</body>
</html>
</body>
</html>