package fr.filrouge.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


import java.io.IOException;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.filrouge.Manager.AgenceManager;
import fr.filrouge.Manager.FormationManager;
import fr.filrouge.Manager.ModuleManager;
import fr.filrouge.Manager.PlanificationFormationManager;
import fr.filrouge.objets.Agence;
import fr.filrouge.objets.Formation;
import fr.filrouge.objets.Module;
import fr.filrouge.objets.PlanificationFormation;


@WebServlet("/catalogue")
public class Catalogue extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		FormationManager formationManager = new FormationManager();
		List<Formation> formations = formationManager.findAll();
		request.setAttribute("formations", formations);
		
		List<Formation> formationsList = formationManager.findAll();
		request.setAttribute("formationsList", formationsList);
		
		ModuleManager moduleManager = new ModuleManager();
		List<Module> modules = moduleManager.findAll();
		request.setAttribute("modules", modules);
		
		AgenceManager agenceManager = new AgenceManager();
		List<Agence> agence = agenceManager.findAll();
		request.setAttribute("agences", agence);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/catalogue.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager() ;
		
		//List<PlanificationFormation> plani = planificationFormationManager.findAll();
		//System.out.println(plani.get(0).getSalle().getAgence());
		//request.setAttribute("planifications", plani);
		 
		FormationManager formationManager = new FormationManager();
		List<Formation> formations = formationManager.findAll();
		List<Formation> formationsTemp = new ArrayList<>();
		request.setAttribute("formations", formations);
		
		ModuleManager moduleManager = new ModuleManager();
		List<Module> modules = moduleManager.findAll();
		request.setAttribute("modules", modules);
		
		AgenceManager agenceManager = new AgenceManager();
		List<Agence> agence = agenceManager.findAll();
		request.setAttribute("agences", agence);
		
		//PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager();
		
		//int ID_MODULE = Integer.valueOf(request.getParameter("moduleId"));
		if (request.getParameter("formationNom") != null) {
			String formationNom = request.getParameter("formationNom");
			for (Formation formation : formations) {
				
				if(formation.getNom().equals(formationNom)) {
					formationsTemp.add(formation);
					System.out.println(formationsTemp);
				} else if (request.getParameter("moduleNom") != null) {
					for (Module module : formation.getModules()) {
						if (module.getNomModule().equals(request.getParameter("moduleNom"))) {
							formationsTemp.add(formation);
						}
						
					}
				}
				
			}
			
		}
		
		if (request.getParameter("moduleNom") != null) {
			for (Formation formation : formations) {
				if(formationsTemp.contains(formation)) {
	
				} else {
					for (Module module : formation.getModules()) {
						if (module.getNomModule().equals(request.getParameter("moduleNom"))) {
							formationsTemp.add(formation);
						}
						
					}
				}
				
			}
			
		}
		
		if (request.getParameter("moduleNom") == null && request.getParameter("formationNom") == null) {
			formationsTemp = formationManager.findAll();
		}
		
		request.setAttribute("formationsList", formationsTemp);
		
		//int agenceId = Integer.valueOf(request.getParameter("agenceId"));

		//List<PlanificationFormation> planificationFormations = planificationFormationManager.findAll();
		//request.setAttribute("planificationFormations", planificationFormations);
		//System.out.println(planificationFormations);
		
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/catalogue.jsp").forward(request, response);
		
		
	}

}


