package fr.filrouge.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.filrouge.Manager.FormationManager;
import fr.filrouge.Manager.ThemeManager;
import fr.filrouge.objets.Formation;
import fr.filrouge.objets.Theme;

@WebServlet("/allformations")
public class allFormation extends HttpServlet {
	
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ThemeManager themeManager = new ThemeManager();
		
		List<Theme> themes = new ArrayList();
		themes = themeManager.findAll();
		System.out.println(themes);
		request.setAttribute("themes", themes);
		FormationManager formationManager = new FormationManager();
		
		List<Formation> formations = formationManager.findAll();
		request.setAttribute("formations", formations);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/allFormation.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
