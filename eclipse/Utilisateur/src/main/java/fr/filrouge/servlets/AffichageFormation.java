package fr.filrouge.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.filrouge.Manager.AgenceManager;
import fr.filrouge.Manager.ClientInterManager;
import fr.filrouge.Manager.FormationManager;
import fr.filrouge.Manager.PlanificationFormationManager;
import fr.filrouge.objets.Agence;
import fr.filrouge.objets.ClientInter;
import fr.filrouge.objets.Formation;
import fr.filrouge.objets.PlanificationFormation ;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/detailsformation")
public class AffichageFormation extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idFormation = Integer.valueOf(request.getParameter("idformation"));
		LocalDate dateFinalClient = LocalDate.now();
		if (request.getSession().getAttribute("clientInter") != null) {
			ClientInter clientInter = (ClientInter) request.getSession().getAttribute("clientInter");
			List<PlanificationFormation> formationsClient = new ArrayList<>();
			ClientInterManager clientInterManager = new ClientInterManager();
			formationsClient = clientInterManager.formationsByClient(clientInter);
			System.out.println("Filtre par date");
			System.out.println(formationsClient);
			
			for (PlanificationFormation planificationFormationClient : formationsClient) {
				if (planificationFormationClient.dateFin().isAfter(dateFinalClient)) {
					dateFinalClient = planificationFormationClient.dateFin();
				}
			}
			System.out.println("La date finale du client est:");
			System.out.println(dateFinalClient);
		}
		
		
		FormationManager formationManager = new FormationManager();
		Formation formation = new Formation();
		formation = formationManager.FormationById(idFormation);
		LocalDate date = LocalDate.now() ;
		PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager();
		List<PlanificationFormation> newPlani = planificationFormationManager.findByIdFormation(idFormation);
		List<PlanificationFormation> planifications = new ArrayList<PlanificationFormation>();
		for(PlanificationFormation planificationFormation : newPlani) {
			if(planificationFormation.getDateDebut().isAfter(date) && planificationFormation.getDateDebut().isAfter(dateFinalClient) ) {
				planifications.add(planificationFormation);	
			}
		}
		request.setAttribute("formation", formation);
		request.setAttribute("plani", planifications);
		AgenceManager agenceManager = new AgenceManager();
		List<Agence> agence = agenceManager.findAll();
		request.setAttribute("agences", agence);
		
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/formation.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
