package fr.filrouge.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import fr.filrouge.Manager.ClientInterManager;
import fr.filrouge.Manager.ClientIntraManager;
import fr.filrouge.objets.ClientInter;
import fr.filrouge.objets.ClientIntra;


@WebServlet("/inscription")
public class Inscription extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String nom_Societe = request.getParameter("nom_Societe");
		String mail = request.getParameter("mail");
		String telephone = request.getParameter("telephone");
		String passwrd = request.getParameter("passwrd");
		String confirmPasswrd = request.getParameter("confirmPasswrd");
		
		if (nom_Societe.isBlank()) {
			
		//r�cup�ration des donn�es des clients inter
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		
		ClientInterManager clientInterManager = new ClientInterManager();
		Map<String,String> erreurs = clientInterManager.verif(mail,passwrd, confirmPasswrd);
			
		if(erreurs.isEmpty()) {
				
			ClientInter clientInter = new ClientInter(nom, prenom, mail,telephone, passwrd);
			clientInterManager.enregistre(clientInter);
			System.out.println(clientInter);
			
				request.setAttribute("messageSucces", "Vous �tes bien inscrit");
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
			.forward(request, response);
		} else {
			request.setAttribute("erreurs", erreurs);
			this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp")
			.forward(request, response);
		}
	}
				
			//r�cup�ration des donn�es des clients inter
			else {
				String nom_Contact = request.getParameter("nom");
				String prenom_Contact = request.getParameter("prenom");
				ClientIntraManager clientIntraManager = new ClientIntraManager();
				Map<String,String> erreurs = clientIntraManager.verif(mail, passwrd, confirmPasswrd);
				
					if(erreurs.isEmpty()) {
			
						ClientIntra clientIntra = new ClientIntra(nom_Societe, nom_Contact, prenom_Contact, mail, telephone, passwrd);
						clientIntraManager.enregistrer(clientIntra); 
						System.out.println(clientIntra);
				
						request.setAttribute("messageSucces", "Vous �tes bien inscrit");
						this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
						.forward(request, response);
				
					} else {
						request.setAttribute("erreurs", erreurs);
						this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp")
						.forward(request, response);
			}
		}
	}
}
