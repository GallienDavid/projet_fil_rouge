package fr.filrouge.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import fr.filrouge.Manager.ClientInterManager;
import fr.filrouge.Manager.ClientIntraManager;
import fr.filrouge.objets.ClientInter;
import fr.filrouge.objets.ClientIntra;

@WebServlet("/connexion")

public class Connexion extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String mail = request.getParameter("mail");
		String passwrd = request.getParameter("passwrd");

		ClientInterManager clientInterManager = new ClientInterManager();
		ClientInter clientInter = clientInterManager.findByMailAndPasswrd(mail, passwrd);
		
		ClientIntraManager clientIntraManager = new ClientIntraManager();
		ClientIntra clientIntra = clientIntraManager.findByMailAndPasswrd(mail, passwrd);

		
		if (!(clientIntra == null)) {
		 request.getSession().setAttribute("clientIntra", clientIntra);
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
		 }
		
		 else if  (!(clientInter == null)) {
		 request.getSession().setAttribute("clientInter", clientInter);
			this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
		 } else {
		 request.setAttribute("message", "Mail ou mot de passe invalide");
			this.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
		 }

	}
}

