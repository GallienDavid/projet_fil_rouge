package fr.filrouge.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.filrouge.Manager.ClientInterManager;
import fr.filrouge.objets.ClientInter;
import fr.filrouge.objets.PlanificationFormation;

@WebServlet("/espaceClient")
public class EspaceClient extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getSession().getAttribute("clientInter") != null) {
			ClientInter clientInter = (ClientInter) request.getSession().getAttribute("clientInter");
			List<PlanificationFormation> formationsClient = new ArrayList<>();
			ClientInterManager clientInterManager = new ClientInterManager();
			formationsClient = clientInterManager.formationsByClient(clientInter);
			System.out.println(formationsClient);
			request.setAttribute("formationsClient", formationsClient);
			
		}

		this.getServletContext().getRequestDispatcher("/WEB-INF/espaceClient.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ClientInter clientInter = (ClientInter) request.getSession().getAttribute("clientInter");
		System.out.println(clientInter.getId_Client());
		int idPlanification = Integer.valueOf(request.getParameter("idPlanification"));
		System.out.println(idPlanification);
		ClientInterManager clientInterManager = new ClientInterManager();
		clientInterManager.deleteByID(idPlanification, clientInter.getId_Client());
		
		if (request.getSession().getAttribute("clientInter") != null) {
			
			List<PlanificationFormation> formationsClient = new ArrayList<>();
			formationsClient = clientInterManager.formationsByClient(clientInter);
			System.out.println(formationsClient);
			request.setAttribute("formationsClient", formationsClient);
			
		}

		this.getServletContext().getRequestDispatcher("/WEB-INF/espaceClient.jsp").forward(request, response);
		
		
	}

}
