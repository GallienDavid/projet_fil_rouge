package fr.filrouge.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.filrouge.Manager.ThemeManager;
import fr.filrouge.objets.Theme;

@WebServlet("/header")
public class NewHeader extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ThemeManager themeManager = new ThemeManager();
		
		List<Theme> themes = new ArrayList();
		themes = themeManager.findAll();
		System.out.println(themes);
		request.setAttribute("themes", themes);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/newHeader.jspf").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
