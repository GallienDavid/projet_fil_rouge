package fr.filrouge.servlets;

import java.io.IOException;
import java.util.List;

import fr.filrouge.Manager.AgenceManager;
import fr.filrouge.objets.Agence;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/contact")
public class Contact extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AgenceManager agenceManager = new AgenceManager();
		List<Agence> agences = agenceManager.findAll();
		request.setAttribute("agences", agences);
		this.getServletContext().getRequestDispatcher("/WEB-INF/contact.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
