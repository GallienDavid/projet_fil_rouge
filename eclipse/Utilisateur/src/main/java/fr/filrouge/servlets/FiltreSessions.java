package fr.filrouge.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.filrouge.Manager.AgenceManager;
import fr.filrouge.Manager.FormationManager;
import fr.filrouge.Manager.PlanificationFormationManager;
import fr.filrouge.objets.Agence;
import fr.filrouge.objets.Formation;
import fr.filrouge.objets.PlanificationFormation;

@WebServlet("/filtreSession")
public class FiltreSessions extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/formation.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String zoneGeo = request.getParameter("zoneGeo");
		int idFormation = Integer.valueOf(request.getParameter("idFormation"));
		
		FormationManager formationManager = new FormationManager();
		Formation formation = new Formation();
		formation = formationManager.FormationById(idFormation);
		PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager();
		List<PlanificationFormation> planifications = planificationFormationManager.findByIdFormation(idFormation);
				
		LocalDate date = LocalDate.now() ;
		List<PlanificationFormation> newPlani = new ArrayList() ;
		for(int i = 0 ; i < planifications.size() ; i++) {
			if(request.getParameter("zoneGeo") != null && !request.getParameter("dateDebut").isBlank()) {
				LocalDate dateDebut = LocalDate.parse(request.getParameter("dateDebut"));
				if(planifications.get(i).getDateDebut().isAfter(date) && planifications.get(i).getDateDebut().isAfter(dateDebut) && planifications.get(i).getSalle().getAgence().getZoneGeo().equals(zoneGeo)) {
					newPlani.add(planifications.get(i));	
				}
			} else if( request.getParameter("zoneGeo") != null ) {
				if(planifications.get(i).getDateDebut().isAfter(date) &&  planifications.get(i).getSalle().getAgence().getZoneGeo().equals(zoneGeo)) {
					newPlani.add(planifications.get(i));	
				}
				
			} else if( !request.getParameter("dateDebut").isBlank() ) {
				LocalDate dateDebut = LocalDate.parse(request.getParameter("dateDebut"));
				if(planifications.get(i).getDateDebut().isAfter(date) && planifications.get(i).getDateDebut().isAfter(dateDebut)) {
					newPlani.add(planifications.get(i));	
				}
			} else {
				if(planifications.get(i).getDateDebut().isAfter(date)) {
					newPlani.add(planifications.get(i));	
				}
			}
		}
		request.setAttribute("formation", formation);
		request.setAttribute("plani", newPlani);
		AgenceManager agenceManager = new AgenceManager();
		List<Agence> agence = agenceManager.findAll();
		request.setAttribute("agences", agence);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/formation.jsp").forward(request, response);
	}

}
