package fr.filrouge.servlets;

import java.io.IOException;

import fr.filrouge.Manager.PlanificationFormationManager;
import fr.filrouge.objets.ClientInter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/inscriptionFormation")
public class InscriptionFormation extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int score = 0;
		if (request.getParameter("questionA").equals("c")) {
			score++;
		}
		if (request.getParameter("questionB").equals("a")) {
			score++;
		}
		if (request.getParameter("questionC").equals("a")) {
			score++;
		}
		if (request.getParameter("questionD").equals("a")) {
			score++;
		}
		if (request.getParameter("questionE").equals("a")) {
			score++;
		}
		if (request.getParameter("questionF").equals("b")) {
			score++;
		}
		
		System.out.println(score);
		if (score >= 3) {
			int idPlanification = Integer.valueOf(request.getParameter("idPlanification"));
			// System.out.println(idPlanification);
			ClientInter clientInter = (ClientInter) request.getSession().getAttribute("clientInter");
			// System.out.println(clientInter);

			PlanificationFormationManager planificationFormationManager = new PlanificationFormationManager();
			planificationFormationManager.inscriptionClientInter(idPlanification, clientInter.getId_Client());

			int nombreInscrit;
			nombreInscrit = planificationFormationManager.nombreInscrit(idPlanification);
			if (nombreInscrit >= 3) {
				planificationFormationManager.validerFormation(idPlanification);
				response.sendRedirect("espaceClient");
			}
		} else {
			request.setAttribute("niveauFaible", "Votre niveau est trop faible, veuillez r�essayer plus tard.");
			
			response.sendRedirect("espaceClient");
		}
		

		

	}

}
