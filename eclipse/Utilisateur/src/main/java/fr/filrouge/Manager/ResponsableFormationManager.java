package fr.filrouge.Manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import fr.filrouge.DAO.ResponsableFormationDAO;
import fr.filrouge.objets.ResponsableFormation;



public class ResponsableFormationManager {

	
	public List<ResponsableFormation> findAll() {
		ResponsableFormationDAO responsableFormationDAO = new ResponsableFormationDAO();
		return responsableFormationDAO.findAll();
	}

	public ResponsableFormation responsableDepuisNom(String nomResponsable) {
		ResponsableFormationDAO responsableFormationDAO = new ResponsableFormationDAO();
		return responsableFormationDAO.responsableDepuisNom(nomResponsable);
	}

	public ResponsableFormation remplirResponsableFormation(ResultSet rs) throws SQLException {
		ResponsableFormationDAO responsableFormationDAO = new ResponsableFormationDAO();
		return responsableFormationDAO.remplirResponsableFormation(rs);
		
	}
}
