package fr.filrouge.Manager;

import java.util.ArrayList;
import java.util.List;

import fr.filrouge.DAO.PlanificationFormationDAO;
import fr.filrouge.objets.ClientInter;
import fr.filrouge.objets.Formation;
import fr.filrouge.objets.PlanificationFormation;

public class PlanificationFormationManager {

	/*
	 * public void enregistrer(PlanificationFormation planificationFormation) {
	 * PlanificationFormationDAO planificationFormationDAO = new
	 * PlanificationFormationDAO();
	 * planificationFormationDAO.enregistrer(planificationFormation); }
	 */

	public List<PlanificationFormation> findAll() {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		return planificationFormationDAO.findAll();
	}

	/*public List<PlanificationFormation> findById(int formationId) {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		return planificationFormationDAO.findById();
	}*/

	public List<PlanificationFormation> findByIdFormation(int idFormation){
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		return planificationFormationDAO.findByIdFormation(idFormation);
	}


	public void inscriptionClientInter(int idPlanification, int idClientInter) {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		planificationFormationDAO.inscriptionClientInter(idPlanification, idClientInter) ;
		
	}

	public int nombreInscrit(int idPlanification) {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		return planificationFormationDAO.nombreInscrit(idPlanification);
	}

	public void validerFormation(int idPlanification) {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		planificationFormationDAO.validerFormation(idPlanification);
	}

}

