package fr.filrouge.Manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.filrouge.DAO.ClientInterDao;
import fr.filrouge.objets.ClientInter;
import fr.filrouge.objets.PlanificationFormation;

public class ClientInterManager {

Map<String,String> erreurs = new HashMap<>();
	
	public Map<String,String> verif(String mail, String passwrd, String confirmPasswrd) {
		verifMail(mail);
		verifPasswrd(passwrd, confirmPasswrd);
		return erreurs;
	}

	
	public void verifMail(String mail) {
		if(!(mail.trim().endsWith(".fr") || mail.endsWith(".com"))) {
			erreurs.put("mail", "L'email doit terminer par .com ou .fr");
		}
	}
	
	public void verifPasswrd(String passwrd, String confirmPasswrd) {
		if(!passwrd.equals(confirmPasswrd)) {
			erreurs.put("confirmPasswrd", "Les mots de passes ne sont pas identiques");
		}
	}
	
	public void enregistre(ClientInter clientInter) {
		ClientInterDao clientInterDao = new ClientInterDao();
		clientInterDao.enregistrer(clientInter);
	}


	public ClientInter findByMailAndPasswrd(String mail, String passwrd) {
		ClientInterDao clientInterDao = new ClientInterDao();
		ClientInter clientInter = clientInterDao.findByMailAndPasswrd(mail, passwrd);
		return clientInter;
	
	}


	public List<PlanificationFormation> formationsByClient(ClientInter clientInter) {
		ClientInterDao clientInterDao = new ClientInterDao();
		return clientInterDao.formationsByClient(clientInter);
	}
	
	public void deleteByID(int idPlanification,int idClient) {
		ClientInterDao clientInterDao = new ClientInterDao();
		clientInterDao.deleteInscription(idPlanification, idClient);
	}
}