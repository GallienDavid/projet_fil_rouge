package fr.filrouge.Manager;

import java.util.HashMap;
import java.util.Map;

import fr.filrouge.DAO.ClientInterDao;
import fr.filrouge.DAO.ClientIntraDao;
import fr.filrouge.objets.ClientInter;
import fr.filrouge.objets.ClientIntra;



public class ClientIntraManager {

Map<String,String> erreurs = new HashMap<>();
	
	public Map<String,String> verif(String mail, String passwrd, String confirmPasswrd) {
		verifMail(mail);
		verifPasswrd(passwrd, confirmPasswrd);
		return erreurs;
	}

	
	public void verifMail(String mail) {
		if(!(mail.trim().endsWith(".fr") || mail.endsWith(".com"))) {
			erreurs.put("mail", "L'email doit terminer par .com ou .fr");
		}
	}
	
	public void verifPasswrd(String passwrd, String confirmPasswrd) {
		if(!passwrd.equals(confirmPasswrd)) {
			erreurs.put("confirmPasswrd", "Les mots de passes ne sont pas identiques");
		}
	}



	
	public void enregistrer(ClientIntra clientIntra) {
		ClientIntraDao clientIntraDao = new ClientIntraDao();
		clientIntraDao.enregistrer(clientIntra);
	}


	public ClientIntra findByMailAndPasswrd(String mail, String passwrd) {
		ClientIntraDao clientIntraDao = new ClientIntraDao();
		ClientIntra clientIntra = clientIntraDao.findByMailAndPasswrd(mail, passwrd);
		return clientIntra;
	}
}
	
	

