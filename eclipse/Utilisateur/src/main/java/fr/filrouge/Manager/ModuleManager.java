package fr.filrouge.Manager;

import java.util.List;

import fr.filrouge.*;
import fr.filrouge.DAO.ModuleDAO;
import fr.filrouge.objets.Module;

public class ModuleManager {
	
	public void enregistrer(Module module) {
		ModuleDAO moduleDAO = new ModuleDAO();
		// NB : on pourrait chiffrer le MDP ici, c'est le bon endroit
		ModuleDAO.enregistrer(module);
	}

	public List<Module> findAll() {
		ModuleDAO moduleDAO = new ModuleDAO();
		return moduleDAO.findAll();
	}

	public static Module moduleDepuisNom(String nomModule) {
		ModuleDAO moduleDAO = new ModuleDAO();
		return moduleDAO.findModuleDepuisNom(nomModule);
	}

}
