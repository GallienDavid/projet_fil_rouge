package fr.filrouge.Manager;

import java.util.List;

import fr.filrouge.DAO.AgenceDAO;
import fr.filrouge.objets.Agence;


public class AgenceManager {

	public List<Agence> findAll() {
		AgenceDAO agenceDAO = new AgenceDAO();
		return agenceDAO.findAll();
	}

	public List<Agence> findDepuisZoneGeo(String zoneGeo) {
		AgenceDAO agenceDAO = new AgenceDAO();
		return agenceDAO.findDepuisZoneGeo(zoneGeo);
	}
	
	
	
}
