package fr.filrouge.Manager;

import java.util.List;

import fr.filrouge.DAO.ThemeDAO;
import fr.filrouge.objets.Theme;

public class ThemeManager {
	
	public List<Theme> findAll() {
		ThemeDAO themeDAO = new ThemeDAO();
		return themeDAO.findAll();
	}

}
