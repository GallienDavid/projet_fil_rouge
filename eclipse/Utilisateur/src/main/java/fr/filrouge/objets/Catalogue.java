package fr.filrouge.objets;

import java.util.List;

public class Catalogue {

	private List<Formation> formation;
	private List<Module> module;
	private List<ResponsableFormation> responsableFormation;
	private List<Theme> theme;
	private List<SousTheme> sousTheme;
	
	
	public Catalogue() {

	}

	public Catalogue(List<Formation> formation, List<Module> module, List<ResponsableFormation> responsableFormation,
			List<Theme> theme, List<SousTheme> sousTheme) {
		super();
		this.formation = formation;
		this.module = module;
		this.responsableFormation = responsableFormation;
		this.theme = theme;
		this.sousTheme = sousTheme;
	}




	public List<Formation> getFormation() {
		return formation;
	}

	public void setFormation(List<Formation> formation) {
		this.formation = formation;
	}

	public List<Module> getModule() {
		return module;
	}

	public void setModule(List<Module> module) {
		this.module = module;
	}




	public List<ResponsableFormation> getResponsableFormation() {
		return responsableFormation;
	}




	public void setResponsableFormation(List<ResponsableFormation> responsableFormation) {
		this.responsableFormation = responsableFormation;
	}




	public List<Theme> getTheme() {
		return theme;
	}




	public void setTheme(List<Theme> theme) {
		this.theme = theme;
	}




	public List<SousTheme> getSousTheme() {
		return sousTheme;
	}




	public void setSousTheme(List<SousTheme> sousTheme) {
		this.sousTheme = sousTheme;
	}




	@Override
	public String toString() {
		return "Catalogue [formation=" + formation + ", module=" + module + ", responsableFormation="
				+ responsableFormation + ", theme=" + theme + ", sousTheme=" + sousTheme + "]";
	}
	
	
}
