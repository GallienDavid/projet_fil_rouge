package fr.filrouge.objets;

import java.util.List;

public class Theme {
	private int id ;
	private String nom ;
	private List<SousTheme> sousTheme;
	
	
	
	public List<SousTheme> getSousTheme() {
		return sousTheme;
	}



	public void setSousTheme(List<SousTheme> sousTheme) {
		this.sousTheme = sousTheme;
	}



	public Theme() {
		super();
	}



	public Theme(int id, String nom, List<SousTheme> sousTheme) {
		super();
		this.id = id;
		this.nom = nom;
		this.sousTheme = sousTheme ;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	@Override
	public String toString() {
		return "Theme [id=" + id + ", nom=" + nom + ", sousTheme=" + sousTheme + "]";
	}



	
	
	
	
	

}
