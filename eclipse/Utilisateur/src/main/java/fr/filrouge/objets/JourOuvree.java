package fr.filrouge.objets;


import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class JourOuvree {
	
	public static LocalDate jourOuvree(LocalDate dateDebut, int nbrJour) {
		Date date = Date.from(dateDebut.atStartOfDay(ZoneId.systemDefault()).toInstant());
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		int businessDayCounter = 0;
		while (businessDayCounter < nbrJour) {
		    
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		    if (dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY) {
		        businessDayCounter++;
		    }
		    cal.add(Calendar.DAY_OF_YEAR, 1);
		}
		
		cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate() ;
		
		return cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate() ; 
		
	}
	
	
}
