package fr.filrouge.objets;

import java.time.LocalDate;


public class PlanificationFormation {
	
	private int id;
	private Formation formation;
	private Salle salle;
	private LocalDate dateDebut;
	private int cible;
	private int nbInscrit ;
	
	
	
	public int getNbInscrit() {
		return nbInscrit;
	}

	public void setNbInscrit(int nbInscrit) {
		this.nbInscrit = nbInscrit;
	}

	public PlanificationFormation() {
		// TODO Auto-generated constructor stub
	}

	public PlanificationFormation(Formation formation, Salle salle, String dateDebut, String cible) {
		super();
		this.formation = formation;
		this.salle = salle;
		this.dateDebut = LocalDate.parse(dateDebut);
		if(cible.equals("inter")) {
			this.cible = 0;
		}else {
			this.cible = 1;
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public int getCible() {
		return cible;
	}

	public void setCible(int cible) {
		this.cible = cible;
	}

	@Override
	public String toString() {
		return "PlanificationFormation [id=" + id + ", formation=" + formation + ", salle=" + salle + ", dateDebut="
				+ dateDebut + ", cible=" + cible + "]";
	}
	
	public LocalDate dateFin() {
		return JourOuvree.jourOuvree(this.dateDebut, this.formation.duree());
	}
	
	public String enCours() {
		LocalDate date = LocalDate.now() ;
		if(this.dateDebut.isAfter(date)) {
			return "A venir" ;
			
		}else if(this.dateFin().isBefore(date)) {
			return "Terminer" ;
		}else {
			return "En cours" ;
		}
	}

}
