package fr.filrouge.objets;

public class Module {
	
	private int ID_MODULE;
	private String nomModule;
	private float prix;
	private int nombreJour;
	
	public Module() {
		// TODO Auto-generated constructor stub
	}

	public Module(String nomModule, String prix, String nombreJour) {
		super();
		this.nomModule = nomModule;
		this.prix = Float.valueOf(prix);
		this.nombreJour = Integer.valueOf(nombreJour);
	}
	
	

	public int getID_MODULE() {
		return ID_MODULE;
	}

	public void setID_MODULE(int iD_MODULE) {
		ID_MODULE = iD_MODULE;
	}

	public String getNomModule() {
		return nomModule;
	}

	public void setNomModule(String nomModule) {
		this.nomModule = nomModule;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public int getNombreJour() {
		return nombreJour;
	}

	public void setNombreJour(int nombreJour) {
		this.nombreJour = nombreJour;
	}

	@Override
	public String toString() {
		//return "Module [nomModule=" + nomModule + ", prix=" + prix + ", nombreJour=" + nombreJour + "]";
		return "Module : " + nomModule + " " + prix + " " + nombreJour;
	}

}
