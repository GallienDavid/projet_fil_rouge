package fr.filrouge.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.filrouge.objets.SousTheme;
import fr.filrouge.objets.Theme;




public class ThemeDAO {
	
	public List<Theme> findAll() {
		List<Theme> themes = new ArrayList<>();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "SELECT * FROM THEMES";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Theme theme  = remplirTheme(rs);	
				themes.add(theme);
			}
			for (int i = 0; i < themes.size(); i++) {
				query = " SELECT SOUS_THEMES.ID_SOUS_THEME, NOM_SOUS_THEME FROM THEMES JOIN THEME_SOUS_THEMES ON THEMES.ID_THEME = THEME_SOUS_THEMES.ID_THEME JOIN SOUS_THEMES ON SOUS_THEMES.ID_SOUS_THEME = THEME_SOUS_THEMES.ID_SOUS_THEME WHERE THEMES.ID_THEME= " + themes.get(i).getId() + ";" ;
				pstmt = connexion.prepareStatement(query);	
				//System.out.println(query);
				rs = pstmt.executeQuery();	
				List<SousTheme> sousThemes = new ArrayList();
				while(rs.next()) {
					SousTheme sousTheme = new SousTheme() ;
					sousTheme = remplirSousTheme(rs);
					sousThemes.add(sousTheme);
				}
				themes.get(i).setSousTheme(sousThemes);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return themes;
	}

	private Theme remplirTheme(ResultSet rs) throws SQLException {
			Theme theme;
			theme = new Theme();
			theme.setId(rs.getInt("ID_THEME"));
			theme.setNom(rs.getString("NOM_THEME"));					
		return theme;
	}
	
	private SousTheme remplirSousTheme(ResultSet rs) throws SQLException {
		SousTheme sousTheme ;
		sousTheme = new SousTheme();
		sousTheme.setId(rs.getInt("ID_SOUS_THEME"));
		sousTheme.setNom(rs.getString("NOM_SOUS_THEME"));
		
		return sousTheme;
		
	}

}
