package fr.filrouge.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.filrouge.Manager.PlanificationFormationManager;
import fr.filrouge.objets.ClientInter;
import fr.filrouge.objets.Module;
import fr.filrouge.objets.PlanificationFormation;

public class ClientInterDao {

	public void enregistrer(ClientInter clientInter) {

		// 1 - Se connecter � la BDD (penser � mettre le driver dans le r�pertoire lib
		// du tomcat)
		try {

			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete

			String query = "INSERT INTO CLIENTS_INTER VALUES(?,?,?,?,CONVERT(NVARCHAR(32),"
					+ "HASHBYTES('MD5', cast(? as varchar)),2))";
			// 3 - Ex�cuter la requete

			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, clientInter.getNom());
			pstmt.setString(2, clientInter.getPrenom());
			pstmt.setString(3, clientInter.getMail());
			pstmt.setString(4, clientInter.getTelephone());
			pstmt.setString(5, clientInter.getPasswrd());

			pstmt.executeUpdate();

			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public ClientInter findByMailAndPasswrd(String mail, String passwrd) {
		ClientInter clientInter = null;
		try {
			Connection connexion = PoolConnexion.getConnexion();

			String query = "SELECT * FROM CLIENTS_INTER WHERE mail = ? and PASSWRD = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";

			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, mail);
			pstmt.setString(2, passwrd);

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				clientInter = connexionClientInter(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clientInter;
	}

	private static ClientInter connexionClientInter(ResultSet rs) throws SQLException {
		ClientInter clientInter = new ClientInter();
		clientInter.setId_Client(rs.getInt("id_Client"));
		clientInter.setNom(rs.getString("nom"));
		clientInter.setPrenom(rs.getString("prenom"));
		clientInter.setMail(rs.getString("mail"));
		clientInter.setTelephone(rs.getString("telephone"));
		clientInter.setPasswrd(rs.getString("passwrd"));
		return clientInter;
	}

	public List<PlanificationFormation> formationsByClient(ClientInter clientInter) {
		List<PlanificationFormation> planificationFormations = new ArrayList<PlanificationFormation>();
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT PLANIFICATION_FORMATION.ID_SALLE, PLANIFICATION_FORMATION.DATE_DEBUT, PLANIFICATION_FORMATION.ID_PLANIFICATION, PLANIFICATION_FORMATION.VALIDER, PLANIFICATION_FORMATION.CIBLE ,NOM_FORMATION, FORMATION.ID_FORMATION,RESPONSABLE_FORMATION.ID_RESPONSABLE, NOM_RESPONSABLE,PRENOM_RESPONSABLE,MAIL_RESPONSABLE,TELEPHONE_RESPONSABLE FROM PLANIFICATION_FORMATION JOIN FORMATION ON\r\n"
				+ "					PLANIFICATION_FORMATION.ID_FORMATION = FORMATION.ID_FORMATION JOIN RESPONSABLE_FORMATION on FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE JOIN INTER_PLANIFICATION p ON PLANIFICATION_FORMATION.ID_PLANIFICATION = p.ID_PLANIFICATION WHERE ID_CLIENT ="
				+ clientInter.getId_Client() + ";";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
				PlanificationFormation planificationFormation = planificationFormationDAO.remplirPlanificationFormation(rs);
				planificationFormations.add(planificationFormation);
			}
			for (int i = 0; i < planificationFormations.size(); i++) {
				query = "SELECT  MODULE.ID_MODULE , NOM_MODULE , PRIX , NOMBRE_JOUR"
						+ " FROM FORMATION JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION"
						+ " JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE WHERE FORMATION.ID_FORMATION = "
						+ planificationFormations.get(i).getFormation().getId() + ";";
				pstmt = connexion.prepareStatement(query);
				// System.out.println(query);
				rs = pstmt.executeQuery();
				ModuleDAO moduleManager = new ModuleDAO();
				List<Module> modules = new ArrayList();
				while (rs.next()) {
					Module module = moduleManager.remplirModule(rs);
					modules.add(module);
				}
				planificationFormations.get(i).getFormation().setModules(modules);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return planificationFormations;
	}
	
	public void deleteInscription(int idPlanification, int idClient) {
		Connection connexion = PoolConnexion.getConnexion();
		String query = "DELETE FROM INTER_PLANIFICATION WHERE ID_PLANIFICATION = " + idPlanification + " AND ID_CLIENT = " +idClient+ " ;" ;
		try {

			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.executeUpdate();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
