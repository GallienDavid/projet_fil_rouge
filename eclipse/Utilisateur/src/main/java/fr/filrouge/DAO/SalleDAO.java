package fr.filrouge.DAO;


	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.ArrayList;
	import java.util.List;

import fr.filrouge.objets.Agence;
import fr.filrouge.objets.Salle;


	public class SalleDAO {

		public List<Salle> findAll() {

			List<Salle> salles = new ArrayList<>();

			// 1 - R�cup�rer la connexion
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM SALLES JOIN AGENCE ON SALLES.ID_AGENCE = AGENCE.ID_AGENCE";
			try {
				PreparedStatement pstmt = connexion.prepareStatement(query);
				// 3 - Ex�cuter la requete
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					Salle salle = remplirSalle(rs);
					salles.add(salle);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return salles;
		}

		private Salle remplirSalle(ResultSet rs) throws SQLException {
			Salle salle;
			salle = new Salle();
			Agence agence;
			agence = new Agence();
			agence.setId(rs.getInt("ID_AGENCE"));
			agence.setZoneGeo(rs.getString("ZONE_GEOGRAHIQUE"));
			agence.setTel(rs.getString("TELEPHONE_AGENCE"));
			agence.setAdresse(rs.getString("ADRESSE_AGENCE")); // le mdp stock� est hach� ici
			agence.setNomResponsable(rs.getString("NOM_RESPONSABLE"));
			agence.setPrenomResponsable(rs.getString("PRENOM_RESPONSABLE"));
			agence.setMailResponsable(rs.getString("CONTACT_MAIL_RESPONSABLE"));
			agence.setTelResponsable(rs.getString("CONTACT_TELEPHONE_RESPONSABLE"));
			salle.setAgence(agence);

			salle.setId(rs.getInt("ID_SALLE"));
			salle.setNbPlaces(rs.getInt("NB_PLACES"));
			salle.setNom(rs.getString("NOM_SALLE"));

			return salle;
		}

		public Salle salleDepuisNom(String salleNom) {
			// 1 - R�cup�rer la connexion
			Salle salle = new Salle();
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM SALLES JOIN AGENCE ON SALLES.ID_AGENCE = AGENCE.ID_AGENCE WHERE NOM_SALLE = "
					+ "'" + salleNom + "';";
			try {
				PreparedStatement pstmt = connexion.prepareStatement(query);
				// 3 - Ex�cuter la requete
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					salle = remplirSalle(rs);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return salle;
		}

		public List<Salle> salleDepuisZoneGeo(String zoneGeo) {
			// 1 - R�cup�rer la connexion
			List<Salle> salles = new ArrayList<>();
			Connection connexion = PoolConnexion.getConnexion();
			// 2 - Fabriquer la requete
			String query = "SELECT * FROM SALLES JOIN AGENCE ON SALLES.ID_AGENCE = AGENCE.ID_AGENCE WHERE ZONE_GEOGRAHIQUE = "
					+ "'" + zoneGeo + "';";
			try {
				PreparedStatement pstmt = connexion.prepareStatement(query);
				// 3 - Ex�cuter la requete
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					Salle salle = new Salle();
					salle = remplirSalle(rs);
					salles.add(salle);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			System.out.println(salles);
			return salles;
		}

		public Salle salleByID(int idSalle) {
			// 1 - R�cup�rer la connexion
					Salle salle = new Salle();
					Connection connexion = PoolConnexion.getConnexion();
					// 2 - Fabriquer la requete
					String query = "SELECT * FROM SALLES JOIN AGENCE ON SALLES.ID_AGENCE = AGENCE.ID_AGENCE WHERE ID_SALLE = "
							+ "'" + idSalle + "';";
					try {
						PreparedStatement pstmt = connexion.prepareStatement(query);
						// 3 - Ex�cuter la requete
						ResultSet rs = pstmt.executeQuery();
						while (rs.next()) {
							salle = remplirSalle(rs);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
					return salle;
		}

	}


