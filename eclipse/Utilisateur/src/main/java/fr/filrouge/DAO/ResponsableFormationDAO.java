package fr.filrouge.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.filrouge.objets.ResponsableFormation;



public class ResponsableFormationDAO {
	
	public List<ResponsableFormation> findAll() {
		
		List<ResponsableFormation> responsableFormations = new ArrayList<>();
		
		// 1 - R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 - Fabriquer la requete
		String query = "SELECT * FROM RESPONSABLE_FORMATION";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 - Ex�cuter la requete
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				ResponsableFormation responsableFormation = remplirResponsableFormation(rs);
				responsableFormations.add(responsableFormation);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//System.out.println(responsableFormations);
		return responsableFormations;
	}

	public ResponsableFormation remplirResponsableFormation(ResultSet rs) throws SQLException {
		ResponsableFormation responsableFormation;
		responsableFormation = new ResponsableFormation();
		responsableFormation.setId(rs.getInt("ID_RESPONSABLE"));
		responsableFormation.setNom(rs.getString("NOM_RESPONSABLE"));
		responsableFormation.setPrenom(rs.getString("PRENOM_RESPONSABLE"));
		responsableFormation.setTel(rs.getString("TELEPHONE_RESPONSABLE")); // le mdp stock� est hach� ici
		responsableFormation.setMail(rs.getString("MAIL_RESPONSABLE"));
		//System.out.println(responsableFormation);
		return responsableFormation;
	}

	public ResponsableFormation responsableDepuisNom(String nomResponsable) {
		ResponsableFormation responsable = new ResponsableFormation();
		// 1 R�cup�rer la connexion
		Connection connexion = PoolConnexion.getConnexion();
		// 2 Fabriquer la requ�te
		String query = "SELECT * FROM RESPONSABLE_FORMATION WHERE NOM_RESPONSABLE = " + "'" + nomResponsable + "';";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				responsable  = remplirResponsableFormation(rs);	
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responsable;
		
		
	}
	
	

}
