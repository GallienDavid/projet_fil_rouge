package fr.filrouge.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import fr.filrouge.objets.Module;
import fr.filrouge.objets.PlanificationFormation;
import fr.filrouge.Manager.ResponsableFormationManager;
import fr.filrouge.objets.Formation;
import fr.filrouge.objets.ResponsableFormation;

public class FormationDAO {

	/*public void enregistrer(Formation formation) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO FORMATION VALUES(?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, formation.getNom());
			pstmt.setFloat(2, formation.getResponsable().getId());
			pstmt.executeUpdate(); 
			
			
			query = "INSERT INTO MODULE_FORMATION VALUES(?,?)";
			pstmt = connexion.prepareStatement(query);
			int id = findIdFormationDepuisNom(formation.getNom());
			//System.out.println(id);
			int nbModule = formation.getModules().size();
			
			for (int i = 0; i < formation.getModules().size(); i++) {
				formation.getModules().get(i).getID_MODULE();
				
				pstmt.setFloat(1,id);
				pstmt.setFloat(2, formation.getModules().get(i).getID_MODULE());
				pstmt.executeUpdate(); 
	
			}
			
			
			connexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}*/
	
	
	public int findIdFormationDepuisNom(String nomFormation) {
		int id = 0 ;
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT ID_FORMATION FROM FORMATION WHERE NOM_FORMATION = " + "'" + nomFormation + "';";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				id = rs.getInt("ID_FORMATION");	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public List<Formation> findAll() {
		PlanificationFormationDAO planificationDAO = new PlanificationFormationDAO();
		List<Formation> formations = new ArrayList<>();
		Connection connexion = PoolConnexion.getConnexion();

		String query = "SELECT FORMATION.ID_FORMATION , NOM_FORMATION , FORMATION.ID_RESPONSABLE ,NOM_RESPONSABLE , PRENOM_RESPONSABLE,TELEPHONE_RESPONSABLE , MAIL_RESPONSABLE FROM FORMATION JOIN RESPONSABLE_FORMATION ON FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Formation formation  = remplirFormationWithoutModule(rs);	
				formations.add(formation);
			}
			for (int i = 0; i < formations.size(); i++) {
				query = "SELECT  MODULE.ID_MODULE , NOM_MODULE , PRIX , NOMBRE_JOUR"
						+ " FROM FORMATION JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION"
						+ " JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE WHERE FORMATION.ID_FORMATION = " + formations.get(i).getId() + ";" ;
				pstmt = connexion.prepareStatement(query);	
				rs = pstmt.executeQuery();	
				ModuleDAO moduleManager = new ModuleDAO() ;
				List<Module> modules = new ArrayList();
				while(rs.next()) {
					Module module ;
					module = new Module();
					module.setID_MODULE(rs.getInt("ID_MODULE"));
					module.setNomModule(rs.getString("NOM_MODULE"));
					module.setPrix(rs.getFloat("PRIX"));
					module.setNombreJour(rs.getInt("NOMBRE_JOUR"));	
					modules.add(module);		
				}
				//List<PlanificationFormation> planis = planificationDAO.findByIdFormation(formations.get(i).getId());
				//System.out.println(modules);
				formations.get(i).setModules(modules);
				//formations.get(i).setPlani(planis);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formations;
	}


	public Formation remplirFormationWithoutModule(ResultSet rs) throws SQLException {
		
		Formation formation;
		formation = new Formation();
		formation.setId(rs.getInt("ID_FORMATION"));
		formation.setNom(rs.getString("NOM_FORMATION"));
		formation.setNbInscritMini(0);
		
		ResponsableFormationManager responsableManager = new ResponsableFormationManager() ;
		
		ResponsableFormation responsableFormation = responsableManager.remplirResponsableFormation(rs);
		formation.setResponsable(responsableFormation);
		
		/*ModuleDAO moduleDAO = new ModuleDAO();
		Module module = moduleDAO.remplirModule(rs);
		formation.getModules().add(module);*/
		
		return formation;
		
		
	}


	public Formation FormationById(int idFormation) {
		PlanificationFormationDAO planificationDAO = new PlanificationFormationDAO();
		Formation formation = new Formation();
		
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT FORMATION.ID_FORMATION , NOM_FORMATION , FORMATION.ID_RESPONSABLE ,NOM_RESPONSABLE , PRENOM_RESPONSABLE,TELEPHONE_RESPONSABLE , MAIL_RESPONSABLE FROM FORMATION JOIN RESPONSABLE_FORMATION ON FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE WHERE FORMATION.ID_FORMATION = " + "'" + idFormation + "';";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
		ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
			formation  = remplirFormationWithoutModule(rs);	
			}
			query = "SELECT  MODULE.ID_MODULE , NOM_MODULE , PRIX , NOMBRE_JOUR"
		+ " FROM FORMATION JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION"
			+ " JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE WHERE FORMATION.ID_FORMATION = " + idFormation + ";" ;
			
		pstmt = connexion.prepareStatement(query);
	rs = pstmt.executeQuery();
			ModuleDAO moduleManager = new ModuleDAO() ;
			List<Module> modules = new ArrayList();
	while(rs.next()) {
		Module module ;
		module = new Module();
				/*System.out.println(rs.getInt("ID_MODULE"));
				System.out.println(rs.getString("NOM_MODULE"));
				System.out.println(rs.getFloat("PRIX"));
				System.out.println(rs.getInt("NOMBRE_JOUR"));*/
		module.setID_MODULE(rs.getInt("ID_MODULE"));
		module.setNomModule(rs.getString("NOM_MODULE"));
		module.setPrix(rs.getFloat("PRIX"));
		module.setNombreJour(rs.getInt("NOMBRE_JOUR"));	
		modules.add(module);	
	}
	List<PlanificationFormation> planis = planificationDAO.findByIdFormation(formation.getId());
	
			System.out.println(modules);
			formation.setModules(modules);
			//formation.setPlani(planis);
				} catch (SQLException e) {
	//		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formation;
	}
	
	

}
