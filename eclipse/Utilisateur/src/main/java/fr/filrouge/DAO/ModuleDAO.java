package fr.filrouge.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import fr.filrouge.objets.Module;


public class ModuleDAO {
	
		public static void enregistrer(Module module) {
				
				try {
					Connection connexion = PoolConnexion.getConnexion();
					String query = "INSERT INTO MODULE VALUES(?,?,?)";
					PreparedStatement pstmt = connexion.prepareStatement(query);
					pstmt.setString(1, module.getNomModule());
					pstmt.setFloat(2, module.getPrix());
					pstmt.setInt(3, module.getNombreJour());
					pstmt.executeUpdate(); 
					connexion.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		
			public List<Module> findAll() {
				List<Module> modules = new ArrayList<>();
				// 1 R�cup�rer la connexion
				Connection connexion = PoolConnexion.getConnexion();
				// 2 Fabriquer la requ�te
				String query = "SELECT * FROM MODULE";
				try {
					PreparedStatement pstmt = connexion.prepareStatement(query);
					// 3 Ex�cuter la requ�te
					ResultSet rs = pstmt.executeQuery();
					while(rs.next()) {
						Module module  = remplirModule(rs);	
						modules.add(module);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return modules;
			}
		
			public Module remplirModule(ResultSet rs) throws SQLException {
				Module module;
				module = new Module();
				module.setID_MODULE(rs.getInt("ID_MODULE"));
				module.setNomModule(rs.getString("NOM_MODULE"));
				module.setPrix(rs.getFloat("PRIX"));
				module.setNombreJour(rs.getInt("NOMBRE_JOUR"));
				return module;
			}
		
			public Module findModuleDepuisNom(String nomModule) {
				Module module = new Module();
				// 1 R�cup�rer la connexion
				Connection connexion = PoolConnexion.getConnexion();
				// 2 Fabriquer la requ�te
				String query = "SELECT * FROM MODULE WHERE NOM_MODULE = " + "'" + nomModule + "';";
				try {
					PreparedStatement pstmt = connexion.prepareStatement(query);
					// 3 Ex�cuter la requ�te
					ResultSet rs = pstmt.executeQuery();
					while(rs.next()) {
						module  = remplirModule(rs);	
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return module;
			}

}
