<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Test</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<!-- Boxicons CDN Link -->
<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css'
	rel='stylesheet'>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="CSS/body.css">
<link rel="stylesheet" href="CSS/style.css">
<link rel="stylesheet" href="CSS/acceuil.css" type="text/css" />

</head>
<body>

	<form method="POST" action="inscriptionFormation">
		<div class="vstack gap-3">
			<div class="bg-light border">
				<div class="mb-3">
					<h5>Comment d�clare-t-on une variable de type nombre entier ?</h5>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionA"
							id="flexRadioDefault1" value="a" checked> <label class="form-check-label"
							for="flexRadioDefault1"> a - integer n </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionA"
							id="flexRadioDefault2" value="b"> <label class="form-check-label"
							for="flexRadioDefault2"> b - entier n </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionA"
							id="flexRadioDefault3" value="c"> <label class="form-check-label"
							for="flexRadioDefault3"> c - int n </label>
					</div>
				</div>
			</div>
			<div class="bg-light border">
				<div class="mb-3">
					<h5>Que m'affiche la ligne " 5 < 2 " ?</h5>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionB"
							id="flexRadioDefault1" value="a" checked> <label class="form-check-label"
							for="flexRadioDefault1"> a - false </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionB"
							id="flexRadioDefault2" value="b"> <label class="form-check-label"
							for="flexRadioDefault2"> b - error </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionB"
							id="flexRadioDefault3" value="c"> <label class="form-check-label"
							for="flexRadioDefault3"> c - true </label>
					</div>
				</div>
			</div>
			<div class="bg-light border">
				<div class="mb-3">
					<h5>Comment s'appelle l'action de transformer du code �crit en
						fichier ex�cutable par la machine ?</h5>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionC"
							id="flexRadioDefault1" value="a" checked> <label class="form-check-label"
							for="flexRadioDefault1"> a - compiler </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionC"
							id="flexRadioDefault2" value="b"> <label class="form-check-label"
							for="flexRadioDefault2"> b - interpr�ter </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionC"
							id="flexRadioDefault3" value="c"> <label class="form-check-label"
							for="flexRadioDefault3"> c - machiner </label>
					</div>
				</div>
			</div>
			<div class="bg-light border">
				<div class="mb-3">
					<h5>A quel type de paradigme de langage informatique
						appartient le langage Java ?</h5>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionD"
							id="flexRadioDefault1" value="a" checked> <label class="form-check-label"
							for="flexRadioDefault1"> a - Orient� objet </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionD"
							id="flexRadioDefault2" value="b"> <label class="form-check-label"
							for="flexRadioDefault2"> b - Imp�rative </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionD"
							id="flexRadioDefault3" value="c"> <label class="form-check-label"
							for="flexRadioDefault3"> c - D�clarative </label>
					</div>
				</div>
			</div>
			<div class="bg-light border">
				<div class="mb-3">
					<h5>Que m'affiche " for(i = 0; i < 6; i = i+3) {afficher(i)}"
						?</h5>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionE"
							id="flexRadioDefault1" value="a" checked> <label class="form-check-label"
							for="flexRadioDefault1"> a - 0 3 </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionE"
							id="flexRadioDefault2" value="b"> <label class="form-check-label"
							for="flexRadioDefault2"> b - 0 3 6 </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionE"
							id="flexRadioDefault3" value="c"> <label class="form-check-label"
							for="flexRadioDefault3"> c - 1 4 </label>
					</div>
				</div>
			</div>
			<div class="bg-light border">
				<div class="mb-3">
					<h5>David a 5/6 de l'�ge de Florentine, Florentine aura 40 ans
						dans 10 ans, quel age aura David dans 10 ans ?</h5>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionF"
							id="flexRadioDefault1" value="a" checked> <label class="form-check-label"
							for="flexRadioDefault1"> a - 31 ans </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionF"
							id="flexRadioDefault2" value="b"> <label class="form-check-label"
							for="flexRadioDefault2"> b - 35 ans </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="questionF"
							id="flexRadioDefault3" value="c"> <label class="form-check-label"
							for="flexRadioDefault3"> c - 37 ans </label>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" value = ${idPlanification } name="idPlanification"/>
		<input type="submit" value="Valider" />

	</form>

</body>
</html>