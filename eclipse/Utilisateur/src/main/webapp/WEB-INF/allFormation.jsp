<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Boxicons CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/body.css">
    <link rel="stylesheet" href="CSS/style.css">
</head>
<body>
<%@ include file="newHeader.jspf" %>
<div class="corps">
	<div class="row">
		 
			<c:forEach items="${formations }" var="formationCourant">
				<div class="col-sm-6">
					<div class="card">
					  <div class="card-header">
					    <h5>${formationCourant.nom }</h5>
					  </div>
					  <div class="card-body">
					    <h5 class="card-title"> Responsable : ${formationCourant.responsable.nom} ${formationCourant.responsable.prenom}</h5>
					    <p> Prix : ${formationCourant.prix()} €</p>
					     <p> Durée : ${formationCourant.duree()} jour(s)</p>
					     <p>Module(s) : </p>
						    <c:forEach items="${formationCourant.modules}" var="module">
								<span>${module.nomModule} | </span>
							</c:forEach>
					    <br>
					    <br>
					    <c:forEach items="${formationCourant.plani}" var="plani">
								<span>${plani.dateDebut} | </span>
						</c:forEach>
					    <form action="detailsformation" method="post"><button type="submit" value="${formationCourant.id}" name="idformation" class="btn btn-primary">Plus d'informations</button></input></form>
					    <form action="inscriptionFormation" method="post"><button type="submit" value="${formationCourant.id}" name="idformation" class="btn btn-primary">Plus d'informations</button></input></form>
					  </div>
					</div>
					<br>
				</div>
			</c:forEach>
		
	</div>

</div>

<script src="CSS/script.js"></script>
</body>
</html>