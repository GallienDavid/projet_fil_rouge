<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Accueil</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<!-- Boxicons CDN Link -->
<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css'
	rel='stylesheet'>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="CSS/body.css">
<link rel="stylesheet" href="CSS/style.css">
<link rel="stylesheet" href="CSS/accueil.css" type="text/css" />

</head>

<body>
	<%@ include file="newHeader.jspf"%>
	</br>
	</br>
	</br>

	<Section>
		<div id="divid">
			<div id="carouselExampleCaptions" class="carousel slide"
				data-bs-ride="carousel">
				<div class="carousel-indicators">
					<button type="button" data-bs-target="#carouselExampleCaptions"
						data-bs-slide-to="0" class="active" aria-current="true"
						aria-label="Slide 1"></button>
					<button type="button" data-bs-target="#carouselExampleCaptions"
						data-bs-slide-to="1" aria-label="Slide 2"></button>
					<button type="button" data-bs-target="#carouselExampleCaptions"
						data-bs-slide-to="2" aria-label="Slide 3"></button>
				</div>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img id="imageHomme" src="image/homme.jpg"
							alt="Un homme pour rejoindre IT-Training"
							title="Un homme pour rejoindre IT-Training">
						<div class="carousel-caption d-none d-md-block">
							<h5 id="pcarousel">
								Bienvenue chez IT-Training </br> Vous souhaitez vous inscrire et
								faire une formation?
							</h5>
							<a href="inscription"><button type="button"
									class="btn btn-warning">
									<strong>Rejoignez l'aventure </strong>
								</button></a>
						</div>
					</div>
					<div class="carousel-item">
						<img id="exemple" src="image/exemple.jpg"
							alt="Un bureau pour décrire la meilleur formation de 2019"
							title="La meilleur formation de 2019">
						<div class="carousel-caption d-none d-md-block">
							<h5 class="color_carousel2">
								IT-Training a été élue meilleure plateforme de formation </br>
								informatique de 2019
							</h5>
						</div>
					</div>
					<div class="carousel-item">
						<img id="calendrier" src="image/calendrie.jpg" alt="Un calendrier"
							title="Un calendrier">
						<div class="carousel-caption d-none d-md-block">

							<a href="catalogue"><button type="button"
									class="btn btn-warning">
									<strong>Découvrez nos formations </strong>
								</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>




	</Section>

	<article id="article1">
		<div class="shadow-sm p-3 mb-5 bg-body rounded">
			<h3>
				<srong>Actualités de IT-Training</srong>
			</h3>
			<div class="row row-cols-1 row-cols-md-4 g-4">
				<div class="col">
					<div class="card h-100">
					<br/>
						<img id="aws" src="image/aws.png" class="card-img-top"
							alt="illustration de amazon partenariat"
							title="illustration de amazon partenariat">
						<div class="card-body">
							</br>
							<h5 class="card-title">
								<strong>Nouveau Partenariat</strong>
							</h5>
							<p class="card-text">Nous avons la joie de vous annoncer
								qu'IT-Training a obtenu un nouveau partenariat avec Amazon
								partner</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<img id="bureau" src="image/bureau.jpg" class="card-img-top"
							alt="image d'une bureau" title="image d'un bureau">
						<div class="card-body">
							<h5 class="card-title">
								<strong>Salon de l'informatique</strong>
							</h5>
							<p class="card-text">Nous serons présent au Salon de
								l'informatique le 27 mars 2022 à Paris pour présenter les
								nouveaux enjeux de l'informatique d'aujourd'hui</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<img src="image/cpf2.png" class="card-img-top"
							alt="Logo du Compte Personnel de Formation"
							title="Logo du Compte Personnel de Formation">
						<div class="card-body">
							</br>
							<h5 class="card-title">
								<strong>Financer votre formation avec le CPF</strong>
							</h5>
							<p class="card-text">Des options simples pour encourager
								l'utilisation du CPF. Découvrez la marche à suivre</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<img src="image/clermont.jpg" class="card-img-top"
							alt="Illustration de Clermont Ferrand"
							title="Illustration de Clermont Ferrand">
						<div class="card-body">
							</br>
							<h5 class="card-title">
								<strong>Ouverture prochaine d'une agence à Clermont
									Ferrand en 2023</strong>
							</h5>
							<p class="card-text">IT Training s'exporte l'année prochaine
								dans la préfecture du Puy-de-Dôme</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>


	<article>
		<div class="shadow-sm p-3 mb-5 bg-body rounded">
			<div class="container">
				<div class="row">
					<div class="col">
						<img src="image/entreprise.jpg" atl="image d'entrprise"
							title="image d'entreprise">

					</div>
					<div class="col">
						</br> </br>
						</br>
						<div class="background">
							</br>

							<h4>
								<strong>Qui sommes nous en quelques chiffres?</strong>
							</h4>
							</br>
							<p class="paragrapheGros">
								<strong>10 ans </strong> d'expériences <br> <strong>6
									sites </strong> de formation à travers la France<br> <strong>10
									Formations</strong> proposées <br> <strong>10 000
									personnes </strong> formées<br> <strong>90% </strong>des personnes
								retrouve du travail dans les <strong>6 mois</strong><br> <strong>6
									Entreprises </strong>partenaires en 2020/2021 <br>
							</p>
							</br>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</article>


	<article>
		<div class="shadow-sm p-3 mb-5 bg-body rounded">
			<div class="container">
				<div class="row">
					<div class="col">
						<img id="carte" src="image/CarteFrance.png"
							class="img-fluid rounded-start"
							alt="Une carte de la france avec les Agences de IT"
							title="Une carte de la france avec les Agences de IT">

					</div>
					<div class="col">
						</br>
						</br>
						</br>
						<div class="background">
							</br>
							<h4>
								<strong>Temps fort du groupe</strong>
							</h4>
							<p class="paragrapheGros">
								</br> <strong>2012</strong> - Création de IT-Training à Paris <br>
								<strong>2014</strong> - Ouverture des agences Rennes et de
								Marseille <br> <strong>2015</strong> - Partenariat avec
								Microsoft Partner <br> <strong>2017 </strong>-
								Implémentation des agences de Metz, Bordeaux et Lille <br>
								<strong>2019</strong> - Obtention du prix de la meilleure
								formation informatique <br> <strong>2020</strong> -
								Partenariat avec Citrix Partner et Vmware Partner <br> <strong>2021</strong>
								- Partenariat avec AWS Training Partner Amazon
							</p>
							</br> </br>
							<h4>
								<strong>Evènement à venir</strong>
							</h4>
							</br>
							<p class="paragrapheGros">
								<strong>2023</strong> - Implémentation d'une agence à
								Clermont-Ferrand
							</p>
							</br>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</article>

	<article>
		<div class="shadow-sm p-3 mb-5 bg-body rounded">

			<h3>
				<strong>Témoignages d'anciens stagiaires</strong>
			</h3>

			<div class="row row-cols-1 row-cols-md-3 g-4">
				<div class="col">
					<div class="card h-100">
						<img id="emmanuel" src="image/emmanuel.jpg" class="card-img-top"
							alt="Un homme de 42 ans, Bob" title="Un homme de 42 ans, Bob">
						<div class="card-body">
							<h5 class="card-title">Bob Morrane, 42 ans. Promotion 2021
								Developpeur JAVA/JEE, La Défense</h5>
							<p class="card-text">Je m'apelle Bob Morrane, 45 ans. J'ai
								été dans la promotion Javascript de La Défense en 2021. Je
								souhaitais changer de vie après une carrière stressante en
								finance. J'ai commencé à m'interesser à l'informatique et
								pratiquer dans le cadre de mon acien travail. Ne voulant plus
								continuer dans une voie dont je me reconnaissais plus, j'ai
								cherché des POEC autour de ma région et IT-Training m'a paru le
								meilleur choix. Je ne regrette pas et aujourd'hui je suis
								épanouie dans mon travail de développeur JAVA/JEE.</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<img src="image/Gerard.jpg" class="card-img-top"
							alt="Un homme de 47 ans, Gerard"
							title="Un homme de 47 ans, Gerard">
						<div class="card-body">
							<h5 class="card-title">Gerard Darm, 47 ans. Promotion 2020
								HelpDesk, La Défense</h5>
							<p class="card-text">Je suis Gerard Darm, de la promotion
								2020 pour être Developpeur JAVA/JEE à la Défense. Je souhaitais
								changer de vie après une carrière de responsable de vente auprès
								d'une PME. L'informatique m'a paru une alternative naturelle et
								de ce fait, j'ai recherché une formation courte qui puisse me
								correspondre. IT-Training a été l'opportunité que je cherchais
								pour réaliser mon changement de vie. Aujourd'hui, j'ai trouvé du
								travail à Paris très rapidement après ma POEC.</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card h-100">
						<img src="image/sandrine.jpg" class="card-img-top"
							alt="Une femme de 32 ans, Sandrine"
							title="Une femme de 32 ans, Sandrine">
						<div class="card-body">
							<h5 class="card-title">Sandrine Pierre, 32 ans. Promotion
								2019 Developpeur C#, Lyon</h5>
							<p class="card-text">Je m'apelle Sandrine Pierre, j'ai 35 ans
								et j'ai effectuée ma POAEC à Lyon en 2019 en tant que
								Developpeuse C#. J'ai toujours été attirée par l'informatique
								depuis que je suis jeune et j'ai longtemps hésitée à poursuivre
								mes études dans cette branche. J'ai choisi une autre voie
								d'étude mais depuis la pandémie, j'ai décidée de me lancer et de
								changer de vie. IT-Training offre la possibilité de nombreuses
								formations dans des domaines diverses en informatique.</p>
						</div>
					</div>
				</div>
			</div>
		</div>



	</article>
	<article class="partenaire">
	<br/>
		<div class="partenaire">
			<h4>
				<strong>Nos partenaires technologiques</strong>
			</h4>
			<div class="container">
				<div class="row row-cols-6">
					<div class="col">
						<img src="image/logo-aws-training-partner.png"
							alt="logo-aws-training-partner" title="logo-aws-training-partner">
					</div>
					<div class="col">
						<img src="image/logo-citrix.png" alt="logo-citrix"
							title="logo-citrix">
					</div>
					<div class="col">
						<img src="image/logo-ibm.png" alt="logo-ibm" title="logo-ibm">
					</div>
					<div class="col">
						<img src="image/logo-microsoft.png" alt="logo-microsoft"
							title="logo-microsoft">
					</div>
					<div class="col">
						<img src="image/logo-sap.png" alt="logo-sap" title="logo-sap">
					</div>
					<div class="col">
						<img src="image/logo-vmware.png" alt="logo-vmware"
							title="logo-vmware">
					</div>
				</div>
			</div>
		</div>
		</div>
		<br/>
	</article>
	
	<footer>
	<br/>
		<div class="container">
			<div class="row row-cols-4">
				<div class="col">
					<h6>
						A propos d'IT
						<h6>
							</br>
							<p class="blanc">
								Nous connaitre</br> Nos centres de formation</br> <a href="catalogue"
									class="enleverlien">Formations proposées </br>
								</a> <a href="actualites" class="enleverlien">Actualités </a>
							</p>
				</div>
				<div class="col">
					<h6>
						Aide
						<h6>
							</br>
							<p class="blanc">
								Nous contacter</br> S'inscrire</br> FAQ </br>
				</div>
				<div class="col">
					<h6>
						Qualité et certification
						<h6>
							<img src="image/afaq.png" width="60px" height="60px" alt="logo afaq" title="logo afaq"
								class="logo"> <img src="image/qualiopi.jpg" width="60px" height="60px"
								alt="logo qualiopi" title="logo qualiopi" class="logo"> <img
								src="image/datadock.jpeg" width="60px" height="60px"alt="logo datadock"
								title="logo datadock" class="logo"> </br>
				</div>
				<div class="col">
					<h6>
						Suivez nous
						<h6>
							</br>
							<p class="blanc">
								Linkedin <i class='bx bxl-linkedin-square'></i></br> Twitter <i class='bx bxl-twitter' ></i></br> Facebook <i class='bx bxl-facebook-circle'></i></br> Youtube <i class='bx bxl-youtube' ></i>
							</p>
				</div>
			</div>
		</div>

	</footer>



</body>



<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
	crossorigin="anonymous"></script>


</html>