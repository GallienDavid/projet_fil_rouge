<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Contact</title>

<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<!-- Boxicons CDN Link -->
<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css'
	rel='stylesheet'>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="CSS/body.css">
<link rel="stylesheet" href="CSS/style.css">
<link rel="stylesheet" href="CSS/acceuil.css" type="text/css" />

</head>
<body>
	<%@ include file="newHeader.jspf"%>
	<div class="corps">
	
	<h1>Agences</h1>
	<p>Nous sommes pr�sent sur 6 sites:</p>

		<div class="row row-cols-1 row-cols-md-3 g-4">
			<c:forEach items="${agences}" var="agence">

				<div class="col">
					<div class="card border-secondary mb-3" style="max-width: 18rem;">
						<div class="card-header">Agence ${agence.zoneGeo }</div>
						<div class="card-body text-secondary">
							<h5 class="card-title">Responsable : ${agence.nomResponsable }
								${agence.prenomResponsable }</h5>
							<p class="card-text">Mail : ${agence.mailResponsable }</p>
							<p class="card-text">Adresse : ${agence.adresse }</p>
							<p class="card-text">T�l�phone : ${agence.tel }</p>
						</div>
					</div>
				</div>

			</c:forEach>
		</div>
	</div>
</body>
</html>