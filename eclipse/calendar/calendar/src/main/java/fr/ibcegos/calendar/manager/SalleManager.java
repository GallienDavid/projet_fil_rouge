package fr.ibcegos.calendar.manager;

import java.util.List;

import fr.ibcegos.calendar.DAO.SalleDAO;
import fr.ibcegos.calendar.beans.Salle;


public class SalleManager {
	
	public List<Salle> findAll() {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.findAll();
	}

	public Salle salleDepuisNom(String salleNom) {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.salleDepuisNom(salleNom);
	}

	public List<Salle> findDepuisZoneGeo(String zoneGeo) {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.salleDepuisZoneGeo(zoneGeo);
	}

	public Salle salleByID(int idSalle) {
		SalleDAO salleDAO = new SalleDAO();
		return salleDAO.salleByID(idSalle);
	}

}
