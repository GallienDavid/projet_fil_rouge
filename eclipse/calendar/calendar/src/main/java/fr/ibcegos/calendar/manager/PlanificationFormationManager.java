package fr.ibcegos.calendar.manager;

import java.util.List;

import fr.ibcegos.calendar.DAO.PlanificationFormationDAO;
import fr.ibcegos.calendar.beans.PlanificationFormation;



public class PlanificationFormationManager {

	public void enregistrer(PlanificationFormation planificationFormation) {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		planificationFormationDAO.enregistrer(planificationFormation);
	}
	
	public List<PlanificationFormation> findAll() {
		PlanificationFormationDAO planificationFormationDAO = new PlanificationFormationDAO();
		return planificationFormationDAO.findAll() ;
	}

}
