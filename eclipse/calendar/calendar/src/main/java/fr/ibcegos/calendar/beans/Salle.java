package fr.ibcegos.calendar.beans;

public class Salle {
	private int id ;
	private Agence agence ;
	private String nom ;
	private int nbPlaces ;
	
	public Salle() {
		
	}

	public Salle(Agence agence, String nom, int nbPlaces) {
		super();
		this.agence = agence;
		this.nom = nom;
		this.nbPlaces = nbPlaces;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNbPlaces() {
		return nbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}
	
	

}
