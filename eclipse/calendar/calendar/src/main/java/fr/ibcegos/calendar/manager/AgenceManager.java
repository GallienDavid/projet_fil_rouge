package fr.ibcegos.calendar.manager;

import java.util.List;

import fr.ibcegos.calendar.DAO.AgenceDAO;
import fr.ibcegos.calendar.beans.Agence;




public class AgenceManager {

	public List<Agence> findAll() {
		AgenceDAO agenceDAO = new AgenceDAO();
		return agenceDAO.findAll();
	}

	public List<Agence> findDepuisZoneGeo(String zoneGeo) {
		AgenceDAO agenceDAO = new AgenceDAO();
		return agenceDAO.findDepuisZoneGeo(zoneGeo);
	}
	
	
	
}
