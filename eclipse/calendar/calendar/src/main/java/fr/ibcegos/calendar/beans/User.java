package fr.ibcegos.calendar.beans;

public class User {
	
	private String username ;
	private String passwrd ;
	
	
	
	public User() {
		super();
	}
	public User(String username, String passwrd) {
		super();
		this.username = username;
		this.passwrd = passwrd;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPasswrd() {
		return passwrd;
	}
	public void setPasswrd(String passwrd) {
		this.passwrd = passwrd;
	}
	@Override
	public String toString() {
		return "User [username=" + username + ", passwrd=" + passwrd + "]";
	}
	
	

}
