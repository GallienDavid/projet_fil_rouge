package fr.ibcegos.calendar.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.calendar.beans.PlanificationFormation;
import fr.ibcegos.calendar.beans.Module;

public class PlanificationFormationDAO {

	public void enregistrer(PlanificationFormation planificationFormation) {
		Connection connexion = PoolConnexion.getConnexion();
		String query = "INSERT INTO PLANIFICATION_FORMATION VALUES(?,?,?,0,?)";
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, planificationFormation.getFormation().getId());
			pstmt.setInt(2, planificationFormation.getSalle().getId());
			pstmt.setDate(3, Date.valueOf(planificationFormation.getDateDebut()));
			pstmt.setInt(4, planificationFormation.getCible());
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<PlanificationFormation> findAll() {
		
		List<PlanificationFormation> planificationFormations = new ArrayList<>();
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT ID_SALLE, DATE_DEBUT, PLANIFICATION_FORMATION.ID_PLANIFICATION,VALIDER,CIBLE ,NOM_FORMATION, FORMATION.ID_FORMATION,RESPONSABLE_FORMATION.ID_RESPONSABLE, NOM_RESPONSABLE,PRENOM_RESPONSABLE,MAIL_RESPONSABLE,TELEPHONE_RESPONSABLE FROM PLANIFICATION_FORMATION JOIN FORMATION ON"
				+ "	PLANIFICATION_FORMATION.ID_FORMATION = FORMATION.ID_FORMATION JOIN RESPONSABLE_FORMATION on FORMATION.ID_RESPONSABLE = RESPONSABLE_FORMATION.ID_RESPONSABLE ORDER BY DATE_DEBUT DESC;";
				
		
		try {
			PreparedStatement pstmt = connexion.prepareStatement(query);
			// 3 Ex�cuter la requ�te
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				PlanificationFormation planificationFormation  = remplirPlanificationFormation(rs);	
				planificationFormations.add(planificationFormation);
			}
			for (int i = 0; i < planificationFormations.size(); i++) {
				query = "SELECT  MODULE.ID_MODULE , NOM_MODULE , PRIX , NOMBRE_JOUR"
						+ " FROM FORMATION JOIN MODULE_FORMATION ON FORMATION.ID_FORMATION = MODULE_FORMATION.ID_FORMATION"
						+ " JOIN MODULE ON MODULE.ID_MODULE = MODULE_FORMATION.ID_MODULE WHERE FORMATION.ID_FORMATION = " + planificationFormations.get(i).getFormation().getId() + ";" ;
				pstmt = connexion.prepareStatement(query);	
				//System.out.println(query);
				rs = pstmt.executeQuery();	
				ModuleDAO moduleManager = new ModuleDAO() ;
				List<Module> modules = new ArrayList();
				while(rs.next()) {
					Module module = moduleManager.remplirModule(rs);
					modules.add(module);
				}
				planificationFormations.get(i).getFormation().setModules(modules);		
			}
			
	}catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	return planificationFormations ;
	
	

}

	private PlanificationFormation remplirPlanificationFormation(ResultSet rs) throws SQLException {
		FormationDAO formationDAO = new FormationDAO();
		SalleDAO salleDAO = new SalleDAO() ;
		PlanificationFormation planificationFormation;
		planificationFormation = new PlanificationFormation();
		planificationFormation.setCible(rs.getInt("CIBLE"));
		planificationFormation.setId(rs.getInt("ID_PLANIFICATION"));
		planificationFormation.setSalle(salleDAO.salleByID(rs.getInt("ID_SALLE")));
		planificationFormation.setDateDebut(rs.getDate("DATE_DEBUT").toLocalDate());
		
		planificationFormation.setFormation(formationDAO.remplirFormationWithoutModule(rs));
		
		return planificationFormation;
		
	}
	
	
	
 
		
		
}
