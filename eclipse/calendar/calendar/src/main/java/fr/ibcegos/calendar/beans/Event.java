package fr.ibcegos.calendar.beans;

import java.io.Serializable;
import java.time.LocalDate;

public class Event { 
	

	private String title ;
	private LocalDate start ;
	private LocalDate end ;
	private String color ;
	private String textColor ;
	private String borderColor ;
	private String hello ;
	
	
	
	public String getHello() {
		return hello;
	}
	public void setHello(String hello) {
		this.hello = hello;
	}
	public String getTextColor() {
		return textColor;
	}
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	public String getBorderColor() {
		return borderColor;
	}
	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public LocalDate getStart() {
		return start;
	}
	public void setStart(LocalDate start) {
		this.start = start;
	}
	public LocalDate getEnd() {
		return end;
	}
	public void setEnd(LocalDate end) {
		this.end = end;
	}
	public Event(String title, LocalDate start, LocalDate end) {
		super();
		this.title = title;
		this.start = start;
		this.end = end;
	}
	public Event() {
		super();
	}
	@Override
	public String toString() {
		return "Event [title=" + title + ", start=" + start + ", end=" + end + "]";
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	
	
}
