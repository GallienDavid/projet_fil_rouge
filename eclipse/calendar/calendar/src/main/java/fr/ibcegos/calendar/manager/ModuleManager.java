package fr.ibcegos.calendar.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ibcegos.calendar.DAO.ModuleDAO;
import fr.ibcegos.calendar.beans.Module;

public class ModuleManager {
	
	public void enregistrer(Module module) {
		ModuleDAO moduleDAO = new ModuleDAO();
		// NB : on pourrait chiffrer le MDP ici, c'est le bon endroit
		ModuleDAO.enregistrer(module);
	}

	public List<Module> findAll() {
		ModuleDAO moduleDAO = new ModuleDAO();
		return moduleDAO.findAll();
	}

	public static Module moduleDepuisNom(String nomModule) {
		ModuleDAO moduleDAO = new ModuleDAO();
		return moduleDAO.findModuleDepuisNom(nomModule);
	}
	
	Map<String,String> erreurs = new HashMap<>();
	
	public Map<String,String> verif(String nom, String duree, String prix) {
		verif("nom",nom);
		verif("duree",duree);
		verif("prix",prix);
		return erreurs;
	}
	
	public void verif(String param, String value) {
		if(value == null || value.isBlank()) {
			erreurs.put(param,value);
		}
	}

}
