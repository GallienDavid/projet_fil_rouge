package fr.ibcegos.calendar.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ibcegos.calendar.beans.Event;
import fr.ibcegos.calendar.beans.PlanificationFormation;

public class EventDAO {
	
	
	public List<Event> findAll() {
			
			List<Event> events = new ArrayList<>();
			
			PlanificationFormationDAO planificationDAO = new PlanificationFormationDAO() ;
			
			List<PlanificationFormation> plani = planificationDAO.findAll() ;
			
			for(int i = 0 ; i < plani.size();i++) {
				Event event = new Event();
				event.setStart(plani.get(i).getDateDebut());
				event.setEnd(plani.get(i).dateFin());
				event.setTitle(plani.get(i).getFormation().getNom());
				if(plani.get(i).getCible() == 0) {
					event.setColor("yellow");
					event.setTextColor("blue");
				}else {
					event.setColor("red");
					event.setTextColor("blue");
				}
				event.setHello("youhou");
				
				events.add(event) ;
			}
			
		return events ;
	}

}
