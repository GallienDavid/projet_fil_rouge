package fr.ibcegos.calendar.beans;

import java.time.LocalDate;

public class Session {
	
	private int id ;
	private LocalDate dateDebut ;
	private byte cible ;
	private Salle salle ;
	private byte valider;
	private double note ;
	private Formateur formateur;
	
	
	public Session() {
	}


	public Session(String dateDebut, String cible, Salle salle, Formateur formateur) {
		super();
		this.dateDebut = LocalDate.parse(dateDebut);
		System.out.println(cible);
		this.cible = Byte.parseByte(cible);
		System.out.println(cible);
		this.salle = salle;
		this.formateur = formateur;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public LocalDate getDateDebut() {
		return dateDebut;
	}


	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}


	public byte getCible() {
		return cible;
	}


	public void setCible(byte cible) {
		this.cible = cible;
	}


	public Salle getSalle() {
		return salle;
	}


	public void setSalle(Salle salle) {
		this.salle = salle;
	}



	public byte getValider() {
		return valider;
	}


	public void setValider(byte valider) {
		this.valider = valider;
	}


	public double getNote() {
		return note;
	}


	public void setNote(double note) {
		this.note = note;
	}


	public Formateur getFormateur() {
		return formateur;
	}


	public void setFormateur(Formateur formateur) {
		this.formateur = formateur;
	}


	@Override
	public String toString() {
		return "Session [id=" + id + ", dateDebut=" + dateDebut + ", cible=" + cible + ", salle=" + salle + ", valider="
				+ valider + ", note=" + note + ", formateur=" + formateur + "]";
	}
	
	
	
}
