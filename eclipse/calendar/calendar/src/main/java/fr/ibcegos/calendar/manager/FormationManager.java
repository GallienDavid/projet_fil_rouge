package fr.ibcegos.calendar.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ibcegos.calendar.DAO.FormationDAO;
import fr.ibcegos.calendar.beans.Formation;




public class FormationManager {
	
	public void enregistrer(Formation formation) {
		FormationDAO formationDAO = new FormationDAO();
		// NB : on pourrait chiffrer le MDP ici, c'est le bon endroit
		formationDAO.enregistrer(formation);
	}

	public List<Formation> findAll() {
		FormationDAO formationDAO = new FormationDAO();
		return formationDAO.findAll();
	}

	public Formation FormationById(int idFormation) {
		
		FormationDAO formationDAO = new FormationDAO();
		
		return formationDAO.FormationById(idFormation);
	}

	
	Map<String,String> erreurs = new HashMap<>();
	
	public void verif(String param, String value) {
		if(value == null || value.isBlank()) {
			erreurs.put(param,value);
		}
	}
	
	public Map<String,String> verif(String nom, String prenom, List<String> nomModules ) {
		verif("nom",nom);
		verif("prenom",prenom);
		for(int i = 0 ; i < nomModules.size() ; i++) {
			verif("module"+i,nomModules.get(i));
		}
		
		return erreurs;
	}
	
	public void verifMail(String mail) {
		if(!(mail.trim().endsWith(".fr") || mail.endsWith(".com")) || mail== null || mail.isBlank()) {
			erreurs.put("mail", "L'email doit terminer par .com ou .fr");
		}
		
	}

}
