package fr.ibcegos.calendar.api;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.ibcegos.calendar.manager.EventManager;
import fr.ibcegos.calendar.beans.*;

@RestController
@CrossOrigin(origins = {"*"} )
public class CalendarAPI {
	
	@GetMapping("/calendars")
	public List<Event> findAll(){
		EventManager eventManager = new EventManager() ;
		List<Event> events = eventManager.findAll() ;
		return eventManager.findAll();
	}
	

}
