USE BDD_IT_Training;

SELECT * FROM dbo.RESPONSABLE_FORMATION ;
INSERT INTO RESPONSABLE_FORMATION VALUES ('Dupont','Alex','0625847596','alex.dupont@gmail.com') ;
INSERT INTO RESPONSABLE_FORMATION VALUES ('Schmitt','Franck','0625142595','franck.schmitt@gmail.com') ;
INSERT INTO RESPONSABLE_FORMATION VALUES ('Heinrich','Joseph','0614986532','joseph.heinrich@gmail.com') ;
INSERT INTO RESPONSABLE_FORMATION VALUES ('Muller','Laure','0625847563','laure.muller@gmail.com') ;
SELECT * FROM dbo.RESPONSABLE_FORMATION ;

INSERT INTO FORMATION VALUES ('JAVA/JEE', 2) ;
INSERT INTO FORMATION VALUES ('PHP', 4) ;
INSERT INTO FORMATION VALUES ('Langages du Web', 4) ;
INSERT INTO FORMATION VALUES ('C#', 3) ;
INSERT INTO FORMATION VALUES ('C++', 3) ;
INSERT INTO FORMATION VALUES ('JavaScript', 4) ;
INSERT INTO FORMATION VALUES ('Ajax', 4) ;
INSERT INTO FORMATION VALUES ('XML', 2) ;
SELECT * FROM dbo.FORMATION ;

SELECT * FROM INTER_PLANIFICATION ;



------------------------------- Modifs n2 ----------------------------------------------------------

SELECT * FROM dbo.FORMATION f
INNER JOIN dbo.RESPONSABLE_FORMATION r
ON r.ID_RESPONSABLE = f.ID_RESPONSABLE;

INSERT INTO MODULE VALUES ('Langages de développement', 'JAVA', 'Informatique') ;
INSERT INTO MODULE VALUES ('Langages de développement', 'C++', 'Informatique') ;
INSERT INTO MODULE VALUES ('Langages de développement', 'C#', 'Informatique') ;
INSERT INTO MODULE VALUES ('Langages de développement', 'Ajax', 'Informatique') ;
INSERT INTO MODULE VALUES ('Langages de développement', 'SQL', 'Informatique') ;
SELECT * FROM dbo.MODULE ;

INSERT INTO FORMATEUR VALUES ('Delpierre', 'Adrien', 8.2,'0684579532','adrien.delpierre@gmail.com', '12 rue du camelcase 35000 Rennes', 118.25,'Grand Ouest',98,1) ;
INSERT INTO FORMATEUR VALUES ('Baccon', 'Erwan', 8.1,'0674215936','erwan.baccon@gmail.com', '15 avenue de Sodebo 44000 Nantes', 142.32,'Grand Ouest',149,2) ;
INSERT INTO FORMATEUR VALUES ('Poltin', 'Jacques', 7.3,'0645986321','jacques.poltin@gmail.com', '12 rue du Maine 75214 Paris', 102.35,'Ile de France',53,3) ;
INSERT INTO FORMATEUR VALUES ('Juvin', 'Christian', 7.7,'0687546391','christian.juvin@gmail.com', '5 rue du général Leclerc 54000 Metz', 107.21,'Nord Est',87,4) ;
INSERT INTO FORMATEUR VALUES ('Milter', 'Laura', 7.5,'0678543269','laura.milter@gmail.com', '47 avenue de la poterie 13000 Marseille', 136.84,'Sud Est',81,5) ;
SELECT * FROM dbo.FORMATEUR;
SELECT * FROM dbo.SALLES;

INSERT INTO dbo.SESSION VALUES (GETDATE(), 0, 2, 0, 2, 5.4, 87.5, 5) ;
INSERT INTO dbo.SESSION VALUES (GETDATE(), 1, 12, 0, 5, 6.2, 140, 6) ;
INSERT INTO dbo.SESSION VALUES (GETDATE(), 1, 4, 0, 3, 8.4, 2000, 50) ;
INSERT INTO dbo.SESSION VALUES (GETDATE(), 0, 21, 0, 1, 9.1, 52, 1) ;
SELECT * FROM dbo.SESSION;
SELECT * FROM dbo.FORMATION;
SELECT * FROM dbo.MODULE;

INSERT INTO dbo.MODULE_FORMATION VALUES (1,1,2) ;
INSERT INTO dbo.MODULE_FORMATION VALUES (1,5,1) ;
INSERT INTO dbo.MODULE_FORMATION VALUES (4,3,3) ;
SELECT * FROM dbo.MODULE_FORMATION;

